import React, { Component, useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet, TouchableOpacity, Image, ScrollView, StatusBar, TextInput } from 'react-native';
import { Appbar, Menu, Provider } from 'react-native-paper';
import Unorderedlist from 'react-native-unordered-list';
import DatePicker from 'react-native-datepicker';
import 'moment-timezone';
import moment from 'moment';
import {widthToDp, heightToDp} from './Responsive'

var blood = require('./drawables/blood.png');
var urine = require('./drawables/urine.png');
var ultra = require('./drawables/ultrasound.png');
var syringe = require('./drawables/syringe.png');

var map_icon = require('./drawables/map.png');
var imagemenu_icon = require('./drawables/imagemenu.png');
const anc_assist = (props) => {

    const [lmpdate, setLmpdate] = useState("");
    const [edd, eddvalue] = useState('');
    const [anc1, anc1value] = useState('');
    const [anc2, anc2value] = useState('');
    const [anc3, anc3value] = useState('');
    const [anc4, anc4value] = useState('');
    const [anc5, anc5value] = useState('');
    const [anc6, anc6value] = useState('');
    const [anc7, anc7value] = useState('');
    const [anc8, anc8value] = useState('');
    const [visible, setVisible] = React.useState(false);


    const ContentTitle = ({ title, style }) => (
        <Appbar.Content
            title={<Text style={style}> {title} </Text>}
        />
    )

    const set_edddate = (date) => {

        console.log("date", date)
        const lmp = moment(date, 'DD-MM-YYYY') ///set the date format
        console.log("my lmp", lmp)
        const myedd = new Date(lmp);
        myedd.setDate(myedd.getDate() + 282); //apply calculation
        myedd.toString()  //convert to string
        const myedddate = String(myedd).split(' '); //split the date
        const myeddnew = (myedddate[2] + '/' + myedddate[1] + '/' + myedddate[3]);
        console.log("edd date", myeddnew)
        if (myeddnew != "undefined/Date/undefined") {
            eddvalue(myeddnew)
        }

        const anc_1 = new Date(lmp);
        anc_1.setDate(anc_1.getDate() + 84); //apply calculation
        anc_1.toString()  //convert to string
        const myanc1 = String(anc_1).split(' '); //split the date
        const myanc1new = (myanc1[2] + '/' + myanc1[1] + '/' + myanc1[3]);
        console.log(myanc1new)
        if (myanc1new != "undefined/Date/undefined") {
            anc1value(myanc1new)
        }

        const anc_2 = new Date(lmp);
        anc_2.setDate(anc_2.getDate() + 140); //apply calculation
        anc_2.toString()  //convert to string
        const myanc2 = String(anc_2).split(' '); //split the date
        const myanc2new = (myanc2[2] + '/' + myanc2[1] + '/' + myanc2[3]);
        console.log(myanc2new)
        if (myanc2new != "undefined/Date/undefined") {
            anc2value(myanc2new)
        }

        const anc_3 = new Date(lmp);
        anc_3.setDate(anc_3.getDate() + 182); //apply calculation
        anc_3.toString()  //convert to string
        const myanc3 = String(anc_3).split(' '); //split the date
        const myanc3new = (myanc3[2] + '/' + myanc3[1] + '/' + myanc3[3]);
        console.log(myanc3new)
        if (myanc3new != "undefined/Date/undefined") {
            anc3value(myanc3new)
        }

        const anc_4 = new Date(lmp);
        anc_4.setDate(anc_4.getDate() + 210); //apply calculation
        anc_4.toString()  //convert to string
        const myanc4 = String(anc_4).split(' '); //split the date
        const myanc4new = (myanc4[2] + '/' + myanc4[1] + '/' + myanc4[3]);
        console.log(myanc4new)
        if (myanc4new != "undefined/Date/undefined") {
            anc4value(myanc4new)
        }

        const anc_5 = new Date(lmp);
        anc_5.setDate(anc_5.getDate() + 238); //apply calculation
        anc_5.toString()  //convert to string
        const myanc5 = String(anc_5).split(' '); //split the date
        const myanc5new = (myanc5[2] + '/' + myanc5[1] + '/' + myanc5[3]);
        console.log(myanc5new)
        if (myanc5new != "undefined/Date/undefined") {
            anc5value(myanc5new)
        }

        const anc_6 = new Date(lmp);
        anc_6.setDate(anc_6.getDate() + 252); //apply calculation
        anc_6.toString()  //convert to string
        const myanc6 = String(anc_6).split(' '); //split the date
        const myanc6new = (myanc6[2] + '/' + myanc6[1] + '/' + myanc6[3]);
        console.log(myanc6new)
        if (myanc6new != "undefined/Date/undefined") {
            anc6value(myanc6new)
        }

        const anc_7 = new Date(lmp);
        anc_7.setDate(anc_7.getDate() + 266); //apply calculation
        anc_7.toString()  //convert to string
        const myanc7 = String(anc_7).split(' '); //split the date
        const myanc7new = (myanc7[2] + '/' + myanc7[1] + '/' + myanc7[3]);
        console.log(myanc7new)
        if (myanc7new != "undefined/Date/undefined") {
            anc7value(myanc7new)
        }

        const anc_8 = new Date(lmp);
        anc_8.setDate(anc_8.getDate() + 282); //apply calculation
        anc_8.toString()  //convert to string
        const myanc8 = String(anc_8).split(' '); //split the date
        const myanc8new = (myanc8[2] + '/' + myanc8[1] + '/' + myanc8[3]);
        console.log(myanc8new)
        if (myanc8new != "undefined/Date/undefined") {
            anc8value(myanc8new)
        }
    }

    useEffect(() => {
        set_edddate()
    }, [])


    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);

    return (
        <ScrollView style={{ backgroundColor: '#FAE3E9' }}>
            <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
            <Provider>
            <View style={{ height: 60, backgroundColor: "#F8BBD0", }}>
            <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "white", fontSize: widthToDp('6%'), fontWeight: "bold", marginTop: heightToDp('3%'), marginLeft: widthToDp('3%'), marginBottom: heightToDp('3%') }}>ANC Assist</Text>
                <View style={{  marginTop: heightToDp('-1.5%'), paddingLeft: widthToDp('55%') }}>
                    <Menu
                        visible={visible}
                        onDismiss={closeMenu}
                        anchor={<TouchableOpacity
                            onPress={openMenu} >
                            <Text style={styles.threedots}>...</Text>
                        </TouchableOpacity>}>
                        <View style={{ flexDirection: "row" }}>
                            <Image source={map_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                            <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('map')} title="Nearby Hospitals" />
                        </View>
                        <View style={{ flexDirection: "row" }}>
                            <Image source={imagemenu_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5 }}></Image>
                            <Menu.Item style={{ marginTop: -10 }} onPress={() => props.navigation.navigate('nutrition')} title="Nutritional Advice" />
                        </View>
                      
                        
                    </Menu>
                </View>
            </View>
            </View>

            <View style={{ flexDirection: "row" }}>
                <Text style={{ marginLeft: widthToDp('20%'), marginTop: heightToDp('2%'), color: "red", fontSize: widthToDp('5%') }}>LMP</Text>
                <Text style={{ marginLeft: widthToDp('35%'), marginTop: heightToDp('2%'), color: "red", fontSize: widthToDp('5%') }}>EDD</Text>
            </View>
            <View style={{ flexDirection: "row", marginBottom: 5 }}>
                <DatePicker
                    style={{ width: widthToDp('40%'), marginTop: widthToDp('2%') }}
                    date={lmpdate}
                    onDateChange={date => {
                        setLmpdate(date);
                        set_edddate(date)
                        console.log("first", date)
                    }}
                    //The enum of date, datetime and time
                    placeholder="select date"
                    format="DD-MM-YYYY"
                    minDate="01-01-2012"
                    maxDate={new Date()}
                    showYearDropdown
                    ScrollableMonthYearDropdown
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 10
                        },
                        dateInput: {
                            marginLeft: 55
                        }
                    }}>
                </DatePicker>
                <TextInput
                    defaultValue={edd}
                    onChangeText={() => eddvalue()}
                    editable={false}
                    style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('20%'), color: "black", paddingTop: heightToDp('2%') }}></TextInput>
            </View>

            <View style={styles.container}>
                <Text style={{ fontStyle: 'italic', color: 'grey' }} > Physical Examinations: </Text>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Collection of (mother's) medical history</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Checking (mother's) blood pressure</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>(Mother's) height and weight</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Pelvic exam</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Doppler fetal heart rate monitoring</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>(Mother's) blood and urine tests {"\n"} </Text></Unorderedlist>
            </View>



            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'grey' }} > First visit 8-12 weeks: before </Text>
                    <TextInput
                        defaultValue={anc1}
                        onChangeText={() => anc1value()}
                        editable={false}
                        placeholder="--No LMP Date--"
                        style={{ marginTop: -5, padding: 2, color: "grey" }}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Examination: Complete general, and obstetrical examination, BP</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Screening: Haemoglobin, Syphilis, HIV and Proteinuria</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Treatments: Syphilis, ARV if eligible, Treat bacteriuria if indicated*</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Preventive Measures: Tetanus toxoid, Iron and folate+</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Advice: Self-care, alcohol and tobacco use, nutrition, safe sex, rest, sleeping under ITN</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Develop a birth and emergency plan {"\n"}</Text></Unorderedlist>
            </View>


            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'red', fontSize: widthToDp('5.5%'), marginLeft: widthToDp('2%') }} > ANC 1</Text>
                    <Image source={blood} style={{ height: 30, width: 30, marginLeft: widthToDp('7%') }}></Image>
                    <Image source={urine} style={{ height: 30, width: 30, marginLeft: widthToDp('3%')}}></Image>
                    <Image source={syringe} style={{ height: 30, width: 30, marginLeft: widthToDp('3%')}}></Image>
                    <Image source={ultra} style={{ height: 30, width: 30, marginLeft: widthToDp('3%') }}></Image>
                    <View>
                        <Text style={{ fontSize: widthToDp('4%'), color: "red", marginLeft: widthToDp('5%')}}>12 Weeks:</Text>
                        <TextInput
                            defaultValue={anc1}
                            onChangeText={() => anc1value()}
                            editable={false}
                            style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('3%'), color: "red", marginTop: heightToDp('-1%'), fontSize:widthToDp('4%') }}></TextInput>
                    </View>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Diabetes</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Anaemia</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>HIV</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Ultrasound</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Tetanus</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Urine Test {"\n"} </Text></Unorderedlist>
            </View>


            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'red', fontSize: widthToDp('5.5%'), marginLeft: widthToDp('2%') }} > ANC 2</Text>
                    <Image source={blood} style={{ height: 30, width: 30, marginLeft: widthToDp('7%') }}></Image>
                    <Image source={ultra} style={{ height: 30, width: 30, marginLeft: widthToDp('3%') }}></Image>
                    <Text style={{ color: "red", marginLeft: widthToDp('5%'), fontSize: widthToDp('4%') }}>20 Weeks: </Text>
                    <TextInput
                        defaultValue={anc2}
                        onChangeText={() => anc2value()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('0%'), color: "red", fontSize:widthToDp('4%'), marginTop: heightToDp('-1.8%')  }}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Diabetes</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Ultrasound {"\n"}</Text></Unorderedlist>
            </View>


            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'grey' }} > Second visit 24-26 weeks: before </Text>
                    <TextInput
                        defaultValue={anc3}
                        onChangeText={() => anc3value()}
                        editable={false}
                        placeholder="--No LMP Date--"
                        style={{ marginTop: -5, padding: 2, color: "grey" }}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Assess maternal and fetal well-being</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Check record for previous complications and treatments during pregnancy</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Examination: Anaemia, BP, fetal growth, and movements</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Screening: Bacteriuria*</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Treatments: Antihelminthic**, ARV if eligible, Treat bacteriuria if indicated*</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Exclude PIH and anaemia</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Preventive Measures: Tetanus toxoid, Iron and folate, IPTp, ARV</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15 }}>
                    <Text style={styles.text_new}>Birth and emergency plan, reinforcement of previous advice {"\n"}</Text></Unorderedlist>
            </View>


            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'red', fontSize: widthToDp('5.5%'), marginLeft: widthToDp('2%') }} > ANC 3</Text>
                    <Image source={blood} style={{ height: 30, width: 30, marginLeft: widthToDp('7%') }}></Image>
                    <Image source={urine} style={{ height: 30, width: 30, marginLeft: widthToDp('3%')}}></Image>
                    <Text style={{ color: "red", marginLeft: widthToDp('5%'), fontSize: widthToDp('4%') }}>26 Weeks:</Text>
                    <TextInput
                        defaultValue={anc3}
                        onChangeText={() => anc3value()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('0%'), color: "red", fontSize:widthToDp('4%'), marginTop: heightToDp('-1.8%') }}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Diabetes</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Anaemia</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Urine Test {"\n"}</Text></Unorderedlist>
            </View>


            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'red', fontSize: widthToDp('5.5%'), marginLeft: widthToDp('2%') }} > ANC 4</Text>
                    <Image source={blood} style={{ height: 30, width: 30, marginLeft: widthToDp('7%') }}></Image>
                    <Text style={{ color: "red", marginLeft: widthToDp('15%'), fontSize: widthToDp('4%') }}>30 Weeks:</Text>
                    <TextInput
                        defaultValue={anc4}
                        onChangeText={() => anc4value()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('0%'), color: "red", fontSize:widthToDp('4%'), marginTop: heightToDp('-1.8%') }}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Diabetes</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15,  marginLeft: 5 }}>
                    <Text style={styles.text_new}> Anaemia {"\n"}</Text></Unorderedlist>
            </View>
            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'grey' }} > Third visit 32 weeks: before </Text>
                    <TextInput
                        defaultValue={anc5}
                        onChangeText={() => anc5value()}
                        editable={false}
                        placeholder="--No LMP Date--"
                        style={{ marginTop: -5, padding: 2, color: "grey" }}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Assess maternal and fetal well-being</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Examination: Anaemia, BP, fetal growth, multiple pregnancy, PIH</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Screening: Bacteriuria*</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Treatments: ARV if eligible, Treat bacteriuria if indicated*</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Preventive Measures: Iron and folate, IPTp, ARV</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Review birth and emergency plan, infant feeding, postpartum/postnatalcare, pragnancy spacing, reinforcement of previous advice {"\n"}</Text></Unorderedlist>
            </View>

            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'red', fontSize: widthToDp('5.5%'), marginLeft: widthToDp('2%')}} > ANC 5</Text>
                    <Image source={blood} style={{ height: 30, width: 30, marginLeft: widthToDp('7%') }}></Image>
                    <Image source={urine} style={{ height: 30, width: 30, marginLeft: widthToDp('3%') }}></Image>
                    <Text style={{ color: "red", marginLeft: widthToDp('5%'), fontSize: widthToDp('4%') }}>34 Weeks:</Text>
                    <TextInput
                        defaultValue={anc5}
                        onChangeText={() => anc5value()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('0%'), color: "red", fontSize:widthToDp('4%'), marginTop: heightToDp('-1.8%')}}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Diabetes</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Urine Test {"\n"}</Text></Unorderedlist>
            </View>

            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'red', fontSize: widthToDp('5.5%'), marginLeft: widthToDp('2%') }} > ANC 6</Text>
                    <Image source={blood} style={{ height: 30, width: 30, marginLeft: widthToDp('7%')}}></Image>
                    <Text style={{ color: "red", marginLeft: widthToDp('15%'), fontSize: widthToDp('4%')}}>36 Weeks:</Text>
                    <TextInput
                        defaultValue={anc6}
                        onChangeText={() => anc6value()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('0%'), color: "red", fontSize:widthToDp('4%'), marginTop: heightToDp('-1.8%')}}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Diabetes</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Anaemia</Text></Unorderedlist>
            </View>



            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'grey' }} > Fourth visit 36-38 weeks: before </Text>
                    <TextInput
                        defaultValue={anc7}
                        onChangeText={() => anc7value()}
                        editable={false}
                        placeholder="--No LMP Date--"
                        style={{ marginTop: -5, padding: 2, color: "grey" }}></TextInput>
                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Assess significant symptoms. Check record for previous complications and treatments during the pregnancy</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Examination: Anaemia, BP, fetal growth and movements, multiple pregnancy, malpresentation</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Screening: Bacteriuria*</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Treatments: ARV if eligible, If breech, ECV or referral for ECV, Treat bacteriuria if indicated*</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Preventive Measures: Iron and folate, ARV</Text></Unorderedlist>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}>Birth and emergency plan, infant feeding, postpartum/postnatalcare, pragnancy spacing, reinforcement of previous advice {"\n"}</Text></Unorderedlist>
            </View>


            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'red', fontSize: widthToDp('5.5%'), marginLeft: widthToDp('2%') }} > ANC 7</Text>
                    <Image source={blood} style={{ height: 30, width: 30, marginLeft: widthToDp('7%') }}></Image>
                    <Text style={{ color: "red", marginLeft: widthToDp('15%'), fontSize: widthToDp('4%') }}>38 Weeks:</Text>
                    <TextInput
                        defaultValue={anc7}
                        onChangeText={() => anc7value()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('0%'), color: "red", fontSize:widthToDp('4%'), marginTop: heightToDp('-1.8%')}}></TextInput>
                </View>

                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Diabetes {"\n"}</Text></Unorderedlist>
            </View>


            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontStyle: 'italic', color: 'red', fontSize: widthToDp('5.5%'), marginLeft: widthToDp('2%') }} > ANC 8</Text>
                    <Image source={blood} style={{ height: 30, width: 30, marginLeft: widthToDp('7%') }}></Image>
                    <Text style={{ color: "red", marginLeft: widthToDp('15%'), fontSize: widthToDp('4%')}}>40 Weeks:</Text>
                    <TextInput
                        defaultValue={anc8}
                        onChangeText={() => anc8value()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: widthToDp('0%'), color: "red", fontSize:widthToDp('4%'), marginTop: heightToDp('-1.8%')}}></TextInput>

                </View>
                <Unorderedlist bulletUnicode={0x25E6} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={styles.text_new}> Diabetes {"\n"}</Text></Unorderedlist>
            </View>


            <View style={styles.container}>
                <Unorderedlist bulletUnicode={0x29BE} color='black' style={{ fontSize: 15, marginLeft: 5 }}>
                    <Text style={{ fontStyle: 'italic', color: 'grey' }} > Acronyms:</Text></Unorderedlist>
                <Text style={styles.text_new}>EDD = estimated date of delivery;</Text>
                <Text style={styles.text_new}>BP = blood pressure;</Text>
                <Text style={styles.text_new}>PIH = pregnancy induced hypertension;</Text>
                <Text style={styles.text_new}>ARV = antiretroviral drugs for HIV/AIDS;</Text>
                <Text style={styles.text_new}>ECV = external cephalic version;</Text>
                <Text style={styles.text_new}>IPTp = intermittent preventive treatment for malaria during pregnancy;</Text>
                <Text style={styles.text_new}>ITN = insecticide treated bednet; {"\n"}</Text>
                <Text style={styles.text_new}>*Additional intervention for use in referral centres but not recommended as routine for resource-limited settings {"\n"} </Text>
                <Text style={styles.text_new}>**Should not be given in first trimester, but if first visit occurs after 16 weeks, it can be given at first visit {"\n"}</Text>
                <Text style={styles.text_new}>+Should also be prescribed as treatment if anaemia is diagnosed {"\n"}</Text>
            </View>

</Provider>
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingBottom: 0,
        marginBottom: 5,
        borderRadius: 15,
        marginLeft: 2,
        marginRight: 2,
        backgroundColor: "white",
        shadowColor: '#470000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.2,
        elevation: 2
    },
    text_new: {
        color: 'grey',
        marginLeft: 5,
        marginRight: 10
    },
    threedots: {
        color: "white",
        fontWeight: "bold",
        fontSize: widthToDp('12%'),
        marginTop: heightToDp ('0.2%'),
        marginLeft: widthToDp('-4%')
      }
});

export default anc_assist