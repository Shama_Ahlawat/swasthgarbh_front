import React, { useState, useEffect } from 'react';
import { Button, Appbar, TextInput, Menu, Divider, Provider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import moment from 'moment';
import 'moment-timezone';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

var profile_icon = require('./drawables/profilepic.png');
var call_doc_icon = require('./drawables/calldoctor.png');
var bell_icon = require('./drawables/bell.png');
var map_icon = require('./drawables/map.png');
var imagemenu_icon = require('./drawables/imagemenu.png');
var arrows_icon = require('./drawables/arrows.png');
var corona_icon = require('./drawables/corona.png');
var faq_icon = require('./drawables/faq.png');
var logout_icon = require('./drawables/logout.png');


const patient_screenbydocsignin = (props) => {
  const [edd, edd_value] = useState('');
  const [name, setName] = useState('')
  const [visible, setVisible] = React.useState(false);
  const [doc_name, set_doc_name] = useState('')
  const [doc_mob, set_doc_mob] = useState('')

  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  const fetchdata = async () => {
    const token = await AsyncStorage.getItem("token")
    await fetch('https://swasthgarbh-backend.herokuapp.com/doctorspatient', {
      headers: new Headers({
        'Authorization': "Bearer " + token,
      }),
    }).then(res => res.json())
      .then(aagya => {
        set_doc_mob(aagya.docmob)
        set_doc_name(aagya.docname)
        setName(aagya.name)

        let setformat = moment(aagya.lmpdate, 'DD-MM-YYYY') ///set the date format
        const edd = new Date(setformat);
        edd.setDate(edd.getDate() + 282); //apply calculation
        edd.toString()  //convert to string
        // console.log(edd)
        const edddate = String(edd).split(' '); //split the date
        const eddnew = (edddate[2] + '-' + edddate[1] + '-' + edddate[3]);
        edd_value(eddnew)
        console.log(eddnew)
      
        }
      )
  }

  useEffect(() => {
    fetchdata()
  }, [])
        

  // const add_data = async (props)=>{
  //   props.navigation.navigate('patient_add_data')
  // }
  const logout = async () => {
    AsyncStorage.removeItem("token").then(() => {
      props.navigation.replace("home_screen")
    })
  }

  return (
    <>
      <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
      <Provider>
        <View style={{ height: 60, backgroundColor: "#F8BBD0", }}>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "white", padding: 10, fontSize: 20, fontWeight: "bold", marginTop: 10 }}>Patient</Text>
            <View style={{ marginRight: 5, marginTop: -7, paddingLeft: 285 }}>
              <Menu
                visible={visible}
                onDismiss={closeMenu}
                anchor={<TouchableOpacity
                  onPress={openMenu} >
                  <Text style={styles.threedots}>...</Text>
                </TouchableOpacity>}>
                <View style={{ flexDirection: "row" }}>
                  <Image source={bell_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -10 }} onPress={() => { }} title="Notifications" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={map_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('map')} title="Nearby Hospitals" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={imagemenu_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => { }} title="Complication Images" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={arrows_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => { }} title="Change/Add Doctor" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={corona_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('youtube_covid')} title="Covid'19 Advice" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={faq_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('youtube_preeclampsia')} title="About Preeclampsia" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={logout_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 10, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => logout()} title="Logout" />
                </View>
              </Menu>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>
            <Image source={profile_icon} style={{ height: 100, width: 100, marginTop: 5, marginBottom: 5, marginLeft: 5, marginRight: 10, marginHorizontal: 0 }}></Image>
            <View style={styles.container_text}>
              <Text style={{ fontSize: 15, color: "gray" }}>name                            {name} </Text>
              <Text style={{ fontSize: 15, color: "gray" }}>EDD                               {edd}</Text>
              <Text style={{ fontSize: 15, color: "gray" }}>Doctor                          {doc_name}</Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 15, color: "gray" }}>Doctor's Mobile          {doc_mob}</Text>
                <Image source={call_doc_icon} style={{ height: 20, width: 20, marginTop:-1, marginLeft: 60, marginRight: 10, marginHorizontal: 0 }}></Image>
              </View>
              <Text style={{ fontSize: 15, color: "gray" }}>WHO Medication </Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: 10, height: 10, backgroundColor: 'green', marginBottom: 10, marginTop: 10, marginLeft: 30 }} />
            <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Systolic BP </Text>
            <View style={{ width: 10, height: 10, backgroundColor: '#68f3a3', marginBottom: 10, marginTop: 10, marginLeft: 8 }} />
            <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Diastolic BP </Text>
            <View style={{ width: 10, height: 10, backgroundColor: "#5e9ee4", marginBottom: 10, marginTop: 10, marginLeft: 8 }} />
            <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Weight </Text>
          </View>

          <View style={{ flexDirection: "row" }}>
            {/* <Button style = {styles.my_Button} 
      onPress = {() => add_data(props)} > <Text style={styles.text}>Add Data</Text> </Button> */}

            <TouchableOpacity
              style={styles.my_Button1}
              onPress={() => props.navigation.navigate('tests')}>
              <Text style={styles.text}>Tests</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.my_Button2}
              onPress={() => props.navigation.navigate('patient_medicine')}>
              <Text style={styles.text}>Medication</Text>
            </TouchableOpacity>
          </View>
          <Text style={{ fontSize: 15, color: "gray", marginLeft: 6, marginTop: 10 }}>Your History </Text>
        </View>

      </Provider>
      {/* <Button mode= 'contained' style = {{marginLeft:18, marginRight:18, marginTop:18}} 
      onPress = {() => logout()} > Sign Out </Button> */}

    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0,
    padding: 5,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2
  },
  container_text: {
    justifyContent: 'flex-start',
    alignItems: "flex-start",
  },
  my_Button1: {
    paddingHorizontal: 45,
    paddingVertical: 10,
    marginLeft: 40,
    marginRight: 20,
    alignSelf: 'flex-start',
    backgroundColor: '#F8BBD0'
  },
  my_Button2: {
    paddingHorizontal: 35,
    paddingVertical: 10,
    marginLeft: 30,
    marginRight: 20,
    alignSelf: 'flex-start',
    backgroundColor: '#F8BBD0'
  },
  logout_Button: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    marginLeft: 5,
    marginRight: 15,
    alignSelf: 'center',
    backgroundColor: '#F8BBD0'
  },
  threedots: {
    color: "white",
    fontWeight: "bold",
    fontSize: 40
  }
})

export default patient_screenbydocsignin;









