import React, { useState, useEffect } from 'react';
import { Button, Appbar, TextInput, Menu, Divider, Provider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import moment from 'moment';
import 'moment-timezone';
import Overlay from 'react-native-modal-overlay';
import Toast from 'react-native-simple-toast';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
var PushNotification = require("react-native-push-notification");
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { FlatList } from 'react-native-gesture-handler';

var profile_icon = require('./drawables/profilepic.png');
var call_doc_icon = require('./drawables/calldoctor.png');
var bell_icon = require('./drawables/bell.png');
var map_icon = require('./drawables/map.png');
var imagemenu_icon = require('./drawables/imagemenu.png');
var arrows_icon = require('./drawables/arrows.png');
var corona_icon = require('./drawables/corona.png');
var faq_icon = require('./drawables/faq.png');
var logout_icon = require('./drawables/logout.png');
var switchdoc_icon = require('./drawables/switchdoc.png');

const patient_screen = (props) => {
  const [edd, edd_value] = useState('');
  const [name, setName] = useState('')
  const [visible, setVisible] = React.useState(false);
  const [docname, set_docname] = useState('')
  const [docmob, set_docmob] = useState('')
  const [doc_name, set_doc_name] = useState('')
  const [doc_mob, set_doc_mob] = useState('')
  const [refreshing, setRefreshing] = useState(false);
  const [data, setdata] = useState([]);
  const [numberholder, setNumberholder] = useState("1");
  const [modalVisible, setModelvisible] = useState(false)
  const [doc_num, setDoc_num] = useState('')
  const [docmobile, setDocmobile] = useState('')
  const [doctorname, setDoctorname] = useState('-- No Data --')
  const [dochos, setDochos] = useState('-- No Data --')
  const [docspeciality, setDocspeciality] = useState('-- No Data --')

  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);


  PushNotification.configure({
    onRegister: function (token) {
      //console.log("TOKEN:", token);
    },
    onNotification: function (notification) {
      console.log("NOTIFICATION:", notification.message);
      notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    permissions: {
      alert: true,
      badge: true,
      sound: true,
    },

    popInitialNotification: true,
    requestPermissions: true,
  });


  const notification_anc = async () => {
    console.log("gpoing to hit")
    const token = await AsyncStorage.getItem("token")
    //console.log(token)
    const patient = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
      headers: new Headers({
        Authorization: "Bearer " + token
      })
    })
    const patient_data = await patient.json();

    let setformat = moment(patient_data.lmpdate, 'DD-MM-YYYY')
    let anc1date = new Date(setformat);
    anc1date.setDate(anc1date.getDate() + 84); //apply calculation
    anc1date.setHours(19)
    anc1date.setMinutes(19);
    anc1date.setMilliseconds(0);
    let anc1_date = anc1date.getDate() + "/" + anc1date.getMonth() + "/" + anc1date.getFullYear()

    anc1datebefore2.setDate(anc1datebefore2.getDate() + 82); //apply calculation
    anc1datebefore2.setHours(19)
    anc1datebefore2.setMinutes(19);
    anc1datebefore2.setMilliseconds(0);
    let anc1_datebefore2 = anc1datebefore2.getDate() + "/" + anc1datebefore2.getMonth() + "/" + anc1datebefore2.getFullYear()

    anc1datebefore5.setDate(anc1datebefore5.getDate() + 79); //apply calculation
    anc1datebefore5.setHours(19)
    anc1datebefore5.setMinutes(19);
    anc1datebefore5.setMilliseconds(0);
    let anc1_datebefore5 = anc1datebefore5.getDate() + "/" + anc1datebefore5.getMonth() + "/" + anc1datebefore5.getFullYear()

    let anc2date = new Date(setformat);
    anc2date.setDate(anc2date.getDate() + 140); //apply calculation
    anc2date.setHours(19)
    anc2date.setMinutes(19);
    anc2date.setMilliseconds(0);
    let anc2_date = anc2date.getDate() + "/" + anc2date.getMonth() + "/" + anc2date.getFullYear()

    let anc2datebefore2 = new Date(setformat);
    anc2datebefore2.setDate(anc2datebefore5.getDate() + 138); //apply calculation
    anc2datebefore2.setHours(19);
    anc2datebefore2.setMinutes(19);
    anc2datebefore2.setMilliseconds(0);
    let anc2_datebefore2 = anc2datebefore2.getDate() + "/" + anc2datebefore2.getMonth() + "/" + anc2datebefore2.getFullYear()

    let anc2datebefore5 = new Date(setformat);
    anc2datebefore5.setDate(anc2datebefore5.getDate() + 135); //apply calculation
    anc2datebefore5.setHours(19);
    anc2datebefore5.setMinutes(19);
    anc2datebefore5.setMilliseconds(0);
    let anc2_datebefore5 = anc2datebefore5.getDate() + "/" + anc2datebefore5.getMonth() + "/" + anc2datebefore5.getFullYear()

    let anc3date = new Date(setformat);
    anc3date.setDate(anc3date.getDate() + 182); //apply calculation
    anc3date.setHours(19)
    anc3date.setMinutes(19);
    anc3date.setMilliseconds(0);
    let anc3_date = anc3date.getDate() + "/" + anc3date.getMonth() + "/" + anc3date.getFullYear()

    let anc3datebefore2 = new Date(setformat);
    anc3datebefore2.setDate(anc3datebefore2.getDate() + 180); //apply calculation
    anc3datebefore2.setHours(19)
    anc3datebefore2.setMinutes(19);
    anc3datebefore2.setMilliseconds(0);
    let anc3_datebefore2 = anc3datebefore2.getDate() + "/" + anc3datebefore2.getMonth() + "/" + anc3datebefore2.getFullYear()

    let anc3datebefore5 = new Date(setformat);
    anc3datebefore5.setDate(anc3datebefore5.getDate() + 177); //apply calculation
    anc3datebefore5.setHours(19)
    anc3datebefore5.setMinutes(19);
    anc3datebefore5.setMilliseconds(0);
    let anc3_datebefore5 = anc3datebefore5.getDate() + "/" + anc3datebefore5.getMonth() + "/" + anc3datebefore5.getFullYear()

    let anc4date = new Date(setformat);
    anc4date.setDate(anc4date.getDate() + 210); //apply calculation
    anc4date.setHours(19)
    anc4date.setMinutes(19);
    anc4date.setMilliseconds(0);
    let anc4_date = anc4date.getDate() + "/" + anc4date.getMonth() + "/" + anc4date.getFullYear()

    let anc4datebefore2 = new Date(setformat);
    anc4datebefore2.setDate(anc4datebefore2.getDate() + 208); //apply calculation
    anc4datebefore2.setHours(19)
    anc4datebefore2.setMinutes(19);
    anc4datebefore2.setMilliseconds(0);
    let anc4_datebefore2 = anc4datebefore2.getDate() + "/" + anc4datebefore2.getMonth() + "/" + anc4datebefore2.getFullYear()

    let anc4datebefore5 = new Date(setformat);
    anc4datebefore5.setDate(anc4datebefore5.getDate() + 205); //apply calculation
    anc4datebefore5.setHours(19)
    anc4datebefore5.setMinutes(19);
    anc4datebefore5.setMilliseconds(0);
    let anc4_datebefore5 = anc4datebefore5.getDate() + "/" + anc4datebefore5.getMonth() + "/" + anc4datebefore5.getFullYear()

    let anc5date = new Date(setformat);
    anc5date.setDate(anc5date.getDate() + 238); //apply calculation
    anc5date.setHours(19)
    anc5date.setMinutes(50);
    anc5date.setMilliseconds(0);
    let anc5_date = anc5date.getDate() + "/" + anc5date.getMonth() + "/" + anc5date.getFullYear()

    let anc5datebefore2 = new Date(setformat);
    anc5datebefore2.setDate(anc5datebefore2.getDate() + 236);
    anc5datebefore2.setMilliseconds(0);
    anc6datebefore2.setMinutes(19);
    anc6datebefore2.setMilliseconds(0);
    let anc5_datebefore2 = anc5datebefore2.getDate() + "/" + anc5datebefore2.getMonth() + "/" + anc5datebefore2.getFullYear()

    let anc5datebefore5 = new Date(setformat);
    anc5datebefore5.setDate(anc5datebefore5.getDate() + 233);
    anc5datebefore5.setMilliseconds(0);
    anc6datebefore5.setMinutes(19);
    anc6datebefore5.setMilliseconds(0);
    let anc5_datebefore5 = anc5datebefore5.getDate() + "/" + anc5datebefore5.getMonth() + "/" + anc5datebefore5.getFullYear()

    let anc6date = new Date(setformat);
    anc6date.setDate(anc6date.getDate() + 252); //apply calculation
    anc6date.setHours(19)
    anc6date.setMinutes(19);
    anc6date.setMilliseconds(0);
    let anc6_date = anc6date.getDate() + "/" + anc6date.getMonth() + "/" + anc6date.getFullYear()

    let anc6datebefore2 = new Date(setformat);
    anc6datebefore2.setDate(anc6date.getDate() + 250); //apply calculation
    anc6datebefore2.setHours(19)
    anc6datebefore2.setMinutes(19);
    anc6datebefore2.setMilliseconds(0);
    anc6_datebefore2 = anc6datebefore2.getDate() + "/" + anc6datebefore2.getMonth() + "/" + anc6datebefore2.getFullYear()

    let anc6datebefore5 = new Date(setformat);
    anc6datebefore5.setDate(anc6date.getDate() + 247); //apply calculation
    anc6datebefore5.setHours(19)
    anc6datebefore5.setMinutes(19);
    anc6datebefore5.setMilliseconds(0);
    let anc6_datebefore5 = anc6datebefore5.getDate() + "/" + anc6datebefore5.getMonth() + "/" + anc6datebefore5.getFullYear()

    let anc7date = new Date(setformat);
    anc7date.setDate(anc7date.getDate() + 266); //apply calculation
    anc7date.setHours(19)
    anc7date.setMinutes(19);
    anc7date.setMilliseconds(0);
    let anc7_date = anc7date.getDate() + "/" + anc7date.getMonth() + "/" + anc7date.getFullYear()

    let anc7datebefore2 = new Date(setformat);
    anc7datebefore2.setDate(anc7datebefore2.getDate() + 264); //apply calculation
    anc7datebefore2.setHours(19)
    anc7datebefore2.setMinutes(19);
    anc7datebefore2.setMilliseconds(0);
    let anc7_datebefore2 = anc7datebefore2.getDate() + "/" + anc7datebefore2.getMonth() + "/" + anc7datebefore2.getFullYear()

    let anc7datebefore5 = new Date(setformat);
    anc7datebefore5.setDate(anc7datebefore5.getDate() + 261); //apply calculation
    anc7datebefore5.setHours(19)
    anc7datebefore5.setMinutes(19);
    anc7datebefore5.setMilliseconds(0);
    let anc7_datebefore5 = anc7datebefore5.getDate() + "/" + anc7datebefore5.getMonth() + "/" + anc7datebefore5.getFullYear()

    let anc8date = new Date(setformat);
    anc8date.setDate(anc8date.getDate() + 282); //apply calculation
    anc8date.setHours(19)
    anc8date.setMinutes(19);
    anc8date.setMilliseconds(0);
    let anc8_date = anc8date.getDate() + "/" + anc8date.getMonth() + "/" + anc8date.getFullYear()

    let anc8datebefore2 = new Date(setformat);
    anc8datebefore2.setDate(anc8datebefore2.getDate() + 280); //apply calculation
    anc8datebefore2.setHours(19)
    anc8datebefore2.setMinutes(19);
    anc8datebefore2.setMilliseconds(0);
    let anc8_datebefore2 = anc8datebefore2.getDate() + "/" + anc8datebefore2.getMonth() + "/" + anc8datebefore2.getFullYear()

    let anc8datebefore5 = new Date(setformat);
    anc8datebefore5.setDate(anc8datebefore5.getDate() + 277); //apply calculation
    anc8datebefore5.setHours(19)
    anc8datebefore5.setMinutes(19);
    anc8datebefore5.setMilliseconds(0);
    let anc8_datebefore5 = anc8datebefore5.getDate() + "/" + anc8datebefore5.getMonth() + "/" + anc8datebefore5.getFullYear()

    let today = new Date()
    let today_date = today.getDate() + "/" + today.getMonth() + "/" + today.getFullYear()

    if (today_date == anc1_date) {
      console.log("today is your anc1 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Today is the date for your first ANC visit, kindly visit to your doctor", // (required)
        date: new Date(anc1date),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc1_datebefore2) {
      console.log("2 days before anc1 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Two days left for your first ANC visit", // (required)
        date: new Date(anc1datebefore2),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc1_datebefore5) {
      console.log("5 days before anc1 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Five days left for your first ANC visit", // (required)
        date: new Date(anc1datebfore5),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc2_date) {
      console.log("today is your anc2 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Today is the date for your second ANC visit, kindly visit to your doctor", // (required)
        date: new Date(anc2date),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc2_datebefore2) {
      console.log("2 days before anc2 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Two days left for your second ANC visit", // (required)
        date: new Date(anc2datebefore2),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc2_datebefore5) {
      console.log("5 days before anc2 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Five days left for your second ANC visit", // (required)
        date: new Date(anc2datebefore5),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc3_date) {
      console.log("today is your anc3 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Today is the date for your third ANC visit, kindly visit to your doctor", // (required)
        date: new Date(anc3date),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc3_datebefore2) {
      console.log("2 days before anc3 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Two days left for your third ANC visit", // (required)
        date: new Date(anc3datebefore2),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc3_datebefore5) {
      console.log("5 days before anc3 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Five days left for your third ANC visit", // (required)
        date: new Date(anc3datebefore5),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc4_date) {
      console.log("today is your anc4 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Today is the date for your fourth ANC visit, kindly visit to your doctor", // (required)
        date: new Date(anc4date),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc4_datebefore2) {
      console.log("2 days before anc4 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Two days left for your fourth ANC visit", // (required)
        date: new Date(anc4datebefore2),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc4_datebefore5) {
      console.log("5 days before anc4 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Five days left for your fourth ANC visit", // (required)
        date: new Date(anc4datebefore5),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc5_date) {
      console.log("today is your anc5 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Today is the date for your fifth ANC visit, kindly visit to your doctor", // (required)
        date: new Date(anc5date),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc5_datebefore2) {
      console.log("2 days before anc5 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Two days left for your fifth ANC visit", // (required)
        date: new Date(anc5datebefore2),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc5_datebefore5) {
      console.log("5 days before anc5 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Five days left for your fifth ANC visit", // (required)
        date: new Date(anc5datebefore5),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc6_date) {
      console.log("today is your anc6 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Today is the date for your sixth ANC visit, kindly visit to your doctor", // (required)
        date: new Date(anc6date),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc6_datebefore2) {
      console.log("2 days before anc6 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Two days left for your sixth ANC visit", // (required)
        date: new Date(anc6datebefore2),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc6_datebefore5) {
      console.log("5 days before anc6 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Five days left for your sixth ANC visit", // (required)
        date: new Date(anc6datebefore5),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc7_date) {
      console.log("today is your anc7 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Today is the date for your seventh ANC visit, kindly visit to your doctor", // (required)
        date: new Date(anc7date),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc7_datebefore2) {
      console.log("2 days before anc7 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Two days left for your seventh ANC visit", // (required)
        date: new Date(anc7datebefore2),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc7_datebefore5) {
      console.log("5 days before anc7 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Five days left for your seventh ANC visit", // (required)
        date: new Date(anc7datefor5),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc8_date) {
      console.log("today is your anc8 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Today is the date for your eighth ANC visit, kindly visit to your doctor", // (required)
        date: new Date(anc8date),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc8_datebefore2) {
      console.log("2 days before anc8 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Two days left for your eighth ANC visit", // (required)
        date: new Date(anc8datebefore2),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

    if (today_date == anc8_datebefore5) {
      console.log("5 days before anc8 date")
      PushNotification.localNotificationSchedule({
        //... You can use all the options from localNotifications
        message: "Five days left for your eighth ANC visit", // (required)
        date: new Date(anc8datebefore5),
        allowWhileIdle: true, // (optional) set notification to work while on doze
      });
    }

  }


  const fetchd = async (doc_num) => {
    await fetch('https://swasthgarbh-backend.herokuapp.com/doctorlist', {
    }).then(res => res.json())
      .then(data => {
        for (var i = 0; i < data.length; i++) {
          if (doc_num == (data[i].mobile)) {
            // //console.log("doc num is",doc_num)  
            setDoctorname(data[i].name)
            setDochos(data[i].hospital)
            setDocspeciality(data[i].speciality)
            setDocmobile(data[i].mobile)
          } else {
            //console.log("doctor not found")
          }
        }
      }
      )
  }

  const fetchdata = async () => {
    const token = await AsyncStorage.getItem("token")
    //console.log(token)
    const patient = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
      headers: new Headers({
        Authorization: "Bearer " + token
      })
    })
    const patient_data = await patient.json();
    setName(patient_data.name)

    let setformat = moment(patient_data.lmpdate, 'DD-MM-YYYY') ///set the date format
    const edd = new Date(setformat);
    edd.setDate(edd.getDate() + 282); //apply calculation
    edd.toString()  //convert to string

    const edddate = String(edd).split(' '); //split the date
    const eddnew = (edddate[2] + '-' + edddate[1] + '-' + edddate[3]);
    edd_value(eddnew)

    if (patient_data.docname && patient_data.mobileDoctor) {
      set_docmob(patient_data.mobileDoctor);
      set_docname(patient_data.docname)
    } else {
      set_doc_mob("- - - -");
      set_doc_name("No Doctor")
    }
    const adddata = await fetch('https://swasthgarbh-backend.herokuapp.com/patientadddata_list')
    const add_data = await adddata.json();
    let arr = []

    for (var i = 0; i < add_data.length; i++) {
      if ((add_data[i].pat_mob) == patient_data.mobile) {
        const days = (add_data[i].date.split(" ")[0])
        add_data[i] = Object.assign({ day: days }, add_data[i]);
        const months = (add_data[i].date.split(" ")[1])
        add_data[i] = Object.assign({ month: months }, add_data[i]);
        const years = (add_data[i].date.split(" ")[2])
        add_data[i] = Object.assign({ year: years }, add_data[i]);
        const times = (add_data[i].date.split(" ")[3])
        add_data[i] = Object.assign({ time: times }, add_data[i]);
        const am_pm = (add_data[i].date.split(" ")[4])
        add_data[i] = Object.assign({ noon_mor: am_pm }, add_data[i]);
        arr.push(add_data[i])
        const pops = arr.pop();
        arr.unshift(pops)
        setdata(arr)

      }

    }
    // //console.log("data found is ", arr)
    if (arr.length === 0) {
      for (let i = 0; i < 30; i++) {
        var sysnum = Math.floor(Math.random() * 40) + 90;
        var dysnum = Math.floor(Math.random() * 40) + 60;
        var body = Math.floor(Math.random() * 30) + 50;
        var bpv = Math.floor(Math.random() * 4) + (1);
        var urine_alb = Math.floor(Math.random() * 4) + 1;
        var date = Math.floor(Math.random() * 30) + 1;
        if (date == 1 || date == 21 || date == 31) {
          var date = date + "st";
        }
        if (date == 2 || date == 22) {
          var date = date + "nd";
        }
        if (date == 3 || date == 23) {
          var date = date + "rd";
        }
        if (date > 3 || date < 21) {
          date = date + "th";
        }

        var months = Math.floor(Math.random() * 12) + 1;
        if (months == 1) {
          months = 'Jan'
        } if (months == 2) {
          months = 'Feb'
        } if (months == 3) {
          months = 'March'
        } if (months == 4) {
          months = 'April'
        } if (months == 5) {
          months = 'May'
        } if (months == 6) {
          months = 'June'
        } if (months == 7) {
          months = 'July'
        } if (months == 8) {
          months = 'Aug'
        } if (months == 9) {
          months = 'Sep'
        } if (months == 10) {
          months = 'Oct'
        } if (months == 11) {
          months = 'Nov'
        } if (months == 12) {
          months = 'Dec'
        }
        arr[i] = Object.assign({ sys_BP: sysnum }, arr[i]);
        arr[i] = Object.assign({ dys_BP: dysnum }, arr[i]);
        arr[i] = Object.assign({ bodyweight: body }, arr[i]);
        arr[i] = Object.assign({ urine: urine_alb }, arr[i]);
        arr[i] = Object.assign({ bleed: bpv }, arr[i]);
        arr[i] = Object.assign({ month: months }, arr[i]);
        arr[i] = Object.assign({ day: date }, arr[i]);
        arr[i] = Object.assign({ year: 2020 }, arr[i]);
      }
      // //console.log("after adding dummy data", arr)
      setdata(arr)
    }
  }

  useEffect(() => {
    fetchdata();
    fetchd();
    notification_anc();
  }, [])

  const changedoc = async () => {
    const toke = await AsyncStorage.getItem("token")
    const patcred = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
      headers: new Headers({
        Authorization: "Bearer " + toke
      })
    })
    const pat = await patcred.json();
    const pat_mob = (pat.mobile)

    const patlist = await fetch('https://swasthgarbh-backend.herokuapp.com/patientlist')
    const pat_data = await patlist.json();
    for (let i = 0; i < pat_data.length; i++) {
      if (pat_data[i].mobile == pat_mob) {
        ////console.log("list me bhi h ", pat_data[i]._id)
        const tok = await AsyncStorage.getItem("token")
        await fetch('https://swasthgarbh-backend.herokuapp.com/change_doc/' + pat_data[i]._id,
          {
            method: "PUT",
            headers: {
              Authorization: "Bearer " + tok,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "mobileDoctor": (docmobile),
              "docname": (doctorname)
            })
          })
        //console.log(doctorname)
      }
    }
  }

  const logout = async () => {
    AsyncStorage.removeItem("token").then(() => {
      props.navigation.replace("Signin")
    })
  }
  const onClose = () => setModelvisible(false);
  const onOpen = () => setModelvisible(true);

  return (
    <>
      <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
      <Provider>
        <View style={{ height: 60, backgroundColor: "#F8BBD0", }}>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "white", padding: 10, fontSize: 20, fontWeight: "bold", marginTop: 10 }}>Patient</Text>
            <View style={{ marginRight: 5, marginTop: -7, paddingLeft: 285 }}>
              <Menu
                visible={visible}
                onDismiss={closeMenu}
                anchor={<TouchableOpacity
                  onPress={openMenu} >
                  <Text style={styles.threedots}>...</Text>
                </TouchableOpacity>}>
                <View style={{ flexDirection: "row" }}>
                  <Image source={bell_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -10 }} onPress={() => props.navigation.navigate('patient_notification')} title="Notifications" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={map_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('map')} title="Nearby Hospitals" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={imagemenu_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => { }} title="Complication Images" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={arrows_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => { onOpen(); closeMenu() }} title="Change/Add Doctor" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={corona_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('youtube_covid')} title="Covid'19 Advice" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={faq_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('youtube_preeclampsia')} title="About Preeclampsia" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={logout_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 10, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => logout()} title="Logout" />
                </View>
              </Menu>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>
            <Image source={profile_icon} style={{ height: 100, width: 100, marginTop: 5, marginBottom: 5, marginLeft: 5, marginRight: 10, marginHorizontal: 0 }}></Image>
            <View style={styles.container_text}>
              <Text style={{ fontSize: 15, color: "gray" }}>Name                             {name} </Text>
              {/* <Text style={{ fontSize: 15, color: "gray" }}>EDD                               {edd}</Text> */}
              <Text style={{ fontSize: 15, color: "gray" }}>Doctor                           {docname} {doc_name}</Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 15, color: "gray" }}>Doctor's Mobile           {docmob}  {doc_mob}</Text>
                <Image source={call_doc_icon} style={{ height: 20, width: 20, marginTop: -1, marginLeft: 60, marginRight: 10, marginHorizontal: 0 }}></Image>
              </View>
              {/* <Text style={{ fontSize: 15, color: "gray" }}>WHO Medication </Text> */}
            </View>
          </View>
        </View>

        <View style={styles.container}>
          {/* <Text style={{ fontSize: 12, color: "#f45536", alignSelf: "center", marginBottom: 10 }}>* Dummy data for educational purpose </Text> */}


{/* 
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: 10, height: 10, backgroundColor: 'green', marginBottom: 10, marginTop: 10, marginLeft: 30 }} />
            <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Systolic BP </Text>
            <View style={{ width: 10, height: 10, backgroundColor: '#68f3a3', marginBottom: 10, marginTop: 10, marginLeft: 8 }} />
            <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Diastolic BP </Text>
            <View style={{ width: 10, height: 10, backgroundColor: "#5e9ee4", marginBottom: 10, marginTop: 10, marginLeft: 8 }} />
            <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Weight </Text>
          </View> */}

          <View style={{ flexDirection: "row",paddingTop:20,paddingBottom:20 }}>

            <TouchableOpacity
              style={styles.my_Button}
              onPress={() => props.navigation.navigate('add_medication')}>
              <Text style={styles.text}>Add Medication</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.my_Button}
              onPress={() => {
                props.navigation.navigate('tests');
              }}>
              <Text style={styles.text}>All medications</Text>
            </TouchableOpacity>
            <TouchableOpacity
               onPress={() => logout()}>
               <Image source={logout_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 10, marginTop: 5 }}></Image>
            </TouchableOpacity>
              {/* <View style={{ flexDirection: "row" }}>
                  <Image onPress={() => logout()} source={logout_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 10, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => logout()} title="Logout" />
            </View> */}
          </View>
        </View>
        <View>
        <Text style={{ fontSize: 15, color: "gray", marginLeft: 6, marginTop: 30,marginBottom:30 }}>Your History </Text>
        <FlatList style={{ flex: 0, backgroundColor: "white" }}
          data={data}
          // maxToRenderPerBatch={10}
          keyExtractor={(item, index) => item._id}
          renderItem={({ item, index }) =>
            <View>
              <Text style={{ flex: 0, marginLeft: 10, marginTop: 2, color: "gray" }}>{index + 1}</Text>
              <View style={{ flexDirection: "row",alignItems:'center' }}>
                <View>
                  <Text style={styles.list3}> {item.day}</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={styles.list5}>{item.month}</Text>
                    <Text style={{ marginLeft: 5, color: "#FB5448", fontSize: 15, marginTop: 2 }}>{item.year}</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ marginLeft: 30, color: "#FB5448", fontSize: 15, marginTop: 2 }}>{item.time}</Text>
                    <Text style={{ marginLeft: 5, color: "#FB5448", fontSize: 15, marginTop: 2 }}>{item.noon_mor}</Text>
                  </View>
                </View>
                <View>

                  <Text style={styles.list1}>Medicine name : </Text>
                  <Text style={styles.list1}>Medicine quantity : </Text>
                  {/* <Text style={styles.list2}>Heart Rate: {item.heart_rate}</Text> */}
                  {/* <Text style={styles.list4}>Extracomments: {item.extracomments}</Text> */}
                </View>
                <View style={{backgroundColor:'#FB5448',padding:10,borderRadius: 50, borderWidth: 1,
                   borderColor: '#fff'}}>
                    <Text style={{color:'white'}}>Rejected</Text>
                </View>
              </View>



              <Divider style={styles.divider} />
            </View>
          }
        />
        </View>


        <Overlay visible={modalVisible} onClose={onClose} closeOnTouchOutside>

          <View style={{ height: 50, alignSelf: "flex-start", width: "95%", marginLeft: 8 }}>
            <View style={{ flexDirection: "row" }}>
              <Image source={switchdoc_icon} style={{ height: 25, width: 25, marginLeft: 5 }}></Image>
              <Text style={{ fontSize: 20, color: "grey", fontWeight: "bold", marginLeft: 10 }}>Change Doctor</Text>
            </View>

            <View style={{ flex: 1, marginTop: 10 }}>
              <TextInput placeholder="Enter doctor's number"
                style={{ borderLeftColor: "white", borderRightColor: "white", borderTopColor: "white", position: "relative", backgroundColor: "white" }}
                onChangeText={(doc_num) => {
                  setDoc_num({ doc_num });
                  fetchd(doc_num);
                }}
                value={doc_num}
                theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
            </View>

          </View>

          <View style={styles.container2}>
            <Text style={{ fontSize: 13, color: "grey", marginTop: 5 }}>Doctor Name:</Text>
            <Text style={{ fontSize: 13, color: "grey", marginTop: 5, fontWeight: "bold" }}>{doctorname}</Text>
            <Text style={{ fontSize: 13, color: "grey", marginTop: 5 }}>Doctor's Hospital:</Text>
            <Text style={{ fontSize: 13, color: "grey", marginTop: 5, fontWeight: "bold" }}>{dochos}</Text>
            <Text style={{ fontSize: 13, color: "grey", marginTop: 5 }}>Doctor's Speciality:</Text>
            <Text style={{ fontSize: 13, color: "grey", marginTop: 5, fontWeight: "bold" }}>{docspeciality}</Text>
          </View>

          <TouchableOpacity
            style={{ alignSelf: "center", backgroundColor: "pink", marginTop: 20 }}
            onPress={() => {
              changedoc(doc_num);
              setDoctorname("-- No Data --")
              setDochos("-- No Data --")
              setDocspeciality("-- No Data --")
              onClose();
            }}>
            {/* // onPress= {this.ShowHideComponent}> */}
            <Text style={{ color: "black", padding: 15 }}>Change Doctor</Text>
          </TouchableOpacity>

        </Overlay>



      </Provider>

    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0,
    padding: 5,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2
  },
  container2: {
    flex: 0,
    padding: 5,
    margin: 5,
    height: 145,
    width: "95%",
    borderRadius: 15,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2,
    marginTop: 65
  },
  container_text: {
    justifyContent: 'flex-start',
    alignItems: "flex-start",
    marginTop:30
  },
  my_Button: {
    paddingHorizontal: 25,
    paddingVertical: 10,
    marginLeft: 5,
    marginRight: 20,
    alignSelf: 'flex-start',
    backgroundColor: '#F8BBD0'
  },
  logout_Button: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    marginLeft: 5,
    marginRight: 15,
    alignSelf: 'center',
    backgroundColor: '#F8BBD0'
  },
  threedots: {
    color: "white",
    fontWeight: "bold",
    fontSize: 40
  },
  list1: {
    marginTop: 10,
    marginLeft: 10,
    color: 'grey'
  },
  list2: {
    marginTop: 2,
    marginLeft: 10,
    color: 'grey'
  },
  list3: {
    marginTop: 30,
    marginLeft: 15,
    color: '#FB5448',
    fontSize: 35
  },
  list4: {
    marginTop: 2,
    marginLeft: 10,
    marginBottom: 10,
    color: 'grey'
  },
  list5: {
    marginTop: 2,
    marginLeft: 25,
    color: '#FB5448',
    fontSize: 15
  },
  divider: {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1, },
    shadowOpacity: 1.55,
    shadowRadius: 2.84,
    elevation: 1,

  }
})

export default patient_screen;