import React, { useState, useEffect } from 'react';
import { Button, Appbar } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';
import moment from 'moment';
import 'moment-timezone';
import PushNotificationIOS from "@react-native-community/push-notification-ios";

var PushNotification = require("react-native-push-notification");



import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

var bp_icon = require('./drawables/bloodpressure.png');
var uterus_icon = require('./drawables/uterus.png');
var heartbeat_icon = require('./drawables/heartbeat.png');
var weight_icon = require('./drawables/weight.png');


const patient_add_data = (props) => {
  const [sys_BP, set_Sys_BP] = useState('')
  const [dys_BP, set_Dys_BP] = useState('')
  const [headache, set_headache] = useState(false)
  const [abdominal, set_abdominal] = useState(false)
  const [visual, set_visual] = useState(false)
  const [fetal_movement, set_fetal_movement] = useState(false)
  const [swelling, set_swelling] = useState(false)
  const [urine, set_urine] = useState('')
  const [bleed, set_bleed] = useState('')
  const [heart_rate, set_heart_rate] = useState('')
  const [bodyweight, set_bodyweight] = useState('')
  const [extracomments, set_extracomments] = useState('')


  PushNotification.configure({
    onRegister: function (token) {
      //console.log("TOKEN:", token);
    },
    onNotification: function (notification) {
      console.log("NOTIFICATION:", notification.message);
      notification.finish(PushNotificationIOS.FetchResult.NoData);
    },
    //onAction: function (notification) {
    //  console.log("ACTION:", notification.action);
    // console.log("NOTIFICATION:", notification);
    //},
    permissions: {
      alert: true,
      badge: true,
      sound: true,
    },

    popInitialNotification: true,
    requestPermissions: true,
  });


  //PushNotification.cancelAllLocalNotifications()

  const addData = async (props) => {
    var mydate = new Date()
    // var date = new Date().toLocaleString(undefined, {timeZone: 'Asia/Kolkata'});
    var date = mydate.getDate();
    var j = date % 10,
      k = date % 100;
    if (j == 1 && k != 11) {
      var date = date + "st";
    }
    if (j == 2 && k != 12) {
      date = date + "nd";
    }
    if (j == 3 && k != 13) {
      date = date + "rd";
    }
    if (j > 3 || k < 21) date = date + "th";


    var m = new Array();
    m[0] = "Jan";
    m[1] = "Feb";
    m[2] = "March";
    m[3] = "April";
    m[4] = "May";
    m[5] = "June";
    m[6] = "July";
    m[7] = "Aug";
    m[8] = "Sep";
    m[9] = "Oct";
    m[10] = "Nov";
    m[11] = "Dec";
    var month = m[mydate.getMonth()];
    var year = new Date().getFullYear();
    var hours = new Date().getHours();
    var minutes = new Date().getMinutes();
    var newformat = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    let newdate = date + ' ' + month + ' ' + year + ' ' + hours + ':' + minutes + ' ' + newformat;
    console.log(newdate)

    const tok = await AsyncStorage.getItem("token")
    const patcred = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
      headers: new Headers({
        Authorization: "Bearer " + tok
      })
    })
    const pat = await patcred.json();
    const pat_mob = (pat.mobile)


    const token = await AsyncStorage.getItem("token")
    fetch("https://swasthgarbh-backend.herokuapp.com/patient_adddata", {
      method: "POST",
      headers: {
        'Authorization': "Bearer " + token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "sys_BP": sys_BP,
        "dys_BP": dys_BP,
        "headache": headache,
        "abdominal": abdominal,
        "visual": visual,
        "fetal_movement": fetal_movement,
        "swelling": swelling,
        "urine": urine,
        "bleed": bleed,
        "heart_rate": heart_rate,
        "bodyweight": bodyweight,
        "extracomments": extracomments,
        "date": newdate,
        'pat_mob': pat_mob
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log("the added data is ", data)
        var pat_mobile = (data.pat_mob)
        var pat_name = (pat.name)
        var doc_mobile = (pat.mobileDoctor)
        var doc_name = (pat.docname)
        if (data.heart_rate > 100) {
          if (data.urine > 2) {
            if (data.bleed > 0) {
              if (data.dys_BP > 100 && data.sys_BP > 160) {
                var not_1 = "Please contact your doctor (Increased Heart rate, urine albumin, bleeding per vaginum, increased systolic and diastolic BP)"
                var stored_date = new Date()
                var hours = new Date().getHours();
                var minutes = new Date().getMinutes();
                var newformat = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
                fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
                  method: "POST",
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                    "notification": not_1,
                    "date": added_date,
                    "pat_name": pat_name,
                    "pat_mobile": pat_mobile,
                    "doc_name": doc_name,
                    "doc_mobile": doc_mobile
                  })
                })
                  .then(res => res.json())
                  .then(async (notificationdata) => {
                    console.log("notification data is ", notificationdata)
                  })

                PushNotification.localNotification({
                  channelId: "your-channel-id", // (required) channelId, if the channel doesn't exist, it will be created with options passed above (importance, vibration, sound). Once the channel is created, the channel will not be update. Make sure your channelId is different if you change these options. If you have created a custom channel, it will apply options of the channel.
                  ticker: "My Notification Ticker", // (optional)
                  showWhen: true, // (optional) default: true
                  // autocancel: true, // (optional) default: true
                  largeIcon: "ic_launcher", // (optional) default: "ic_launcher". Use "" for no large icon.
                  largeIconUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                  smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small 
                  bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                  color: "red", // (optional) default: system default
                  vibrate: true, // (optional) default: true
                  vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
                  tag: "some_tag", // (optional) add tag to message
                  group: "group", // (optional) add group to message
                  groupSummary: false, // (optional) set this notification to be the group summary for a group of notifications, default: false
                  ongoing: false, // (optional) set whether this is an "ongoing" notification
                  priority: "high", // (optional) set notification priority, default: high
                  visibility: "private", // (optional) set notification visibility, default: private
                  ignoreInForeground: false, // (optional) if true, the notification will not be visible when the app is in the foreground (useful for parity with how iOS notifications appear). should be used in combine with `com.dieam.reactnativepushnotification.notification_foreground` setting
                  shortcutId: "shortcut-id", // (optional) If this notification is duplicative of a Launcher shortcut, sets the id of the shortcut, in case the Launcher wants to hide the shortcut, default undefined
                  onlyAlertOnce: false, // (optional) alert will open only once with sound and notify, default: false

                  when: null, // (optionnal) Add a timestamp pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
                  usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
                  timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null

                  messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 

                  //actions: ["Yes", "No"], // (Android only) See the doc for notification actions to know more
                  invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

                  /* iOS only properties */
                  alertAction: "view", // (optional) default: view
                  category: "", // (optional) default: empty string

                  /* iOS and Android properties */
                  // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
                  title: "SwasthGarbh Alert", // (optional)
                  message: "Please contact your doctor (Increased Heart rate, urine albumin, bleeding per vaginum, increased systolic and diastolic BP)", // (required)
                  userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
                  playSound: false, // (optional) default: true
                  soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
                  ///number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
                  ///repeatType: "day",

                });
              } else {
                var not_1 = "Please contact your doctor at the earliest (Increased Heart rate, urine albumin, bleeding per vaginum)"
                var stored_date = new Date()
                var hours = new Date().getHours();
                var minutes = new Date().getMinutes();
                var newformat = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
                fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
                  method: "POST",
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                    "notification": not_1,
                    "date": added_date,
                    "pat_name": pat_name,
                    "pat_mobile": pat_mobile,
                    "doc_name": doc_name,
                    "doc_mobile": doc_mobile
                  })
                })
                  .then(res => res.json())
                  .then(async (notificationdata) => {
                    console.log("notification data is ", notificationdata)
                  })
                PushNotification.localNotification({
                  showWhen: true, // (optional) default: true
                  // autocancel: true, // (optional) default: true
                  smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
                  //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
                  //subText: "This is a subText", // (optional) default: none
                  bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                  color: "red", // (optional) default: system default
                  vibrate: true, // (optional) default: true
                  vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

                  messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
                  // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
                  title: "SwasthGarbh Alert", // (optional)
                  message: "Please contact your doctor at the earliest (Increased Heart rate, urine albumin, bleeding per vaginum)", // (required)
                  userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
                  playSound: false, // (optional) default: true
                  soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

                });
              }
            } else if (data.dys_BP > 100 && data.sys_BP > 160) {
              var not_1 = "Please contact your doctor at the earliest (Increased Heart rate, urine albumin, increased systolic and diastolic BP)"
              var stored_date = new Date()
              var hours = new Date().getHours();
              var minutes = new Date().getMinutes();
              var newformat = hours >= 12 ? 'PM' : 'AM';
              hours = hours % 12;
              hours = hours ? hours : 12;
              minutes = minutes < 10 ? '0' + minutes : minutes;
              let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
              fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
                method: "POST",
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  "notification": not_1,
                  "date": added_date,
                  "pat_name": pat_name,
                  "pat_mobile": pat_mobile,
                  "doc_name": doc_name,
                  "doc_mobile": doc_mobile
                })
              })
                .then(res => res.json())
                .then(async (notificationdata) => {
                  console.log("notification data is ", notificationdata)
                })
              PushNotification.localNotification({
                showWhen: true, // (optional) default: true
                // autocancel: true, // (optional) default: true
                smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
                //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
                //subText: "This is a subText", // (optional) default: none
                bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                color: "red", // (optional) default: system default
                vibrate: true, // (optional) default: true
                vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

                messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
                // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
                title: "SwasthGarbh Alert", // (optional)
                message: "Please contact your doctor at the earliest (Increased Heart rate, urine albumin, increased systolic and diastolic BP)", // (required)
                userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
                playSound: false, // (optional) default: true
                soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

              });
            } else {
              var not_1 = "Please contact your doctor at the earliest (Increased Heart rate, urine albumin)"
              var stored_date = new Date()
              var hours = new Date().getHours();
              var minutes = new Date().getMinutes();
              var newformat = hours >= 12 ? 'PM' : 'AM';
              hours = hours % 12;
              hours = hours ? hours : 12;
              minutes = minutes < 10 ? '0' + minutes : minutes;
              let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
              fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
                method: "POST",
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  "notification": not_1,
                  "date": added_date,
                  "pat_name": pat_name,
                  "pat_mobile": pat_mobile,
                  "doc_name": doc_name,
                  "doc_mobile": doc_mobile
                })
              })
                .then(res => res.json())
                .then(async (notificationdata) => {
                  console.log("notification data is ", notificationdata)
                })
              PushNotification.localNotification({
                showWhen: true, // (optional) default: true
                // autocancel: true, // (optional) default: true
                smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
                //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
                //subText: "This is a subText", // (optional) default: none
                bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                color: "red", // (optional) default: system default
                vibrate: true, // (optional) default: true
                vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

                messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
                // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
                title: "SwasthGarbh Alert", // (optional)
                message: "Please contact your doctor at the earliest (Increased Heart rate, urine albumin)", // (required)
                userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
                playSound: false, // (optional) default: true
                soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

              });
            }
          } else if (data.bleed > 0) {
            if (data.dys_BP > 100 && data.sys_BP > 160) {
              var not_1 = "Please contact your doctor at the earliest (Increased Heart rate, bleeding per vaginum, increased systolic and diastolic BP)"
              var stored_date = new Date()
              var hours = new Date().getHours();
              var minutes = new Date().getMinutes();
              var newformat = hours >= 12 ? 'PM' : 'AM';
              hours = hours % 12;
              hours = hours ? hours : 12;
              minutes = minutes < 10 ? '0' + minutes : minutes;
              let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
              fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
                method: "POST",
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  "notification": not_1,
                  "date": added_date,
                  "pat_name": pat_name,
                  "pat_mobile": pat_mobile,
                  "doc_name": doc_name,
                  "doc_mobile": doc_mobile
                })
              })
                .then(res => res.json())
                .then(async (notificationdata) => {
                  console.log("notification data is ", notificationdata)
                })
              PushNotification.localNotification({
                showWhen: true, // (optional) default: true
                // autocancel: true, // (optional) default: true
                smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
                //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
                //subText: "This is a subText", // (optional) default: none
                bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                color: "red", // (optional) default: system default
                vibrate: true, // (optional) default: true
                vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

                messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
                // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
                title: "SwasthGarbh Alert", // (optional)
                message: "Please contact your doctor at the earliest (Increased Heart rate, bleeding per vaginum, increased systolic and diastolic BP)", // (required)
                userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
                playSound: false, // (optional) default: true
                soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

              });
            } else {
              var not_1 = "Please contact your doctor at the earliest (Increased Heart rate, bleeding per vaginum)"
              var stored_date = new Date()
              var hours = new Date().getHours();
              var minutes = new Date().getMinutes();
              var newformat = hours >= 12 ? 'PM' : 'AM';
              hours = hours % 12;
              hours = hours ? hours : 12;
              minutes = minutes < 10 ? '0' + minutes : minutes;
              let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
              fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
                method: "POST",
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  "notification": not_1,
                  "date": added_date,
                  "pat_name": pat_name,
                  "pat_mobile": pat_mobile,
                  "doc_name": doc_name,
                  "doc_mobile": doc_mobile
                })
              })
                .then(res => res.json())
                .then(async (notificationdata) => {
                  console.log("notification data is ", notificationdata)
                })
              PushNotification.localNotification({
                showWhen: true, // (optional) default: true
                // autocancel: true, // (optional) default: true
                smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
                //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
                //subText: "This is a subText", // (optional) default: none
                bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                color: "red", // (optional) default: system default
                vibrate: true, // (optional) default: true
                vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

                messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
                // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
                title: "SwasthGarbh Alert", // (optional)
                message: "Please contact your doctor at the earliest (Increased Heart rate, bleeding per vaginum)", // (required)
                userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
                playSound: false, // (optional) default: true
                soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

              });
            }
          } else if (data.dys_BP > 100 && data.sys_BP > 160) {
            var not_1 = "Please contact your doctor at the earliest (Increased Heart rate, increased systolic and diastolic BP)"
            var stored_date = new Date()
            var hours = new Date().getHours();
            var minutes = new Date().getMinutes();
            var newformat = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
            fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "notification": not_1,
                "date": added_date,
                "pat_name": pat_name,
                "pat_mobile": pat_mobile,
                "doc_name": doc_name,
                "doc_mobile": doc_mobile
              })
            })
              .then(res => res.json())
              .then(async (notificationdata) => {
                console.log("notification data is ", notificationdata)
              })
            PushNotification.localNotification({
              showWhen: true, // (optional) default: true
              // autocancel: true, // (optional) default: true
              smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
              //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
              //subText: "This is a subText", // (optional) default: none
              bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
              color: "red", // (optional) default: system default
              vibrate: true, // (optional) default: true
              vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

              messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
              // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
              title: "SwasthGarbh Alert", // (optional)
              message: "Please contact your doctor at the earliest (Increased Heart rate, increased systolic and diastolic BP)", // (required)
              userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
              playSound: false, // (optional) default: true
              soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

            });
          }
          else {
            var not_1 = "Please contact your doctor at the earliest (Increased Heart rate)"
            var stored_date = new Date()
            var hours = new Date().getHours();
            var minutes = new Date().getMinutes();
            var newformat = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
            fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "notification": not_1,
                "date": added_date,
                "pat_name": pat_name,
                "pat_mobile": pat_mobile,
                "doc_name": doc_name,
                "doc_mobile": doc_mobile
              })
            })
              .then(res => res.json())
              .then(async (notificationdata) => {
                console.log("notification data is ", notificationdata)
              })
            PushNotification.localNotification({
              showWhen: true, // (optional) default: true
              // autocancel: true, // (optional) default: true
              smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
              //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
              //subText: "This is a subText", // (optional) default: none
              bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
              color: "red", // (optional) default: system default
              vibrate: true, // (optional) default: true
              vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

              messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
              // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
              title: "SwasthGarbh Alert", // (optional)
              message: "Please contact your doctor at the earliest (Increased Heart rate)", // (required)
              userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
              playSound: false, // (optional) default: true
              soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

            });
          }
        } else if (data.urine > 2) {
          if (data.bleed > 0) {
            var not_1 = "Please contact your doctor at the earliest (Urine albumin, bleeding per vaginum, increased systolic and diastolic BP)"
            var stored_date = new Date()
            var hours = new Date().getHours();
            var minutes = new Date().getMinutes();
            var newformat = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
            fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "notification": not_1,
                "date": added_date,
                "pat_name": pat_name,
                "pat_mobile": pat_mobile,
                "doc_name": doc_name,
                "doc_mobile": doc_mobile
              })
            })
              .then(res => res.json())
              .then(async (notificationdata) => {
                console.log("notification data is ", notificationdata)
              })
            if (data.dys_BP > 100 && data.sys_BP > 160) {
              PushNotification.localNotification({
                showWhen: true, // (optional) default: true
                // autocancel: true, // (optional) default: true
                smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
                //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
                //subText: "This is a subText", // (optional) default: none
                bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                color: "red", // (optional) default: system default
                vibrate: true, // (optional) default: true
                vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

                messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
                // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
                title: "SwasthGarbh Alert", // (optional)
                message: "Please contact your doctor at the earliest (Urine albumin, bleeding per vaginum, increased systolic and diastolic BP)", // (required)
                userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
                playSound: false, // (optional) default: true
                soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

              });
            } else {
              var not_1 = "Please contact your doctor at the earliest (Urine albumin, bleeding per vaginum)"
              var stored_date = new Date()
              var hours = new Date().getHours();
              var minutes = new Date().getMinutes();
              var newformat = hours >= 12 ? 'PM' : 'AM';
              hours = hours % 12;
              hours = hours ? hours : 12;
              minutes = minutes < 10 ? '0' + minutes : minutes;
              let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
              fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
                method: "POST",
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  "notification": not_1,
                  "date": added_date,
                  "pat_name": pat_name,
                  "pat_mobile": pat_mobile,
                  "doc_name": doc_name,
                  "doc_mobile": doc_mobile
                })
              })
                .then(res => res.json())
                .then(async (notificationdata) => {
                  console.log("notification data is ", notificationdata)
                })
              PushNotification.localNotification({
                showWhen: true, // (optional) default: true
                // autocancel: true, // (optional) default: true
                smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
                //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
                //subText: "This is a subText", // (optional) default: none
                bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
                color: "red", // (optional) default: system default
                vibrate: true, // (optional) default: true
                vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

                messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
                // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
                title: "SwasthGarbh Alert", // (optional)
                message: "Please contact your doctor at the earliest (Urine albumin, bleeding per vaginum)", // (required)
                userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
                playSound: false, // (optional) default: true
                soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

              });
            }
          } else if (data.dys_BP > 100 && data.sys_BP > 160) {
            var not_1 = "Please contact your doctor at the earliest (Urine albumin, increased systolic and diastolic BP)"
            var stored_date = new Date()
            var hours = new Date().getHours();
            var minutes = new Date().getMinutes();
            var newformat = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
            fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "notification": not_1,
                "date": added_date,
                "pat_name": pat_name,
                "pat_mobile": pat_mobile,
                "doc_name": doc_name,
                "doc_mobile": doc_mobile
              })
            })
              .then(res => res.json())
              .then(async (notificationdata) => {
                console.log("notification data is ", notificationdata)
              })
            PushNotification.localNotification({
              showWhen: true, // (optional) default: true
              // autocancel: true, // (optional) default: true
              smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
              //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
              //subText: "This is a subText", // (optional) default: none
              bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
              color: "red", // (optional) default: system default
              vibrate: true, // (optional) default: true
              vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

              messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
              // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
              title: "SwasthGarbh Alert", // (optional)
              message: "Please contact your doctor at the earliest (Urine albumin, increased systolic and diastolic BP)", // (required)
              userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
              playSound: false, // (optional) default: true
              soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

            });
          } else {
            var not_1 = "Please contact your doctor at the earliest (Urine albumin)"
            var stored_date = new Date()
            var hours = new Date().getHours();
            var minutes = new Date().getMinutes();
            var newformat = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
            fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "notification": not_1,
                "date": added_date,
                "pat_name": pat_name,
                "pat_mobile": pat_mobile,
                "doc_name": doc_name,
                "doc_mobile": doc_mobile
              })
            })
              .then(res => res.json())
              .then(async (notificationdata) => {
                console.log("notification data is ", notificationdata)
              })
            PushNotification.localNotification({
              showWhen: true, // (optional) default: true
              // autocancel: true, // (optional) default: true
              smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
              //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
              //subText: "This is a subText", // (optional) default: none
              bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
              color: "red", // (optional) default: system default
              vibrate: true, // (optional) default: true
              vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

              messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
              // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
              title: "SwasthGarbh Alert", // (optional)
              message: "Please contact your doctor at the earliest (Urine albumin)", // (required)
              userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
              playSound: false, // (optional) default: true
              soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

            });
          }
        }
        else if (data.bleed > 0) {
          if (data.dys_BP > 100 && data.sys_BP > 160) {
            var not_1 = "Please contact your doctor at the earliest (Bleeding per vaginum, increased systolic and diastolic BP)"
            var stored_date = new Date()
            var hours = new Date().getHours();
            var minutes = new Date().getMinutes();
            var newformat = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
            fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "notification": not_1,
                "date": added_date,
                "pat_name": pat_name,
                "pat_mobile": pat_mobile,
                "doc_name": doc_name,
                "doc_mobile": doc_mobile
              })
            })
              .then(res => res.json())
              .then(async (notificationdata) => {
                console.log("notification data is ", notificationdata)
              })
            PushNotification.localNotification({
              showWhen: true, // (optional) default: true
              // autocancel: true, // (optional) default: true
              smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
              //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
              //subText: "This is a subText", // (optional) default: none
              bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
              color: "red", // (optional) default: system default
              vibrate: true, // (optional) default: true
              vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

              messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
              // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
              title: "SwasthGarbh Alert", // (optional)
              message: "Please contact your doctor at the earliest (Bleeding per vaginum, increased systolic and diastolic BP)", // (required)
              userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
              playSound: false, // (optional) default: true
              soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

            });
          } else {
            var not_1 = "Please contact your doctor at the earliest (Bleeding per vaginum)"
            var stored_date = new Date()
            var hours = new Date().getHours();
            var minutes = new Date().getMinutes();
            var newformat = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
            fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "notification": not_1,
                "date": added_date,
                "pat_name": pat_name,
                "pat_mobile": pat_mobile,
                "doc_name": doc_name,
                "doc_mobile": doc_mobile
              })
            })
              .then(res => res.json())
              .then(async (notificationdata) => {
                console.log("notification data is ", notificationdata)
              })
            PushNotification.localNotification({
              showWhen: true, // (optional) default: true
              // autocancel: true, // (optional) default: true
              smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
              //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
              //subText: "This is a subText", // (optional) default: none
              bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
              color: "red", // (optional) default: system default
              vibrate: true, // (optional) default: true
              vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

              messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
              // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
              title: "SwasthGarbh Alert", // (optional)
              message: "Please contact your doctor at the earliest (Bleeding per vaginum)", // (required)
              userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
              playSound: false, // (optional) default: true
              soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

            });
          }
        } else if (data.dys_BP > 100 && data.sys_BP > 160) {
          var not_1 = "Please contact your doctor at the earliest (Increased systolic and diastolic BP)"
          var stored_date = new Date()
          var hours = new Date().getHours();
          var minutes = new Date().getMinutes();
          var newformat = hours >= 12 ? 'PM' : 'AM';
          hours = hours % 12;
          hours = hours ? hours : 12;
          minutes = minutes < 10 ? '0' + minutes : minutes;
          let added_date = stored_date + ' ' + hours + ':' + minutes + ' ' + newformat;
          fetch("https://swasthgarbh-backend.herokuapp.com/addnotification_adddata", {
            method: "POST",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "notification": not_1,
              "date": added_date,
              "pat_name": pat_name,
              "pat_mobile": pat_mobile,
              "doc_name": doc_name,
              "doc_mobile": doc_mobile
            })
          })
            .then(res => res.json())
            .then(async (notificationdata) => {
              console.log("notification data is ", notificationdata)
            })
          PushNotification.localNotification({
            showWhen: true, // (optional) default: true
            // autocancel: true, // (optional) default: true
            smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
            //  bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
            //subText: "This is a subText", // (optional) default: none
            bigPictureUrl: "https://www.example.tld/picture.jpg", // (optional) default: undefined
            color: "red", // (optional) default: system default
            vibrate: true, // (optional) default: true
            vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

            messageId: "google:message_id", // (optional) added as `message_id` to intent extras so opening push notification can find data stored by @react-native-firebase/messaging module. 
            // id:0, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
            title: "SwasthGarbh Alert", // (optional)
            message: "Please contact your doctor at the earliest (Increased systolic and diastolic BP)", // (required)
            userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)
            playSound: false, // (optional) default: true
            soundName: "default", // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)

          });
        }

        try {
          // await AsyncStorage.setItem('token', data.token)
          props.navigation.navigate('patient_screen')
        } catch (e) {
          console.log("error is ", e)
        }
      })
  }


  const ContentTitle = ({ title, style }) => (
    <Appbar.Content
      title={<Text style={style}> {title} </Text>}
    />
  );

  return (
    <>
      <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
      <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
        <ContentTitle title="Add data" style={{ color: 'white', fontSize: 20 }} />
      </Appbar.Header>
      <ScrollView style={{ backgroundColor: "white" }}>

        <View style={{
          flex: 1,
          padding: 10,
          marginBottom: 5,
          borderRadius: 15,
          marginTop: 5,
          marginLeft: 10,
          marginRight: 10,
          backgroundColor: "white",
          shadowColor: '#470000',
          shadowOffset: { width: 0, height: 5 },
          shadowOpacity: 0.2,
          elevation: 2
        }}>

          <View style={{ flexDirection: "row" }}>
            <Image source={bp_icon} style={{ height: 50, width: 50, marginTop: 5, marginBottom: 5, marginLeft: 5, marginRight: 10, marginHorizontal: 0 }}></Image>
            <View>
              <Text style={{ fontSize: 20, color: "gray", alignSelf: "center", marginBottom: 1 }}>Blood </Text>
              <Text style={{ fontSize: 20, color: "gray", alignSelf: "center" }}>Pressure </Text>
            </View>
            <View style={{ alignSelf: "flex-end" }}>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 15, color: "gray", left: 25, marginBottom: 15 }}>Systolic BP </Text>
                <View style={{ marginTop: -5, position: "absolute", marginLeft: 150 }}>
                  <TextInput
                    defaultValue={sys_BP}
                    onChangeText={(text) => set_Sys_BP(text)}
                    style={styles.input}></TextInput></View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 15, color: "gray", left: 25 }}>Diastolic BP </Text>
                <View style={{ marginTop: -5, position: "absolute", marginLeft: 150 }}>
                  <TextInput
                    value={dys_BP}
                    onChangeText={(text) => set_Dys_BP(text)}
                    style={styles.input}></TextInput></View>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>
            <CheckBox
              value={headache}
              onValueChange={set_headache}
              style={styles.checkbox}
            />
            <Text style={{ margin: 5 }}>Headache</Text>
          </View>

          <View style={{ flexDirection: "row" }}>
            <CheckBox
              value={abdominal}
              onValueChange={set_abdominal}
              style={styles.checkbox}
            />
            <Text style={{ margin: 5 }}>Abdominal Pain</Text>
          </View>

          <View style={{ flexDirection: "row" }}>
            <CheckBox
              value={visual}
              onValueChange={set_visual}
              style={styles.checkbox}
            />
            <Text style={{ margin: 5 }}>Visual Problems</Text>
          </View>

          <View style={{ flexDirection: "row" }}>
            <CheckBox
              value={fetal_movement}
              onValueChange={set_fetal_movement}
              style={styles.checkbox}
            />
            <Text style={{ margin: 5 }}>Decreased Fetal Movements</Text>
          </View>

          <View style={{ flexDirection: "row" }}>
            <CheckBox
              value={swelling}
              onValueChange={set_swelling}
              style={styles.checkbox}
            />
            <Text style={{ margin: 5 }}>Swelling in Hands or Face</Text>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>

            <Image source={uterus_icon} style={{ height: 50, width: 50, marginTop: 5, marginBottom: 5, marginLeft: 5, marginRight: 10, marginHorizontal: 0 }}></Image>
            <View style={{ alignSelf: "flex-end" }}>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 15, color: "gray", left: 25, marginBottom: 25 }}>Urine Albumin </Text>
                <View style={{ marginTop: -5, position: "absolute", marginLeft: 230 }}>
                  <TextInput
                    value={urine}
                    onChangeText={(text) => set_urine(text)}
                    style={styles.input} ></TextInput></View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 15, color: "gray", left: 25, }}>Bleeding per vaginum </Text>
                <View style={{ marginTop: -5, position: "absolute", marginLeft: 230 }}>
                  <TextInput
                    value={bleed}
                    onChangeText={(text) => set_bleed(text)}
                    style={styles.input} ></TextInput></View>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>
            <Image source={heartbeat_icon} style={{ height: 50, width: 50, marginTop: 5, marginBottom: 5, marginLeft: 5, marginRight: 10, marginHorizontal: 0 }}></Image>
            <Text style={{ fontSize: 20, color: "gray", left: 25, marginTop: 20 }}>Heart Rate </Text>
            <View style={{ marginTop: -5, position: "absolute", marginLeft: 260, marginTop: 15 }}>
              <TextInput placeholder="in Beats/min"
                value={heart_rate}
                onChangeText={(text) => set_heart_rate(text)}
                style={styles.input}></TextInput></View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>
            <Image source={weight_icon} style={{ height: 50, width: 50, marginTop: 5, marginBottom: 5, marginLeft: 5, marginRight: 10, marginHorizontal: 0 }}></Image>
            <Text style={{ fontSize: 20, color: "gray", left: 25, marginTop: 20 }}>Body Weight </Text>
            <View style={{ marginTop: -5, position: "absolute", marginLeft: 260, marginTop: 15 }}>
              <TextInput placeholder="in Kg"
                value={bodyweight}
                onChangeText={(text) => set_bodyweight(text)}
                style={styles.input}></TextInput></View>
          </View>
        </View>

        <View style={styles.container}>
          <Text style={{ fontSize: 15, color: "#f78da7", left: 5, marginTop: 10 }}>Extra Comments/ Complications </Text>

          <TextInput placeholder="Something you want doctor to know"
            multiline={true}
            numberOfLines={4}
            value={extracomments}
            onChangeText={(text) => set_extracomments(text)}
            style={styles.input2}></TextInput>
          <Button style={{ alignSelf: "center", padding: 5, marginTop: 30, backgroundColor: "pink", marginTop: 40, marginBottom: 10 }}
            onPress={() => alert("karenge upload")} > <Text style={{ color: "black" }}>Upload photo</Text> </Button>
        </View>

        <Button style={{ alignSelf: "center", backgroundColor: "pink", marginTop: 20, marginBottom: 10 }}
          onPress={() => addData(props)} > <Text style={{ color: "black" }}>Send</Text> </Button>


      </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    marginBottom: 5,
    borderRadius: 15,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2
  },
  input: {
    flex: 0,
    alignItems: "center",
    marginRight: 10,
    borderBottomColor: "black",
    borderTopColor: "white",
    borderRightColor: "white",
    borderLeftColor: "white",
    color: "black",
    marginTop: -2,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  input2: {
    flex: 0,
    alignItems: "center",
    marginRight: 10,
    borderBottomColor: "pink",
    borderTopColor: "white",
    borderRightColor: "white",
    borderLeftColor: "white",
    color: "black",
    marginTop: -2,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  checkbox: {
    alignSelf: "flex-start",
  }
})
export default patient_add_data