import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, Image, FlatList } from 'react-native';
import { Divider, Provider } from 'react-native-paper'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { add } from 'date-fns';


var complication_icon = require('./drawables/imagemenu.png');
var call_doc_icon = require('./drawables/calldoctor.png');
var refresh_icon = require('./drawables/reload2.png');
var WHO_icon = require('./drawables/pinkultrasound.png');
var checkup_icon = require('./drawables/checkup.png');
var logout_icon = require('./drawables/logout.png');


export default class patient_insidedoc extends Component {

  state = {
    _menu: null,
    data: []
  }

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  doc_fetchdata = async () => {
    const token = await AsyncStorage.getItem("token")
    console.log(token)
    const doctor = await fetch('https://swasthgarbh-backend.herokuapp.com/doctor', {
      headers: new Headers({
        Authorization: "Bearer " + token
      })
    })
    const doc_data = await doctor.json();
    console.log(doc_data)
  }


  fetchdata = async () => {
    const adddata = await fetch('https://swasthgarbh-backend.herokuapp.com/patientadddata_list')
    const add_data = await adddata.json();
    let arr = []
    for (var i = 0; i < add_data.length; i++) {
      if (add_data[i].pat_mob == this.props.route.params.P1.mobile) {
        console.log(add_data[i].pat_mob)
        const days = (add_data[i].date.split(" ")[0])
        add_data[i] = Object.assign({ day: days }, add_data[i]);
        const months = (add_data[i].date.split(" ")[1])
        add_data[i] = Object.assign({ month: months }, add_data[i]);
        const years = (add_data[i].date.split(" ")[2])
        add_data[i] = Object.assign({ year: years }, add_data[i]);
        const times = (add_data[i].date.split(" ")[3])
        add_data[i] = Object.assign({ time: times }, add_data[i]);
        const am_pm = (add_data[i].date.split(" ")[4])
        add_data[i] = Object.assign({ noon_mor: am_pm }, add_data[i]);
        arr.push(add_data[i])
        const pops = arr.pop();
        arr.unshift(pops)
        console.log(arr)
        this.setState({ data: arr });
      }
    }

    if (arr.length === 0) {
      for (let i = 0; i < 30; i++) {
        var sysnum = Math.floor(Math.random() * 40) + 90;
        var dysnum = Math.floor(Math.random() * 40) + 60;
        var body = Math.floor(Math.random() * 30) + 50;
        var bpv = Math.floor(Math.random() * 4) + (1);
        var urine_alb = Math.floor(Math.random() * 4) + 1;
        var date = Math.floor(Math.random() * 30) + 1;
        if (date == 1 || date == 21 || date == 31) {
          date = date + "st";
        }
        if (date == 2 || date == 22) {
          date = date + "nd";
        }
        if (date == 3 || date == 23) {
          date = date + "rd";
        }
        if (date > 3 || date < 21) {
          date = date + "th";
        }

        var months = Math.floor(Math.random() * 12) + 1;
        if (months == 1) {
          months = 'Jan'
        } if (months == 2) {
          months = 'Feb'
        } if (months == 3) {
          months = 'March'
        } if (months == 4) {
          months = 'April'
        } if (months == 5) {
          months = 'May'
        } if (months == 6) {
          months = 'June'
        } if (months == 7) {
          months = 'July'
        } if (months == 8) {
          months = 'Aug'
        } if (months == 9) {
          months = 'Sep'
        } if (months == 10) {
          months = 'Oct'
        } if (months == 11) {
          months = 'Nov'
        } if (months == 12) {
          months = 'Dec'
        }
        arr[i] = Object.assign({ sys_BP: sysnum }, arr[i]);
        arr[i] = Object.assign({ dys_BP: dysnum }, arr[i]);
        arr[i] = Object.assign({ bodyweight: body }, arr[i]);
        arr[i] = Object.assign({ urine: urine_alb }, arr[i]);
        arr[i] = Object.assign({ bleed: bpv }, arr[i]);
        arr[i] = Object.assign({ month: months }, arr[i]);
        arr[i] = Object.assign({ day: date }, arr[i]);
        arr[i] = Object.assign({ year: 2020 }, arr[i]);
      }
      console.log("after adding dummy data", arr)
      this.setState({ data: arr });

    }


  }

  componentDidMount() {
    this.fetchdata();
    this.doc_fetchdata();
  }


  logout = async () => {
    AsyncStorage.removeItem("token").then(() => {
      this.props.navigation.replace("home_screen")
    })
  }


  render(props) {
    return (

      <Provider>
        <>
          <View style={{ height: 60, backgroundColor: "#F8BBD0", }}>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ color: "white", padding: 10, fontSize: 20, fontWeight: "bold", marginTop: 10 }}>All Patients</Text>
              <Menu style={{ marginRight: 90, marginHorizontal: 90, marginTop: 10 }}
                ref={this.setMenuRef}
                button={<Text style={{ color: "white", fontWeight: "bold", fontSize: 36 }} onPress={this.showMenu}>                        ...</Text>}
              >
                <View style={{ flexDirection: "row" }}>
                  <Image source={refresh_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 12 }}></Image>
                  <MenuItem onPress={this.hideMenu}>Refresh</MenuItem>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={WHO_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 10 }}></Image>
                  <MenuItem onPress={() => this.props.navigation.navigate("anc_assist")}>WHO Tests</MenuItem>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={complication_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 10 }}></Image>
                  <MenuItem onPress={() => alert("yo")}>Complication Images</MenuItem>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={checkup_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 10 }}></Image>
                  <MenuItem onPress={() => alert("karenge")}>Details</MenuItem>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={logout_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 10 }}></Image>
                  <MenuItem onPress={() => this.logout()}>Logout</MenuItem>
                </View>

              </Menu>
            </View></View>

          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <View style={styles.container_text}>
                <Text style={{ fontSize: 15, color: "gray", marginLeft: 10 }}>Name</Text></View>
              <Text style={{ fontSize: 15, color: "gray", marginLeft: 10, position: "absolute", marginLeft: 200 }}>{this.props.route.params.P1.name}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={styles.container_text}>
                <Text style={{ fontSize: 15, color: "gray", marginLeft: 10 }}>EDD</Text></View>
              <Text style={{ fontSize: 15, color: "gray", marginLeft: 10, position: "absolute", marginLeft: 200 }}>{this.props.route.params.P1.EDD}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={styles.container_text}>
                <Text style={{ fontSize: 15, color: "gray", marginLeft: 10 }}>Email</Text></View>
              <Text style={{ fontSize: 15, color: "gray", marginLeft: 10, position: "absolute", marginLeft: 200 }}>{this.props.route.params.P1.email}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={styles.container_text}>
                <Text style={{ fontSize: 15, color: "gray", marginLeft: 10 }}>Mobile</Text></View>
              <Text style={{ fontSize: 15, color: "gray", marginLeft: 10, position: "absolute", marginLeft: 200 }}>{this.props.route.params.P1.mobile}</Text>
              <Image source={call_doc_icon} style={{ height: 20, width: 20, marginTop: 1, position: "absolute", marginLeft: 330, marginRight: 10 }}></Image>
            </View>
            <View style={styles.container_text}>
              <Text style={{ fontSize: 15, color: "gray", marginLeft: 10 }}>WHO Following</Text></View>
          </View>


          <View style={styles.container}>
            <Text style={{ fontSize: 12, color: "#f45536", alignSelf: "center", marginBottom: 10 }}>* Dummy data for educational purpose, Until patient enters data</Text>
            <View style={{ flexDirection: "row" }}>
              <View style={{ width: 10, height: 10, backgroundColor: 'green', marginBottom: 10, marginTop: 10, marginLeft: 30 }} />
              <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Systolic BP </Text>
              <View style={{ width: 10, height: 10, backgroundColor: '#68f3a3', marginBottom: 10, marginTop: 10, marginLeft: 8 }} />
              <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Diastolic BP </Text>
              <View style={{ width: 10, height: 10, backgroundColor: "#5e9ee4", marginBottom: 10, marginTop: 10, marginLeft: 8 }} />
              <Text style={{ fontSize: 10, color: "black", marginTop: 7 }}>  Weight </Text>
            </View>

            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                style={styles.my_Button}
                onPress={() => alert('hii')}>
                <Text style={styles.text}>Notify</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.my_Button}
                onPress={() => props.navigation.navigate('patient_medicine')}>
                <Text style={styles.text}>Medicine</Text>
              </TouchableOpacity>
            </View>
            <Text style={{ fontSize: 15, color: "gray", marginLeft: 6, marginTop: 10 }}>Patient's History </Text>

          </View>

          <FlatList style={{ flex: 0, backgroundColor: "white" }}
            data={this.state.data}
            keyExtractor = {(item, index) => item._id}
            renderItem={({ item, index }) => {
  
            <View>
            <Text style={{ flex: 0, marginLeft: 10, marginTop: 2, color: "gray" }}>{index + 1}</Text>
            <View style={{ flexDirection: "row" }}>
              <View>
                <Text style={styles.list3}> {item.day}</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={styles.list5}>{item.month}</Text>
                  <Text style={{ marginLeft: 5, color: "#FB5448", fontSize: 15, marginTop: 2 }}>{item.year}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ marginLeft: 30, color: "#FB5448", fontSize: 15, marginTop: 2 }}>{item.time}</Text>
                  <Text style={{ marginLeft: 5, color: "#FB5448", fontSize: 15, marginTop: 2 }}>{item.noon_mor}</Text>
                </View>
              </View>
              <View>

                <Text style={styles.list1}>Systolic BP: {item.sys_BP}</Text>
                <Text style={styles.list2}>Diastolic BP: {item.dys_BP}</Text>
                <Text style={styles.list2}>Urine Albumin: {item.urine}</Text>
                <Text style={styles.list2}>Bleeding: {item.bleed}</Text>
                <Text style={styles.list2}>Heart Rate: {item.heart_rate}</Text>
                <Text style={styles.list2}>Weight: {item.bodyweight}</Text>
                <Text style={styles.list4}>Extracomments: {item.extracomments}</Text>
              </View>
            </View>

            <Divider style={styles.divider} />
          </View>
          }
        }

        />
      </>
      </Provider>

    )
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 0,
    padding: 5,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2
  },
  container_text: {
    justifyContent: 'flex-start',
    alignItems: "flex-start",
  },
  my_Button: {
    paddingHorizontal: 50,
    paddingVertical: 10,
    marginLeft: 25,
    marginRight: 15,
    alignSelf: 'flex-start',
    backgroundColor: '#F8BBD0'
  },
  logout_Button: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    marginLeft: 5,
    marginRight: 15,
    alignSelf: 'center',
    backgroundColor: '#F8BBD0'
  },
  threedots: {
    color: "white",
    fontWeight: "bold",
    fontSize: 40
  },
  list1: {
    marginTop: 10,
    marginLeft: 10,
    color: 'grey'
  },
  list2: {
    marginTop: 2,
    marginLeft: 10,
    color: 'grey'
  },
  list3: {
    marginTop: 30,
    marginLeft: 15,
    color: '#FB5448',
    fontSize: 35
  },
  list4: {
    marginTop: 2,
    marginLeft: 10,
    marginBottom: 10,
    color: 'grey'
  },
  list5: {
    marginTop: 2,
    marginLeft: 25,
    color: '#FB5448',
    fontSize: 15
  },
  divider: {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1, },
    shadowOpacity: 1.55,
    shadowRadius: 2.84,
    elevation: 1,

  }
})

