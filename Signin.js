
import React, { useState, useEffect } from 'react';
import { Button, Appbar, TextInput, Provider } from 'react-native-paper';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Overlay from 'react-native-modal-overlay';

const Signin = (props) => {
  const [mobile, setMobile] = useState('');
  const [password, setPassword] = useState('');
  const [finalpass, setFinalpass] = useState('');
  const [visible, setVisible] = useState(true)
  const [fcmtoken , setfcmtoken] = useState()

  getFirebaseTokenFromAsyncStorage = async() => {
    console.log('coming ')
   await AsyncStorage.getItem("fcmToken", (error, result) => {
      if (result !== undefined && result !== null) {
        setfcmtoken(result)
        // this.setState({ fcmToken: result });
        console.log('login class fcm token ',result)
       
      } else {
      console.log('no firebase token genrated')
      }
    });
  };
useEffect(() => {
  getFirebaseTokenFromAsyncStorage();
}, [])


  const sendpatCred = async (props) => {
    fetch("https://swasthgarbh-backend.herokuapp.com/patient_signin", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "mobile": mobile,
        "password": password,
        "fcm_token":fcmtoken
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log('dataaaaaaa$$$$$$$$$$$$$$$$',data)
        try {
          await AsyncStorage.setItem('token', data.token)
          props.navigation.replace('/patient_screen')
        }
        catch (e) {
          console.log("patient not found")
          Alert(e)
        }
      })
  }

  const senddocCred = async (props) => {
    fetch("https://swasthgarbh-backend.herokuapp.com/doctor_signin", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "mobile": mobile,
        "password": password
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log('',data)
        try {
          await AsyncStorage.setItem('token', data.token)
          props.navigation.replace('doctor_screen')
        }
        catch (e) {
          console.log("patient not found")
          Alert(e)
        }
      })
  }

  const sendpatbydocCred = async (props) => {
    fetch("https://swasthgarbh-backend.herokuapp.com/patient_signinbydoc", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "mobile": mobile,
        "password": password
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log('my information data',data)
        try {
          await AsyncStorage.setItem('token', data.token)
          props.navigation.replace('patient_screenbydocsignin')
          console.log(token)
        }
        catch (e) {
          console.log("patient not found")
          Alert(e)
        }
      })
  }

  const onOpen = () => setVisible (true);
  const onClose = () => setVisible (false);

  return (
    <Provider>
    <>
     <Overlay visible={visible} onClose={onClose} closeOnTouchOutside>
     <View style={{ height: "50%", alignSelf: "center", width: "95%", marginLeft: 8 }}>
      <KeyboardAvoidingView behavior='position'>
        <TextInput label="Mobile number"
          value={mobile}
          onChangeText={(text) => setMobile(text)}
          style={{ marginLeft: 18, marginTop: 18, marginRight: 18 }}
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        <TextInput label="Password"
          secureTextEntry={true}
          style={{ marginLeft: 18, marginTop: 18, marginRight: 18 }}
          // value={password}
          onChangeText={(text) => {
            setPassword(text);
            setFinalpass(text)
          }
          }
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        <Text style={{ color: "grey", marginLeft: 170, marginTop: 10, fontStyle: "italic" }}>ⓘ Forgot Password</Text>


        <Button mode='contained' style={{ marginLeft: 18, marginRight: 18, marginTop: 18 }}
          onPress={() => {
            sendpatCred(props);
            senddocCred(props);
            sendpatbydocCred(props);
          }} > Sign In </Button>



      </KeyboardAvoidingView>
      </View>
      </Overlay>
    </>
    </Provider>
  );
};


export default Signin;

























