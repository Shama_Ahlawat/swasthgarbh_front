import React, { useState, useEffect } from 'react';
import { Button, Appbar } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';
import moment from 'moment';
import 'moment-timezone';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
var PushNotification = require("react-native-push-notification");
import {ActivityIndicator} from 'react-native'


import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    KeyboardAvoidingView,
    TouchableOpacity,
    TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

var blood = require('./drawables/blood.png');
var urine = require('./drawables/urine.png');
var ultra = require('./drawables/ultrasound.png');
var syringe = require('./drawables/syringe.png');


let tests = (props) => {
    let date = new Date();
    let [testlmp, set_testlmp] = useState('loading');
    let [edd, eddvalue] = useState('loading');
    let [anc1_date, set_anc1_date] = useState('loading');
    let [anc2_date, set_anc2_date] = useState('loading');
    let [anc3_date, set_anc3_date] = useState('loading');
    let [anc4_date, set_anc4_date] = useState('loading');
    let [anc5_date, set_anc5_date] = useState('loading');
    let [anc6_date, set_anc6_date] = useState('loading');
    let [anc7_date, set_anc7_date] = useState('loading');
    let [anc8_date, set_anc8_date] = useState('loading');
    let [anc1diabetes, set_anc1diabetes] = useState(false);
    let [anc1anaemia, set_anc1anaemia] = useState(false);
    let [anc1ultrasound, set_anc1ultrasound] = useState(false);
    let [anc1HIV, set_anc1HIV] = useState(false);
    let [anc1tetnus, set_anc1tetnus] = useState(false);
    let [anc1urinetest, set_anc1urinetest] = useState(false);
    let [anc2diabetes, set_anc2diabetes] = useState(false);
    let [anc2ultrasound, set_anc2ultrasound] = useState(false);
    let [anc3diabetes, set_anc3diabetes] = useState(false);
    let [anc3anaemia, set_anc3anaemia] = useState(false);
    let [anc3urinetest, set_anc3urinetest] = useState(false);
    let [anc4diabetes, set_anc4diabetes] = useState(false);
    let [anc5diabetes, set_anc5diabetes] = useState(false);
    let [anc5urinetest, set_anc5urinetest] = useState(false);
    let [anc6diabetes, set_anc6diabetes] = useState(false);
    let [anc6anaemia, set_anc6anaemia] = useState(false);
    let [anc7diabetes, set_anc7diabetes] = useState(false);
    let [anc8diabetes, set_anc8diabetes] = useState(false);
    const[loader , setLoader] = useState(false)
    //new
    const [details,setDetails] = useState([])
    const [particularMedicine,setParticulatMedicine] = useState({})
    const [is_medicine,showMedicine] = useState(false)

   useEffect(async () => {
       setLoader(true)
    const token = await AsyncStorage.getItem("token")
    //console.log(token)
    const patient = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
      headers: new Headers({
        Authorization: "Bearer " + token
      })
    })
    const patient_data = await patient.json();
    console.log('patient dddd',patient_data)
        await fetch(`https://swasthgarbh-backend.herokuapp.com/medicinelist_by?mobile=${patient_data.mobile}&doc_mob=${patient_data.mobileDoctor}`, {
        }).then(res => res.json())
          .then(data => {
            // console.log('floooooooooorrrrrrrrrrrrrr ================>>>>',data)
            setDetails(data)
            setLoader(false)
          }
          )
   }, [])

    let fetchdata = async (props) => {
    let token = await AsyncStorage.getItem("token")
        fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
            headers: new Headers({
                Authorization: "Bearer " + token
            })
        }).then(res => res.json())
            .then(data => {
                console.log(data)
                // set_testlmp(data.lmpdate)
                let setformatlmp = moment(data.lmpdate, 'DD-MM-YYYY') ///set the date format
                let mylmp = new Date(setformatlmp);
                mylmp.toString()  //convert to string
                let mylmpdate = String(mylmp).split(' '); //split the date
                let mylmpnew = (mylmpdate[2] + '/' + mylmpdate[1] + '/' + mylmpdate[3]);
                console.log("my lmp", mylmpnew)
                set_testlmp(mylmpnew)


                let setformat = moment(data.lmpdate, 'DD-MM-YYYY') ///set the date format
                console.log("my lmp changed for edd", mylmpnew)
                let myedd = new Date(setformat);
                myedd.setDate(myedd.getDate() + 282); //apply calculation
                myedd.toString()  //convert to string
                let myedddate = String(myedd).split(' '); //split the date
                let myeddnew = (myedddate[2] + '/' + myedddate[1] + '/' + myedddate[3]);
                eddvalue(myeddnew)
                // console.log("my edd", myeddnew)

                let anc1date = new Date(setformat);
                anc1date.setDate(anc1date.getDate() + 84); //apply calculation
                console.log("anc1 date is ",anc1date)
                anc1date.toString()  //convert to string
                let myanc1date = String(anc1date).split(' '); //split the date
                myanc1date = (myanc1date[2] + '/' + myanc1date[1] + '/' + myanc1date[3]);
                console.log("final anc1 date is ", myanc1date)
                set_anc1_date(myanc1date)


                let anc2date = new Date(setformat);
                anc2date.setDate(anc2date.getDate() + 140); //apply calculation
                anc2date.toString()  //convert to string
                let myanc2date = String(anc2date).split(' '); //split the date
                myanc2date = (myanc2date[2] + '/' + myanc2date[1] + '/' + myanc2date[3]);
                set_anc2_date(myanc2date)
                
                let anc3date = new Date(setformat);
                anc3date.setDate(anc3date.getDate() + 182); //apply calculation
                anc3date.toString()  //convert to string
                let myanc3date = String(anc3date).split(' '); //split the date
                myanc3date = (myanc3date[2] + '/' + myanc3date[1] + '/' + myanc3date[3]);
                set_anc3_date(myanc3date)
                
                let anc4date = new Date(setformat);
                anc4date.setDate(anc4date.getDate() + 210); //apply calculation
                anc4date.toString()  //convert to string
                let myanc4date = String(anc4date).split(' '); //split the date
                myanc4date = (myanc4date[2] + '/' + myanc4date[1] + '/' + myanc4date[3]);
                set_anc4_date(myanc4date)

                let anc5date = new Date(setformat);
                anc5date.setDate(anc5date.getDate() + 238); //apply calculation
                anc5date.toString()  //convert to string
                let myanc5date = String(anc5date).split(' '); //split the date
                myanc5date = (myanc5date[2] + '/' + myanc5date[1] + '/' + myanc5date[3]);
                set_anc5_date(myanc5date)
               

                let anc6date = new Date(setformat);
                anc6date.setDate(anc6date.getDate() + 252); //apply calculation
                anc6date.toString()  //convert to string
                let myanc6date = String(anc6date).split(' '); //split the date
                myanc6date = (myanc6date[2] + '/' + myanc6date[1] + '/' + myanc6date[3]);
                set_anc6_date(myanc6date)
                

                let anc7date = new Date(setformat);
                anc7date.setDate(anc7date.getDate() + 266); //apply calculation
                anc7date.toString()  //convert to string
                let myanc7date = String(anc7date).split(' '); //split the date
                myanc7date = (myanc7date[2] + '/' + myanc7date[1] + '/' + myanc7date[3]);
                set_anc7_date(myanc7date)
                

                let anc8date = new Date(setformat);
                anc8date.setDate(anc8date.getDate() + 282); //apply calculation
                anc8date.toString()  //convert to string
                let myanc8date = String(anc8date).split(' '); //split the date
                myanc8date = (myanc8date[2] + '/' + myanc8date[1] + '/' + myanc8date[3]);
                set_anc8_date(myanc8date)
               
            }
            )

    }


    let ancdata = async () => {
        let token = await AsyncStorage.getItem("token")
        let patcred = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
            headers: new Headers({
                Authorization: "Bearer " + token
            })
        })
        let pat = await patcred.json();
        let pat_mob = (pat.mobile)
        //console.log (pat_mob)
        let n = []
        let anc = await fetch('https://swasthgarbh-backend.herokuapp.com/ancdatalist')
        // console.log ("hit")
        let anc_data = await anc.json();
        for (let i = 0; i < anc_data.length; i++) {
            if (anc_data[i].mobile == pat_mob) {
                console.log("match mobile")
                n.push(anc_data[i])
                console.log["whats there", n]
                console.log("update anc data")
                let toke = await AsyncStorage.getItem("token")
                fetch("https://swasthgarbh-backend.herokuapp.com/anc_data/" + anc_data[i]._id, {
                    method: "PUT",
                    headers: {
                        Authorization: "Bearer " + toke,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "anc1diabetes": anc1diabetes,
                        "anc1anaemia": anc1anaemia,
                        "anc1ultrasound": anc1ultrasound,
                        "anc1HIV": anc1HIV,
                        "anc1tetnus": anc1tetnus,
                        "anc1urinetest": anc1urinetest,
                        "anc2diabetes": anc2diabetes,
                        "anc2ultrasound": anc2ultrasound,
                        "anc3diabetes": anc3diabetes,
                        "anc3anaemia": anc3anaemia,
                        "anc3urinetest": anc3urinetest,
                        "anc4diabetes": anc4diabetes,
                        "anc5diabetes": anc5diabetes,
                        "anc5urinetest": anc5urinetest,
                        "anc6diabetes": anc6diabetes,
                        "anc6anaemia": anc6anaemia,
                        "anc7diabetes": anc7diabetes,
                        "anc8diabetes": anc8diabetes,
                        "mobile": pat_mob
                    })
                })
                props.navigation.navigate('patient_screen')

            }
        }
        console.log("inside it", n)
        if (n.length === 0) {
            console.log("posting anc data")
            let toke = await AsyncStorage.getItem("token")
            fetch("https://swasthgarbh-backend.herokuapp.com/anc_data", {
                method: "POST",
                headers: {
                    Authorization: "Bearer " + toke,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "anc1_date": myanc1date,
                    "anc2_date": myanc2date,
                    "anc3_date": myanc3date,
                    "anc4_date": myanc4date,
                    "anc5_date": myanc5date,
                    "anc6_date": myanc6date,
                    "anc7_date": myanc7date,
                    "anc8_date": myanc8date,
                    "anc1diabetes": anc1diabetes,
                    "anc1anaemia": anc1anaemia,
                    "anc1ultrasound": anc1ultrasound,
                    "anc1HIV": anc1HIV,
                    "anc1tetnus": anc1tetnus,
                    "anc1urinetest": anc1urinetest,
                    "anc2diabetes": anc2diabetes,
                    "anc2ultrasound": anc2ultrasound,
                    "anc3diabetes": anc3diabetes,
                    "anc3anaemia": anc3anaemia,
                    "anc3urinetest": anc3urinetest,
                    "anc4diabetes": anc4diabetes,
                    "anc5diabetes": anc5diabetes,
                    "anc5urinetest": anc5urinetest,
                    "anc6diabetes": anc6diabetes,
                    "anc6anaemia": anc6anaemia,
                    "anc7diabetes": anc7diabetes,
                    "anc8diabetes": anc8diabetes,
                    "mobile": pat_mob
                })
            })
                .then(res => res.json())
                .then(async (data) => {
                    // console.log(data)
                    try {
                        props.navigation.navigate('patient_screen')
                    } catch (e) {
                        console.log("error is ", e)
                    }
                })
        }
    }


    let fetchanc = async () => {
        let tok = await AsyncStorage.getItem("token")
        let patcred = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
            headers: new Headers({
                Authorization: "Bearer " + tok
            })
        })
        let pat = await patcred.json();
        let pat_mob = (pat.mobile)
        // console.log(pat_mob)

        let anc = await fetch('https://swasthgarbh-backend.herokuapp.com/ancdatalist')
        let anc_data = await anc.json();
        //console.log ("total anc data", anc_data)
        for (let i = 0; i < anc_data.length; i++) {
            if (anc_data[i].mobile == pat_mob) {
                //console.log("get anc data", anc_data[i])
                if (anc_data[i].anc1anaemia) {
                    set_anc1anaemia(anc_data[i].anc1anaemia)
                } else {
                    set_anc1anaemia(false)
                }
                if (anc_data[i].anc1diabetes) {
                    set_anc1diabetes(anc_data[i].anc1diabetes)
                } else {
                    set_anc1diabetes(false)
                }
                if (anc_data[i].anc1ultrasound) {
                    set_anc1ultrasound(anc_data[i].anc1ultrasound)
                } else {
                    set_anc1ultrasound(false)
                }
                if (anc_data[i].anc1HIV) {
                    set_anc1HIV(anc_data[i].anc1HIV)
                } else {
                    set_anc1HIV(false)
                }
                if (anc_data[i].anc1tetnus) {
                    set_anc1tetnus(anc_data[i].anc1tetnus)
                } else {
                    set_anc1tetnus(false)
                }
                if (anc_data[i].anc1urinetest) {
                    set_anc1urinetest(anc_data[i].anc1urinetest)
                } else {
                    set_anc1urinetest(false)
                }
                if (anc_data[i].anc2diabetes) {
                    set_anc2diabetes(anc_data[i].anc2diabetes)
                } else {
                    set_anc2diabetes(false)
                }
                if (anc_data[i].anc2ultrasound) {
                    set_anc2ultrasound(anc_data[i].anc2ultrasound)
                } else {
                    set_anc2ultrasound(false)
                }
                if (anc_data[i].anc3diabetes) {
                    set_anc3diabetes(anc_data[i].anc3diabetes)
                } else {
                    set_anc3diabetes(false)
                }
                if (anc_data[i].anc3anaemia) {
                    set_anc3anaemia(anc_data[i].anc3anaemia)
                } else {
                    set_anc3anaemia(false)
                }
                if (anc_data[i].anc3urinetest) {
                    set_anc3urinetest(anc_data[i].anc3urinetest)
                } else {
                    set_anc3urinetest(false)
                }
                if (anc_data[i].anc4diabetes) {
                    set_anc4diabetes(anc_data[i].anc4diabetes)
                } else {
                    set_anc4diabetes(false)
                }
                if (anc_data[i].anc5diabetes) {
                    set_anc5diabetes(anc_data[i].anc5diabetes)
                } else {
                    set_anc5diabetes(false)
                }
                if (anc_data[i].anc5urinetest) {
                    set_anc5urinetest(anc_data[i].anc5urinetest)
                } else {
                    set_anc5urinetest(false)
                }
                if (anc_data[i].anc6diabetes) {
                    set_anc6diabetes(anc_data[i].anc6diabetes)
                } else {
                    set_anc6diabetes(false)
                }
                if (anc_data[i].anc6anaemia) {
                    set_anc6anaemia(anc_data[i].anc6anaemia)
                } else {
                    set_anc6anaemia(false)
                }
                if (anc_data[i].anc7diabetes) {
                    set_anc7diabetes(anc_data[i].anc7diabetes)
                } else {
                    set_anc7diabetes(false)
                }
                if (anc_data[i].anc8diabetes) {
                    set_anc8diabetes(anc_data[i].anc8diabetes)
                } else {
                    set_anc8diabetes(false)
                }
            }
        }

    }

    console.log('details $$$$$$$$',details)

    useEffect(() => {
        fetchdata();
        fetchanc();
    }, [])

    let ContentTitle = ({ title, style }) => (
        <Appbar.Content
            title={<Text style={style}> {title} </Text>}
        />
    );

    const openParticularMedicine = (medicine) => {
          setParticulatMedicine(medicine)
          showMedicine(true)
    }
    const converter = (str) => {
        var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
      return [date.getFullYear(), mnth, day].join("-");
    }

    const deleteMedicine = async () => {
        // particularMedicine._id
        setLoader(true)
        const token = await AsyncStorage.getItem("token")
        fetch(`https://swasthgarbh-backend.herokuapp.com/delete_medicine?id=${particularMedicine._id}`, {
          method: "DELETE",
          headers: {
            'Content-Type': 'application/json',
             Authorization: " secretkey " + token
          },
          
    })
    .then(res => res.json())
        .then(async (data) => {
          showMedicine(false)
          const newarr = details.filter(function(item) {
            return item._id !== particularMedicine._id
        })
        setDetails(newarr)
        setLoader(false)
        })
   }

    return (
        <>
            <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
            <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
                <ContentTitle title="Medicine lists" style={{ flexDirection:'row',alignItems:'center',color: 'white', fontSize: 20 }} />
            </Appbar.Header>
            <ScrollView style={{ backgroundColor: "white" }}>
               {!is_medicine && <View>
                    <Text style={{fontSize:15,color:'grey',marginLeft:20,marginTop:20}}>Active meds</Text>
                    <View>
                     {details.map((ele) => {
                               return <View  onStartShouldSetResponder={() =>
                                openParticularMedicine(ele)
                              }>

                                   {/* <TouchableOpacity
                             onPress={(ele) => openParticularMedicine(ele.med_name) }> */}
  
                                 <Text style={{padding:10,marginTop:20,marginLeft:20,marginRight:20,fontSize:14,backgroundColor:'#F8BBD0',color:'white',borderBottom:1,borderBottomColor:'white'}}>{ele.med_name}</Text>
                                {/* </TouchableOpacity> */}
                                </View>
                     })}
                    </View>
                </View>}
                {is_medicine && 
                <View>
                  <View style={{backgroundColor:'#F8BBD0',height:150}}>
                  <Text  style={{color:'white',fontSize:30,paddingLeft:20,marginTop:20}}>{particularMedicine.med_name}</Text>
               </View>
               <View style={{marginLeft:20,marginRight:20}}>
                   <View style={{marginTop:20,marginBottom:20,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                   <Text style={{color:'grey'}}>Reminders</Text>
                    <View style={{color:'#F8BBD0',borderWidth:1,borderColor:'#F8BBD0',padding:5}}><Text style={{color:'#F8BBD0'}} onPress={deleteMedicine}>Delete</Text></View>
                   </View>
                   <Text style={{marginTop:10,marginBottom:10}}>{`${particularMedicine.schedule} , take from ${converter(particularMedicine.from_date)} to ${converter(particularMedicine.to_date)}`}</Text>
                   {particularMedicine.alarm_timer.map((ele) => {
                       return <Text style={{marginBottom:5}}>{`${ele} take ${particularMedicine.med_quantity}`}</Text>
                   })}
                   <Text style={{marginTop:20,marginBottom:20,color:'grey'}}>Condition</Text>
                   <Text>{particularMedicine.med_reason}</Text>
               </View>
               </View>
                }

{
                 loader &&
                <ActivityIndicator size="large" color="#F8BBD0" />
                }

{/* 
                <View style={{ flexDirection: "row" }}>
                    <Text style={{ marginLeft: 80, marginTop: 10, color: "red", fontSize: 15 }}>LMP</Text>
                    <Text style={{ marginLeft: 160, marginTop: 10, color: "red", fontSize: 15 }}>EDD</Text>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>

                    <TextInput
                        defaultValue={testlmp}
                        onChangeText={() => set_testlmp()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: 60, color: "black" }}></TextInput>
                    <TextInput
                        defaultValue={edd}
                        onChangeText={() => eddvalue()}
                        editable={false}
                        style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: 40, color: "black" }}></TextInput>
                </View>

                <View style={styles.container}>
                    <View style={{ flexDirection: "row", marginTop: 5 }}>
                        <Text style={{ fontStyle: 'italic', color: 'red', fontSize: 18, marginLeft: 5 }} > ANC 1</Text>
                        <Image source={blood} style={{ height: 30, width: 30, marginLeft: 30 }}></Image>
                        <Image source={urine} style={{ height: 30, width: 30, marginLeft: 10 }}></Image>
                        <Image source={syringe} style={{ height: 30, width: 30, marginLeft: 10 }}></Image>
                        <Image source={ultra} style={{ height: 30, width: 30, marginLeft: 10 }}></Image>
                        <View style={{ marginLeft: 35 }}>
                            <Text style={{ color: "red", marginBottom: 2 }}>12 Weeks -</Text>
                            <TextInput
                                defaultValue={anc1_date}
                                onChangeText={() => set_anc1_date()}
                                editable={false}
                                style={{ flex: 0, alignItems: "center", padding: 2, color: "red" }}></TextInput>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc1diabetes} onValueChange={set_anc1diabetes}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 9 }}>Diabetes</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc1anaemia} onValueChange={set_anc1anaemia}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 8 }}>Anaemia</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc1HIV} onValueChange={set_anc1HIV}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 8 }}>HIV</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc1ultrasound} onValueChange={set_anc1ultrasound}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 8 }}>Ultrasound</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc1tetnus} onValueChange={set_anc1tetnus}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 8 }}>Tetnus</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc1urinetest} onValueChange={set_anc1urinetest}
                            style={{ alignSelf: "flex-start", marginTop: 2, marginBottom: 5 }} />
                        <Text style={{ marginLeft: 5, marginTop: 8, marginBottom: 5 }}>Urine Test</Text>
                    </View>
                </View>


                <View style={styles.container}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontStyle: 'italic', color: 'red', fontSize: 18, marginLeft: 5 }} > ANC 2</Text>
                        <Image source={blood} style={{ height: 30, width: 30, marginLeft: 30 }}></Image>
                        <Image source={ultra} style={{ height: 30, width: 30, marginLeft: 10 }}></Image>
                        <View style={{ flexDirection: "row", marginLeft: 45 }}>
                            <Text style={{ color: "red", marginTop: 6 }}>20 Weeks -</Text>
                            <TextInput
                                defaultValue={anc2_date}
                                onChangeText={() => set_anc2_date()}
                                editable={false}
                                style={{ flex: 0, alignItems: "center", padding: 2, color: "red" }}></TextInput>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc2diabetes} onValueChange={set_anc2diabetes}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 9 }}>Diabetes</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc2ultrasound} onValueChange={set_anc2ultrasound}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 7, marginBottom: 10 }}>Ultrasound</Text>
                    </View>
                </View>

                <View style={styles.container}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontStyle: 'italic', color: 'red', fontSize: 18, marginLeft: 5 }} > ANC 3</Text>
                        <Image source={blood} style={{ height: 30, width: 30, marginLeft: 30 }}></Image>
                        <Image source={urine} style={{ height: 30, width: 30, marginLeft: 10 }}></Image>
                        <View style={{ flexDirection: "row", marginLeft: 45 }}>
                            <Text style={{ color: "red", marginTop: 6 }}>26 Weeks -</Text>
                            <TextInput
                                defaultValue={anc3_date}
                                onChangeText={() => set_anc3_date()}
                                editable={false}
                                style={{ flex: 0, alignItems: "center", padding: 2, color: "red" }}></TextInput>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc3diabetes} onValueChange={set_anc3diabetes}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 9 }}>Diabetes</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc3anaemia} onValueChange={set_anc3anaemia}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 7 }}>Anaemia</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc3urinetest} onValueChange={set_anc3urinetest}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 7, marginBottom: 10 }}>Urine Test</Text>
                    </View>
                </View>

                <View style={styles.container}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontStyle: 'italic', color: 'red', fontSize: 18, marginLeft: 5 }} > ANC 4</Text>
                        <Image source={blood} style={{ height: 30, width: 30, marginLeft: 30 }}></Image>
                        <View style={{ flexDirection: "row", marginLeft: 85 }}>
                            <Text style={{ color: "red", marginTop: 6 }}>30 Weeks -</Text>
                            <TextInput
                                defaultValue={anc4_date}
                                onChangeText={() => set_anc4_date()}
                                editable={false}
                                style={{ flex: 0, alignItems: "center", padding: 2, color: "red" }}></TextInput>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc4diabetes} onValueChange={set_anc4diabetes}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 9, marginBottom: 10 }}>Diabetes</Text>
                    </View>
                </View>

                <View style={styles.container}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontStyle: 'italic', color: 'red', fontSize: 18, marginLeft: 5 }} > ANC 5</Text>
                        <Image source={blood} style={{ height: 30, width: 30, marginLeft: 30 }}></Image>
                        <Image source={urine} style={{ height: 30, width: 30, marginLeft: 10 }}></Image>
                        <View style={{ flexDirection: "row", marginLeft: 45 }}>
                            <Text style={{ color: "red", marginTop: 6 }}>34 Weeks -</Text>
                            <TextInput
                                defaultValue={anc5_date}
                                onChangeText={() => set_anc5_date()}
                                editable={false}
                                style={{ flex: 0, alignItems: "center", padding: 2, color: "red" }}></TextInput>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc5diabetes} onValueChange={set_anc5diabetes}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 9 }}>Diabetes</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc5urinetest} onValueChange={set_anc5urinetest}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 9 }}>Urine Test</Text>
                    </View>
                </View>

                <View style={styles.container}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontStyle: 'italic', color: 'red', fontSize: 18, marginLeft: 5 }} > ANC 6</Text>
                        <Image source={blood} style={{ height: 30, width: 30, marginLeft: 30 }}></Image>
                        <View style={{ flexDirection: "row", marginLeft: 85 }}>
                            <Text style={{ color: "red", marginTop: 6 }}>36 Weeks -</Text>
                            <TextInput
                                defaultValue={anc6_date}
                                onChangeText={() => set_anc6_date()}
                                editable={false}
                                style={{ flex: 0, alignItems: "center", padding: 2, color: "red" }}></TextInput>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc6diabetes} onValueChange={set_anc6diabetes}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 9 }}>Diabetes</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc6anaemia} onValueChange={set_anc6anaemia}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 7, marginBottom: 10 }}>Anaemia</Text>
                    </View>
                </View>

                <View style={styles.container}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontStyle: 'italic', color: 'red', fontSize: 18, marginLeft: 5 }} > ANC 7</Text>
                        <Image source={blood} style={{ height: 30, width: 30, marginLeft: 30 }}></Image>
                        <View style={{ flexDirection: "row", marginLeft: 85 }}>
                            <Text style={{ color: "red", marginTop: 6 }}>38 Weeks -</Text>
                            <TextInput
                                defaultValue={anc7_date}
                                onChangeText={() => set_anc7_date()}
                                editable={false}
                                style={{ flex: 0, alignItems: "center", padding: 2, color: "red" }}></TextInput>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc7diabetes} onValueChange={set_anc7diabetes}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 10, marginBottom: 10 }}>Diabetes</Text>
                    </View>
                </View>

                <View style={styles.container}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontStyle: 'italic', color: 'red', fontSize: 18, marginLeft: 5 }} > ANC 8</Text>
                        <Image source={blood} style={{ height: 30, width: 30, marginLeft: 30 }}></Image>
                        <View style={{ flexDirection: "row", marginLeft: 85 }}>
                            <Text style={{ color: "red", marginTop: 6 }}>40 Weeks -</Text>
                            <TextInput
                                defaultValue={anc8_date}
                                onChangeText={() => set_anc8_date()}
                                editable={false}
                                style={{ flex: 0, alignItems: "center", padding: 2, color: "red" }}></TextInput>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox value={anc8diabetes} onValueChange={set_anc8diabetes}
                            style={{ alignSelf: "flex-start", marginTop: 2 }} />
                        <Text style={{ marginLeft: 5, marginTop: 9 }}>Diabetes</Text>
                    </View>
                </View> */}
{/* 
                <TouchableOpacity
                    style={styles.my_Button}
                    onPress={() => { ancdata() }}>
                    <Text style={{ color: "black" }}>Update My Data</Text>
                </TouchableOpacity> */}


            </ScrollView>
        </>


    )
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingBottom: 0,
        marginBottom: 5,
        borderRadius: 15,
        marginLeft: 2,
        marginRight: 2,
        backgroundColor: "white",
        shadowColor: '#470000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.2,
        elevation: 2
    },
    my_Button: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        marginLeft: 5,
        marginBottom: 10,
        marginRight: 15,
        marginTop: 10,
        alignSelf: 'center',
        backgroundColor: '#FAE3E9'
    },
    text_new: {
        color: 'grey',
        marginLeft: 5,
        marginRight: 10
    }
});

export default tests