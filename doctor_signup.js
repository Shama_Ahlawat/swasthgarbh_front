import React, {useState} from 'react';
import {Button, Appbar, TextInput} from 'react-native-paper';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const doctor_signup= (props) => {
    const [name,setName] = useState('');
    const [speciality,setSpeciality] = useState('')
    const [email,setEmail] = useState('');
    const [hospital,setHospital] = useState('')
    const [mobile,setMobile] = useState('');
    const [password,setPassword] = useState('')

    const sendCred = async (props) => {
      fetch("https://swasthgarbh-backend.herokuapp.com/doctor_signup", {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "mobile": mobile,
          "password": password,
          "name": name,
          "speciality": speciality,
          "hospital": hospital,
          "email": email
        })
      })
        .then(res => res.json())
        .then(async (data) => {
          console.log(data.mobile)
          try {
            await AsyncStorage.setItem('token', data.token)
            props.navigation.replace("doctor_screen")
          } catch (e) {
            console.log("error is ", e)
          }
        })
    }

    const ContentTitle = ({ title, style }) => (
      <Appbar.Content
        title={<Text style={style}> {title} </Text>}
      />
    );

  return (
    <>
    <Appbar.Header style={{backgroundColor:"#F8BBD0"}}>
      <ContentTitle title="Doctor Registration"  style={{color:'white', fontSize:20}} />
    </Appbar.Header>
    <KeyboardAvoidingView behavior='padding'>
    <StatusBar backgroundColor="#F8BBD0" barStyle = "light-content" />
   
      <TextInput label = "Name" 
       value={name}
       onChangeText = {(text)=>setName(text)}
      style= {{marginLeft:18, marginTop:18, marginRight:18, backgroundColor: "#F8BBD0" }} 
      theme ={{colors:{primary:"#F8BBD0"}}}></TextInput>

      <View style={{flexDirection:"row"}}>
      <Text style={{fontSize:18, marginTop:40, marginLeft:20}}>+91</Text>
      <TextInput label = "Mobile number" 
      value={mobile}
      onChangeText = {(text)=>setMobile(text)}
      placeholder = "+91"
      style= {{flex:1,marginLeft:18, marginTop:18, marginRight:18, backgroundColor: "#F8BBD0" }} 
      theme ={{colors:{primary:"#F8BBD0"}}}></TextInput>
      </View>

      <TextInput label = "Password" 
      secureTextEntry={true}
      style= {{marginLeft:18, marginTop:18, marginRight:18, backgroundColor: "#F8BBD0" }} 
      value={password}
      onChangeText = {(text)=>setPassword(text)}
      theme ={{colors:{primary:"#F8BBD0"}}}></TextInput>

      <TextInput label = "Speciality" 
       value={speciality}
       onChangeText = {(text)=>setSpeciality(text)}
       style= {{marginLeft:18, marginTop:18, marginRight:18, backgroundColor: "#F8BBD0" }} 
       theme ={{colors:{primary:"#F8BBD0"}}}></TextInput>

      <TextInput label = "Email" 
       value={email}
       onChangeText = {(text)=>setEmail(text)}
      style= {{marginLeft:18, marginTop:18, marginRight:18, backgroundColor: "#F8BBD0" }} 
      theme ={{colors:{primary:"#F8BBD0"}}}></TextInput>

      <TextInput label = "Hospital" 
       value={hospital}
       onChangeText = {(text)=>setHospital(text)}
      style= {{marginLeft:18, marginTop:18, marginRight:18, backgroundColor: "#F8BBD0" }} 
      theme ={{colors:{primary:"#F8BBD0"}}}></TextInput>

      <View style={styles.container}>
              <TouchableOpacity style={styles.myButton}
              onPress = {() => sendCred(props)}>
                <Text style={styles.text}>Register</Text>
              </TouchableOpacity>
            </View>
    </KeyboardAvoidingView>
    
    </>
  );
};

const styles = StyleSheet.create({
    container_appbar:{
        flex: 1,
        backgroundColor: "#F8BBD0",
        alignSelf:"flex-end",
        paddingLeft:15
    },
    text_appbar:{
        fontSize:20 
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',  
        marginTop:35
      },
    text:{
      color:'black',
      fontWeight: "bold",
      fontSize: 15
      },
      myButton:{
        paddingHorizontal:40,
        paddingVertical:10,
        backgroundColor:"#F8BBD0"
        
      }
})


export default doctor_signup;

























