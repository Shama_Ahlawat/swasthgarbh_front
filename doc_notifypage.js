import React, { Component, useEffect, useState } from 'react';
import { AppRegistry, StyleSheet, Text, View, Image, FlatList, ScrollView, StatusBar } from 'react-native';
import { Divider, Provider, Appbar } from 'react-native-paper'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';

const doc_notifypage = (props) => {
        const [data, setdata] = useState([]);

        const ContentTitle = ({ title, style }) => (
                <Appbar.Content
                  title={<Text style={style}> {title} </Text>}
                />
              );

        const fetchnotification = async () => {
                const tok = await AsyncStorage.getItem("token")
                const doccred = await fetch('https://swasthgarbh-backend.herokuapp.com/doctor', {
                        headers: new Headers({
                                Authorization: "Bearer " + tok
                        })
                })
                const doc = await doccred.json();
                const arr = []
                const allnotification = await fetch('https://swasthgarbh-backend.herokuapp.com/patientnotification_list')
                const notify = await allnotification.json();

                for (var i = 0; i < notify.length; i++) {
                        if (notify[i].doc_mobile == doc.mobile) {
                                var noti = notify[i].notification
                                var final_not =  noti.split("(")
                                //console.log(final_not[0], " and ", final_not[1])
                                var final_noti = final_not[1].split(")")
                                notify[i] = Object.assign({ notif_fordoc: final_noti }, notify[i]);
                                arr.push(notify[i])
                                const pops = arr.pop();
                                arr.unshift(pops)
                                console.log(arr)
                        }
                }

                setdata(arr)
        }

        useEffect(() => {
                fetchnotification();
        }, [])

        return (
                <ScrollView>
                <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />

                <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
                  <ContentTitle title="Notifications" style={{ color: 'white', fontSize: 20 }} />
                </Appbar.Header>
               
                        <FlatList style={{ flex: 0, backgroundColor: "white" }}
                                data={data}
                                // maxToRenderPerBatch={10}
                                keyExtractor={(item, index) => item._id}
                                renderItem={({ item, index }) =>
                                        <View>
                                                <Text style={{ marginTop: 15, color: "gray", fontSize: 18, marginLeft: 5, marginBottom: 15 }}>{item.pat_name} (mobile no. {item.pat_mobile}) critical: {item.notif_fordoc}</Text>

                                                <Divider style={styles.divider} />
                                        </View>
                                }
                        />

                </ScrollView>

        )

}

const styles = StyleSheet.create({
        divider: {
                shadowColor: "#000",
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 1.55,
                shadowRadius: 2.84,
                elevation: 2

        }
})
export default doc_notifypage