import React, { Component, useState } from 'react';
import { Button, Appbar, TextInput, Menu, Divider, Provider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import Modal from 'react-native-modal';
import DatePicker from 'react-native-datepicker';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import {
  SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Image,
  KeyboardAvoidingView,
  TouchableOpacity, Switch
} from 'react-native';
import Overlay from 'react-native-modal-overlay';

var plus_icon = require('./drawables/hospital.png');
export default class doctor_medicine extends Component {

  constructor(props) {
    super(props)
    this.state = {
      date: "",
      data: ['Daily', 'Weekly', 'Monthly'],
      checked: 0,
      modalVisible: false,
      med_name: '',
      from_date: '',
      to_date: '',
      extra_med: '',
      sos_switch: false,
      show: true
    }
  }

  ShowHideComponent = () => {

    (sos_switch) => this.setState({ sos_switch })

    if (this.state.show == true) {
      this.setState({ show: false });
    } else {
      this.setState({ show: true });
    }
  };

  handleMedname = (text) => {
    this.setState({ med_name: text })
  }
  handleFromdate = (text) => {
    this.setState({ from_date: text })
  }
  handleTodate = (text) => {
    this.setState({ to_date: text })
  }
  handleExtracomment = (text) => {
    this.setState({ extra_med: text })
  }
  onClose = () => this.setState({ modalVisible: false });
  onOpen = () => this.setState({ modalVisible: true });

  render() {
    return (


      <View style={{
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
        backgroundColor: "#F8BBD0",
        alignSelf: "flex-end", marginTop: 550, marginRight: 20,
        shadowColor: '#470000',
        shadowOffset: { width: 1, height: 6 },
        shadowOpacity: 1,
        elevation: 8
      }} >
       

        <TouchableOpacity onPress={() => this.onOpen()}>
          <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
          <Image source={plus_icon} style={{ height: 30, width: 30, alignSelf: "center", marginTop: 15 }}></Image>
        </TouchableOpacity>


        <Overlay visible={this.state.modalVisible} onClose={this.onClose} closeOnTouchOutside>

          <View style={{ height: 50, alignSelf: "flex-start", width: "95%", marginLeft: 8 }}>
            <TextInput placeholder="Medicine Name"
              onChangeText={this.handleMedname}
              style={{ flex: 1, flexDirection: "column", alignItems: "center", backgroundColor: "white" }}></TextInput>
          </View>


          {this.state.show ? (
            <View style={{ flexDirection: "row" }}>
              <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                <Text style={{ color: "grey", fontSize: 18, marginTop: 20, marginRight: 60, }}>From</Text>
              </View>
              <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                <DatePicker style={{ width: "100%", marginTop: 15, marginLeft: 130 }}
                  date={this.state.from_date}
                  placeholder="Date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput: {
                      marginLeft: 36
                    }
                  }} onDateChange={(date) => { this.setState({ from_date: date }) }} >
                </DatePicker>
              </View>
            </View>
          ) : null}

          {this.state.show ? (
            <View style={{ flexDirection: "row", marginHorizontal: 100 }}>
              <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                <Text style={{ color: "grey", fontSize: 18, marginTop: 20, marginRight: 80, }}>To</Text>
              </View>
              <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                <DatePicker style={{ width: "100%", marginTop: 15, marginLeft: 130 }}
                  date={this.state.to_date}
                  placeholder="Date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput: {
                      marginLeft: 36
                    }
                  }} onDateChange={(date) => { this.setState({ to_date: date }) }} >
                </DatePicker>
              </View>
            </View>
          ) : null}

          <View style={{ flexDirection: "row", marginTop: 20 }}>
            <Text style={{ marginTop: 5, marginLeft: 10 }}>SOS</Text>
            <Switch
              value={this.state.sos_switch}
              onValueChange={(sos_switch) => {
                this.setState({ sos_switch });
                this.ShowHideComponent();
              }}
              trackColor={{ true: 'pink', false: 'grey' }}
              thumbColor='pink'
              style={{ flex: 1 }}
            ></Switch>
          </View>

          {this.state.show ? (
            <View style={{ flexDirection: "row" }}>
              {
                this.state.data.map((data, key) => {
                  return (
                    <View>
                      {this.state.checked == key ?
                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginHorizontal: 15 }}>
                          <Image style={{ height: 25, width: 25, marginTop: 20, marginRight: 5 }} source={require('./drawables/radio.png')}></Image>
                          <Text style={{ marginRight: 15, marginTop: 20, fontSize: 15 }}>{data}</Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={() => { this.setState({ checked: key }) }} style={{ flexDirection: "row", alignItems: "center", marginHorizontal: 15 }}>
                          <Image style={{ height: 25, width: 25, marginTop: 20, marginRight: 5 }} source={require('./drawables/radio_button.png')}></Image>
                          <Text style={{ marginRight: 15, marginTop: 20, fontSize: 15 }}>{data}</Text>
                        </TouchableOpacity>
                      }
                    </View>
                  )
                })
              }
            </View>) : null}


          <View style={{ height: 50, alignSelf: "flex-start", width: "95%", marginLeft: 8, marginTop: 20 }}>
            <TextInput placeholder="Extra Comments"
              onChangeText={this.handleExtracomment}
              multiline={true}
              style={{ flex: 1, flexDirection: "column", alignItems: "center", backgroundColor: "white" }}></TextInput>
          </View>
          <TouchableOpacity
            style={{ alignSelf: "center", backgroundColor: "pink", marginTop: 20 }}
            onPress={() => alert("medicine added")}>
            {/* // onPress= {this.ShowHideComponent}> */}
            <Text style={{ color: "black", padding: 15 }}>Add Medicine</Text>
          </TouchableOpacity>

        </Overlay>

      </View>
    );
  }
}


// const styles = StyleSheet.create({
//   container: {
//     flex: 0,
//     borderRadius: 15,
//     backgroundColor: "#F8BBD0",
//     // shadowColor: '#470000',
//     // shadowOffset: { width: 0, height: 5 },
//     // shadowOpacity: 0.2,
//     // elevation: 2
//   }

// })

