import React,{Component, useState} from 'react';
import MapView, {Marker} from 'react-native-maps';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete/GooglePlacesAutocomplete';

import {
    View,
    Text,
    StatusBar,
    Image,
    KeyboardAvoidingView,
    TouchableOpacity,
    StyleSheet
  } from 'react-native';

 

const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

export default class map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      region: {
      latitude: 10,
      longitude: 10,
      latitudeDelta: 0.001,
      longitudeDelta: 0.001
    }
    };
  }
    render(){
    return(
      <>
      {/* <StatusBar backgroundColor="#F8BBD0" barStyle = "light-content" /> */}

      {/* <View style = {{margin:10}}> */}
      <GooglePlacesAutocomplete
      keyboardShouldPersistTaps='always'
    
      onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        console.log(data, details);
      }}
      query={{
        key: 'AIzaSyC3dEkrrzVAgFerUH7vx0aQTciWf8aINCo',
        language: 'en',
        types:"hospital"
      }}
    />
    {/* </View> */}

        <View style={styles.MainContainer}>  
         <MapView  
          style={styles.mapStyle}  
          showsUserLocation={true} 
          onMapReady={this.onMapReady} 
          zoomEnabled={false}  
          zoomControlEnabled={true}  
          initialRegion={this.state.region}
          onRegionChangeComplete={this.onRegionChange}
          // initialRegion={{  
          //   latitude: 29.864940,   
          //   longitude: 77.896480,  
          //   latitudeDelta: 0.0922,  
          //   longitudeDelta: 0.0421,  
          // }}
          >
          <MapView.Marker  
            coordinate={{ "latitude": this.state.region.latitude,   
    "longitude": this.state.region.longitude }} 
            title={"IIT Roorkee"}  
          
          />  
        </MapView> 
            </View>
            </>
    )}}
const styles = StyleSheet.create({ 
    MainContainer: {  
        position: 'absolute',  
        top: 55,  
        left: 0,  
        right: 0,  
        bottom: 0,  
        // alignItems: 'center',  
        justifyContent: 'flex-end',  
      },  
mapStyle: {  
    position: 'absolute',  
    top: 0,  
    left: 0,  
    right:0,  
    bottom: 0,  
  }
})

