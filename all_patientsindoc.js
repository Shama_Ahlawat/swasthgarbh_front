import React, { useState, useEffect } from 'react';
import { Button, Appbar, TextInput, Menu, Divider, Provider, FlatList} from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import moment from 'moment';
import 'moment-timezone';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    KeyboardAvoidingView,
    TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


const all_patientsindoc = (props) => {
    const [edd, edd_value] = useState('');
    const [name, setName] = useState('');
    const [visible, setVisible] = React.useState(false);
    //   const [docmob, set_docmob] = useState('')
    //   const [doc_name, set_doc_name] = useState('')
    //   const [doc_mob, set_doc_mob] = useState('')

    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);

    var refresh_icon = require('./drawables/reload2.png');
    var logout_icon = require('./drawables/logout.png');

    const logout = async () => {
        AsyncStorage.removeItem("token").then(() => {
            props.navigation.replace("home_screen")
        })
    }


    const fetchdata = async () => {
        const token = await AsyncStorage.getItem("token")
        Promise.all([
            await fetch('https://swasthgarbh-backend.herokuapp.com/patientlist'),
            await fetch('https://swasthgarbh-backend.herokuapp.com/doctor', {
                headers: new Headers({
                    Authorization: "Bearer " + token
                }),
            })
        ]).then(([patdata, docdata]) => Promise.all([patdata.json(), docdata.json()]))
            .then(([patdata, docdata]) => {
                for (var i = 0; i < patdata.length; i++) {
                    if (patdata[i].mobileDoctor) {
                        if ((patdata[i].mobileDoctor) == docdata.mobile) {
                            count = count + 1
                            console.log(count)
                            const names_patient =(patdata[i].name) 
                            // setName({id: names_patient, title: i})
                        }
                    }
                }

            })
    }
    useEffect(() => {
        fetchdata()
    }, [])

    const renderItem = ({ item }) => (
        <Item title={item.title} />
    );

    return (
        <>
            <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
            <Provider>
                <View style={{ height: 60, backgroundColor: "#F8BBD0", }}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ color: "white", padding: 10, fontSize: 20, fontWeight: "bold", marginTop: 10 }}>All Patients</Text>
                        <Menu
                            style={{ marginRight: 200, marginHorizontal: 120, marginTop: 10 }}
                            visible={visible}
                            onDismiss={closeMenu}
                            anchor={<TouchableOpacity
                                onPress={openMenu} >
                                <Text style={styles.threedots}>                       ...</Text>
                            </TouchableOpacity>}>
                            <View style={{ flexDirection: "row" }}>
                                <Image source={refresh_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5 }}></Image>
                                <Menu.Item style={{ marginTop: -10 }} onPress={() => { }} title="Refresh" />
                            </View>
                            <View style={{ flexDirection: "row" }}>
                                <Image source={logout_icon} style={{ height: 30, width: 30, marginHorizontal: 0, marginLeft: 5, marginTop: 5 }}></Image>
                                <Menu.Item style={{ marginTop: -5 }} onPress={() => logout()} title="Logout" />
                            </View>
                        </Menu>
                    </View>
                </View>
                <View style={styles.container}>
                    <FlatList
                        data={names_patient}
                        renderItem={(fetchdata)=>{
                            console.log(names_patient)
                        }}
                     
                    />
                </View>




            </Provider>

        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 0,
        padding: 10,
        paddingBottom: 0,
        marginBottom: 5,
        borderRadius: 15,
        marginLeft: 2,
        marginRight: 2,
        marginTop: 5,
        backgroundColor: "white",
        shadowColor: '#470000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.2,
        elevation: 2
    },
    threedots: {
        color: "white",
        fontWeight: "bold",
        fontSize: 40
    }
})

export default all_patientsindoc;