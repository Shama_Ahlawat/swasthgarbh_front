import React, { Component, useState } from 'react';
import { Button, Appbar, TextInput, Dialog, Portal, Provider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import Modal from 'react-native-modal';
import Toast from 'react-native-simple-toast';
import DatePicker from 'react-native-datepicker';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import {
  SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Image,
  KeyboardAvoidingView, TouchableHighlight,
  TouchableOpacity, Switch
} from 'react-native';
import Overlay from 'react-native-modal-overlay';
import overlay_medicine from './overlay_medicine';

var plus_icon = require('./drawables/hospital.png');
var dustbin_icon = require('./drawables/dustbin.png')
export default class patient_medicine extends Component {
  state = {
    med_data: [],

  }

  constructor(props) {
    super(props)
    this.state = {
      data: ['Daily', 'Weekly', 'Monthly'],
      checked: 0,
      modalVisible: false,
      medicine_name: '',
      from_date: '',
      to_date: '',
      extra_med: '',
      sos_switch: false,
      show: true,
      mobile: '',
      doc_mob: '',
      period: '',
      updateVisible: false,
      isDialogVisible: false
    }
  }



  sendmed = async () => {
    const tok = await AsyncStorage.getItem("token")
    const patient = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
      headers: {
        'Authorization': "Bearer " + tok
      }
    })
    const pat = await patient.json();
    this.setState({ mobile: pat.mobile });
    if (pat.mobileDoctor) {
      this.setState({ doc_mob: pat.mobileDoctor });
    } else {
      this.setState({ doc_mob: "no doctor" });
    }

    const token = await AsyncStorage.getItem("token")
    const patientmed = await fetch('https://swasthgarbh-backend.herokuapp.com/patient_medicine', {
      method: "POST",
      headers: {
        'Authorization': "Bearer " + token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "med_name": this.state.medicine_name,
        "to_date": this.state.to_date,
        "from_date": this.state.from_date,
        "extra_med": this.state.extra_med,
        "checked": this.state.checked,
        "sos_switch": this.state.sos_switch,
        "mobile": this.state.mobile,
        "doc_mob": this.state.doc_mob
      })
    })
    const med = await patientmed.json();

  }

  listofmed = async () => {
    const tok = await AsyncStorage.getItem("token")
    const patient = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
      headers: {
        'Authorization': "Bearer " + tok
      }
    })
    const pat = await patient.json();

    const medicines = await fetch('https://swasthgarbh-backend.herokuapp.com/patientmedicine_list')
    const medi = await medicines.json();

    let m = []
    for (var i = 0; i < medi.length; i++) {
      if (medi[i].mobile == pat.mobile) {

        if (medi[i].sos_switch == false) {

          if ((medi[i].checked) == "0") {
            this.setState({ period: "daily" });

          } else if ((medi[i].checked) == "2") {
            this.setState({ period: "monthly" });

          } else if ((medi[i].checked) == "1") {
            this.setState({ period: "weekly" });

          }
        }
        medi[i] = Object.assign({ period: this.state.period }, medi[i]);
        m.push(medi[i])
        this.setState({ med_data: m });
        console.log(m)
      }
    }
  }

  componentDidMount() {
    this.listofmed();
  }

  ShowHideComponent = () => {
    (sos_switch) => this.setState({ sos_switch })
    if (this.state.show == true) {
      this.setState({ show: false });
    } else {
      this.setState({ show: true });
    }
  };

  handleFromdate = (text) => {
    this.setState({ from_date: text })
  }
  handleTodate = (text) => {
    this.setState({ to_date: text })
  }

  onClose = () => this.setState({ modalVisible: false });
  onOpen = () => this.setState({ modalVisible: true });

  _onPressButton() {
    Toast.show('Long Press to edit/delete');
  }
  updateClose = () => this.setState({ updateVisible: false });
  updateOpen = () => { this.setState({ updateVisible: true }) };


  render() {
    return (
      <Provider>
        <>
          <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
            <Appbar.Content title={<Text style={{ color: 'white', fontSize: 25 }}> Medicine </Text>} />
          </Appbar.Header>
          <FlatList style={{}}
            data={this.state.med_data}
            keyExtractor={(item, index) => item._id}
            renderItem={({ item, index }) =>
              <View>
                {item.sos_switch == false ?
                  <TouchableHighlight onLongPress={() => this.props.navigation.navigate("overlay_medicine", { datas: item })} underlayColor="white"
                    // <TouchableHighlight onLongPress={() => this.getItem(item, index)} underlayColor="white"
                    onPress={this._onPressButton}>
                    <View style={styles.container}>
                      <Text style={{ fontSize: 20, marginLeft: 10, color: "#FB5448" }}>{item.med_name}</Text>
                      <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", marginTop: 10 }}>Period</Text>
                        <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", position: "absolute", marginLeft: 200, marginTop: 10 }}>{item.period}</Text>
                      </View>
                      <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", marginTop: 10 }}>Starting from:</Text>
                        <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", position: "absolute", marginLeft: 200, marginTop: 10 }}>{item.from_date}</Text>
                      </View>
                      <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", marginTop: 10 }}>To be taken till:</Text>
                        <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", position: "absolute", marginLeft: 200, marginTop: 10 }}>{item.to_date}</Text>
                      </View>
                      <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", marginTop: 10 }}>{item.extra_med}</Text>
                    </View>
                  </TouchableHighlight>
                  : <TouchableHighlight onLongPress={() => this.props.navigation.navigate("overlay_medicine", { datas: item })} underlayColor="white"
                    onPress={this._onPressButton}>
                    <View style={styles.container}>
                      <Text style={{ fontSize: 20, marginLeft: 10, color: "#FB5448" }}>{item.med_name}</Text>
                      <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", marginTop: 10 }}>SOS</Text>
                      <Text style={{ fontSize: 15, marginLeft: 10, color: "grey", marginTop: 10 }}>{item.extra_med}</Text>
                    </View>
                  </TouchableHighlight>}

              </View>

            }
          >
          </FlatList>

          <View style={{
            width: 60,
            height: 60,
            borderRadius: 60 / 2,
            backgroundColor: "#F8BBD0",
            marginTop: 570, marginLeft: 340, position: "absolute",
            shadowColor: '#470000',
            shadowOffset: { width: 1, height: 6 },
            shadowOpacity: 1,
            elevation: 8
          }} >


            <TouchableOpacity onPress={() => this.onOpen()}>
              <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
              <Image source={plus_icon} style={{ height: 30, width: 30, alignSelf: "center", marginTop: 15 }}></Image>
            </TouchableOpacity>


            <Overlay visible={this.state.modalVisible} onClose={this.onClose} closeOnTouchOutside>

              <View style={{ height: 50, alignSelf: "flex-start", width: "95%", marginLeft: 8 }}>
                <TextInput placeholder="Medicine Name"
                  onChangeText={(medicine_name) => {
                    this.setState({ medicine_name });
                    // console.log(medicine_name)
                  }}
                  value={this.state.medicine_name}
                  style={{ flex: 1, flexDirection: "column", alignItems: "center", backgroundColor: "white" }}></TextInput>
              </View>


              {this.state.show ? (
                <View style={{ flexDirection: "row" }}>
                  <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                    <Text style={{ color: "grey", fontSize: 18, marginTop: 20, marginRight: 60, }}>From</Text>
                  </View>
                  <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <DatePicker style={{ width: "100%", marginTop: 15, marginLeft: 130 }}
                      date={this.state.from_date}
                      placeholder="Date"
                      format="DD-MM-YYYY"
                      minDate="01-01-2012"
                      maxDate={new Date()}
                      showYearDropdown
                      ScrollableMonthYearDropdown
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 36
                        }
                      }} onDateChange={(date) => { this.setState({ from_date: date }) }} >
                    </DatePicker>
                  </View>
                </View>
              ) : null}

              {this.state.show ? (
                <View style={{ flexDirection: "row", marginHorizontal: 100 }}>
                  <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                    <Text style={{ color: "grey", fontSize: 18, marginTop: 20, marginRight: 80, }}>To</Text>
                  </View>
                  <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <DatePicker style={{ width: "100%", marginTop: 15, marginLeft: 130 }}
                      date={this.state.to_date}
                      placeholder="Date"
                      format="DD-MM-YYYY"
                      minDate="01-01-2012"
                      maxDate={new Date()}
                      showYearDropdown
                      ScrollableMonthYearDropdown
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 36
                        }
                      }} onDateChange={(date) => { this.setState({ to_date: date }) }} >
                    </DatePicker>
                  </View>
                </View>
              ) : null}

              <View style={{ flexDirection: "row", marginTop: 20 }}>
                <Text style={{ marginTop: 5, marginLeft: 10 }}>SOS</Text>
                <Switch
                  value={this.state.sos_switch}
                  onValueChange={(sos_switch) => {
                    this.setState({ sos_switch });
                    this.ShowHideComponent();
                  }}
                  trackColor={{ true: 'pink', false: 'grey' }}
                  thumbColor='pink'
                  style={{ flex: 1 }}
                ></Switch>
              </View>

              {this.state.show ? (
                <View style={{ flexDirection: "row" }}>
                  {
                    this.state.data.map((data, key) => {
                      return (
                        <View>
                          {this.state.checked == key ?
                            <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginHorizontal: 15 }}>
                              <Image style={{ height: 25, width: 25, marginTop: 20, marginRight: 5 }} source={require('./drawables/radio.png')}></Image>
                              <Text style={{ marginRight: 15, marginTop: 20, fontSize: 15 }}>{data}</Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => { this.setState({ checked: key }); console.log(key) }} style={{ flexDirection: "row", alignItems: "center", marginHorizontal: 15 }}>
                              <Image style={{ height: 25, width: 25, marginTop: 20, marginRight: 5 }} source={require('./drawables/radio_button.png')}></Image>
                              <Text style={{ marginRight: 15, marginTop: 20, fontSize: 15 }}>{data}</Text>
                            </TouchableOpacity>
                          }
                        </View>
                      )
                    })
                  }
                </View>) : null}


              <View style={{ height: 50, alignSelf: "flex-start", width: "95%", marginLeft: 8, marginTop: 20 }}>
                <TextInput placeholder="Extra Comments"
                  onChangeText={(extra_med) => {
                    this.setState({ extra_med });
                    // console.log(extra_med)
                  }}
                  value={this.state.extra_med}
                  multiline={true}
                  style={{ flex: 1, flexDirection: "column", alignItems: "center", backgroundColor: "white" }}></TextInput>
              </View>
              <TouchableOpacity
                style={{ alignSelf: "center", backgroundColor: "pink", marginTop: 20 }}
                onPress={() => {
                  this.onClose();
                  this.sendmed();
                }}>
                {/* // onPress= {this.ShowHideComponent}> */}
                <Text style={{ color: "black", padding: 15 }}>Add Medicine</Text>
              </TouchableOpacity>

            </Overlay>
         
          </View>

        </>
      </Provider>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 0,
    padding: 10,
    paddingBottom: 0,
    marginBottom: 5,
    borderRadius: 15,
    marginLeft: 2,
    marginRight: 2,
    marginTop: 5,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2
  }

})
