import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, Image, FlatList } from 'react-native';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import moment from 'moment';
import 'moment-timezone';

var next_icon = require('./drawables/next.png');
var refresh_icon = require('./drawables/reload2.png');
var logout_icon = require('./drawables/logout.png');
var checkup_icon = require('./drawables/checkup.png');

export default class patient_listview extends Component {

    state = {
        data: [],
        data_uhid: [],
        _menu: null
    }

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };

    logout = async () => {
        AsyncStorage.removeItem("token").then(() => {
            this.props.navigation.replace("home_screen")
        })
    }

    fetchdata = async () => {
        const patient = await fetch('https://swasthgarbh-backend.herokuapp.com/patientlist');
        const pat_data = await patient.json();


        const token = await AsyncStorage.getItem("token")
        const doctor = await fetch('https://swasthgarbh-backend.herokuapp.com/doctor', {
            headers: new Headers({
                Authorization: "Bearer " + token
            })
        })
        const doc_data = await doctor.json();

        let pat = []
        for (var i = 0; i < pat_data.length; i++) {

            if (pat_data[i].mobileDoctor) {
                if ((pat_data[i].mobileDoctor) == doc_data.mobile) {
                    let setformatlmp = moment(pat_data[i].lmpdate, 'DD-MM-YYYY') ///set the date format
                    const mylmp = new Date(setformatlmp);
                    mylmp.setDate(mylmp.getDate() + 282);
                    mylmp.toString()  //convert to string
                    const mylmpdate = String(mylmp).split(' '); //split the date
                    const edd = (mylmpdate[2] + '/' + mylmpdate[1] + '/' + mylmpdate[3]);
                    //console.log("my edd", edd)
                    pat_data[i] = Object.assign({ EDD: edd }, pat_data[i]);
                    pat.push(pat_data[i])

                }
            }
        }

        this.setState({ data: pat });
        
    };

    componentDidMount() {
        this.fetchdata();

    }

    _onRefresh() {
        this.setState({ refreshing: true });
        this.fetchdata().then(() => {
            this.setState({ refreshing: false })
        })
    }


    render() {
        return (

            <View>

                <View style={{ height: 60, backgroundColor: "#F8BBD0", }}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ color: "white", padding: 10, fontSize: 20, fontWeight: "bold", marginTop: 10 }}>All Patients</Text>
                        <Menu style={{ marginRight: 200, marginHorizontal: 120, marginTop: 10 }}
                            ref={this.setMenuRef}
                            button={<Text style={{ color: "white", fontWeight: "bold", fontSize: 36 }} onPress={this.showMenu}>                        ...</Text>}
                        >
                            <View style={{ flexDirection: "row" }}>
                                <Image source={refresh_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 12 }}></Image>
                                <MenuItem onPress={() => this._onRefresh()}>Refresh</MenuItem>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                                <Image source={checkup_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 10 }}></Image>
                                <MenuItem onPress={() => this.props.navigation.navigate("anc_assist")}>ANC Assist</MenuItem>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                                <Image source={logout_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 10 }}></Image>
                                <MenuItem onPress={() => this.logout()}>Logout</MenuItem>
                            </View>

                        </Menu>
                    </View></View>


                <FlatList style={{ flex: 0 }}
                    data={this.state.data}
                    keyExtractor={(item, index) => item._id}
                    renderItem={({ item, index }) =>
                        <View style={styles.container}>
                            <View style={{ flexDirection: "row" }}>
                                <Text style={{ fontSize: 20, marginLeft: 10 }}>{item.name}</Text>

                                <View style={{ position: "absolute", marginLeft: 340 }}>
                                    <TouchableOpacity style={{ alignSelf: "center" }}
                                        onPress={() => this.props.navigation.navigate("patient_insidedoc", { P1: item })}>
                                        {/* // onPress={() => this.props.navigation.navigate("patient_insidedoc", {id: item._id})}> */}
                                        <Image source={next_icon} style={{ height: 20, width: 20, marginTop: 6 }}></Image>
                                    </TouchableOpacity>
                                </View>
                                {/* <Text>{item.pro_price}</Text> */}
                            </View>
                            <Text style={{ fontSize: 15, marginLeft: 10, marginBottom: 50, color: "#575a5d" }}>EDD: {item.EDD}</Text>
                        </View>
                    }
                />
            </View>


        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        padding: 10,
        paddingBottom: 0,
        marginBottom: 5,
        borderRadius: 15,
        marginLeft: 2,
        marginRight: 2,
        marginTop: 5,
        backgroundColor: "white",
        shadowColor: '#470000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.2,
        elevation: 2
    },
    threedots: {
        color: "white",
        fontWeight: "bold",
        fontSize: 40
    }

})


