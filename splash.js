import React, {Component} from 'react';
import {View, StyleSheet, ImageBackground, Image} from 'react-native';


var logo=require('./drawables/sg.png');
export default class Splash extends Component
{
        constructor(props)
        {
            super(props);
            setTimeout(()=>
            {
                this.props.navigation.navigate('home_screen');
            },3000);

        }
        render()
        {
        return(
            <ImageBackground
                style={{backgroundColor: '#F8BBD0', height:'100%', width:'100%'}}>
                <View
                style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <Image source={logo}
                    style={{height:300, width:150}}></Image>
                </View>
            </ImageBackground>
            
        );
    }
}
