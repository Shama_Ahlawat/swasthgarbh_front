import React, { useState, setState, Component, useEffect } from 'react';
import { Button, Appbar, TextInput } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import 'moment-timezone';

// import Picker from "@react-native-community/picker";

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  KeyboardAvoidingView,
  Picker,
  TouchableOpacity,
  Switch
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


const patient_regdoc = (props) => {
  const [mobile, setMobile] = useState('')
  const [password, setPassword] = useState('')
  const [name, setName] = useState('')
  const [address, setAddress] = useState('')
  const [age, setAge] = useState('')
  const [date, setDate] = useState('')
  const [email, setEmail] = useState('')
  const [high_bp_check, set_high_bp_check] = useState(false)
  const [history_pre, set_history_pre] = useState(false)
  const [mother_sis, set_mother_sis] = useState(false)
  const [obesity, set_obesity] = useState(false)
  const [baby, set_baby] = useState(false)
  const [any_history, set_any_history] = useState(false)
  const [education, setEducation] = useState('Select')
  const [lmpdate, setLmpdate] = useState(null)
  const [sestatus, setSestatus] = useState(null)
  const [isEnabled, setIsEnabled] = useState(false);
  const [uhid, setuhid] = useState('');
  const [open, setOpen] = useState(false);
  



  const ContentTitle = ({ title, style }) => (
    <Appbar.Content
      title={<Text style={style}> {title} </Text>}
    />
  );

  const sendCred = async () => {
    const token_doc = await AsyncStorage.getItem("token")
    const doctor = await fetch('https://swasthgarbh-backend.herokuapp.com/doctor', {
      headers: new Headers({
        Authorization: "Bearer " + token_doc
      })
    })
    const doc_data = await doctor.json();
    const savedocmob = (doc_data.mobile)
    const savedocname = (doc_data.name)
    console.log(savedocmob)

    const dash = "_"
    const pass = (name + dash + uhid)


    let setformat = moment(lmpdate, 'DD-MM-YYYY') ///set the date format
    const edd = new Date(setformat);
    edd.setDate(edd.getDate() + 282); //apply calculation
    edd.toString()  //convert to string

    const edddate = String(edd).split(' '); //split the date
    
    
    if (edddate[1] == 'Jan') {
      edddate[1] = '01'
    } if (edddate[1] == 'Feb') {
      edddate[1] = '02'
    } if (edddate[1] == 'March') {
      edddate[1] = '03'
    } if (edddate[1] == 'April') {
      edddate[1] = '04'
    } if (edddate[1] == 'May') {
      edddate[1] = '05'
    } if (edddate[1] == 'June') {
      edddate[1] = '06'
    } if (edddate[1] == 'July') {
      edddate[1] = '07'
    } if (edddate[1] == 'Aug') {
      edddate[1] = '08'
    } if (edddate[1] == 'Sep' ) {
      edddate[1] = '09'
    } if (edddate[1] == 'Oct') {
      edddate[1] = 10
    } if (edddate[1] == 'Nov') {
      edddate[1] = 11
    } if (edddate[1] == 'Dec') {
      edddate[1] = 12
    }

    const eddnew = (edddate[2] + '-' + edddate[1] + '-' + edddate[3]);

    // const tok = await AsyncStorage.getItem("token")
    const signup = await fetch("https://swasthgarbh-backend.herokuapp.com/patient_signupbydoc", {
      method: "POST",
      headers: new Headers({
        // 'Authorization': "Bearer " + tok,
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({
        "mobile": mobile,
        "uhid": uhid,
        "docmob": savedocmob,
        "docname": savedocname,
        "name": name,
        "password": pass,
        "address": address,
        "email": email,
        "age": age,
        "high_bp_check": high_bp_check,
        "history_pre": history_pre,
        "mother_sis": mother_sis,
        "obesity": obesity,
        "baby": baby,
        "any_history": any_history,
        "sestatus": sestatus,
        "education": education,
        "lmpdate": lmpdate
      })
    })
    const signup_data = await signup.json();
    console.log("token sent is ",signup_data)
    console.log (lmpdate)
    // await AsyncStorage.setItem('token', signup_data.token)
    // props.navigation.replace("patient_screenbydoc", {P1: lmpdate})

    props.navigation.replace("patient_screenbydoc", {P1: lmpdate, P2: eddnew, P3: mobile, P4:savedocmob})
  }

  useEffect(() => {
    sendCred()
  }, [])



  return (
    <ScrollView>
      <KeyboardAvoidingView behavior='padding'>
        <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />

        <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
          <ContentTitle title="Patient Registration" style={{ color: 'white', fontSize: 20 }} />
        </Appbar.Header>

        <TextInput label="Name"
          onChangeText={(name) => {
            setName(name);
            console.log(name)
          }}
          style={{ marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#0FDFB0" }}
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        <TextInput label="Address (Optional)"
          onChangeText={(address) => setAddress(address)}
          style={{ flex: 0, marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#0FDFB0", }}
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        <TextInput label="UHID/Reg.no."
          onChangeText={(uhid) => { setuhid(uhid); console.log(uhid) }}
          style={{ flex: 0, marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#0FDFB0", }}
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        <TextInput label="Age"
          onChangeText={(age) => setAge(age)}
          style={{ marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#0FDFB0" }}
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        <View style={{ flexDirection: "row" }}>
          <Text style={{ marginLeft: 5, color: "gray", marginTop: 40 }}>+91 - </Text>
          <TextInput label="Mobile number"
            onChangeText={(mobile) => setMobile(mobile)}
            style={{ flex: 1, marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#FAE3E9" }}
            keyboadType='numeric'
            theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
        </View>

        <View style={{ flexDirection: "row" }}>
          <Text style={{ color: "grey", marginTop: 20, marginLeft: 18 }}>Last Menstrual Period</Text>

          <DatePicker
            style={{ width: 200, marginTop: 10 }}
            date={lmpdate}
            onDateChange={date => setLmpdate(date)}
            //The enum of date, datetime and time
            placeholder="select date"
            format="DD-MM-YYYY"
            minDate="01-01-2012"
            maxDate={new Date()}
            showYearDropdown
            ScrollableMonthYearDropdown
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateInput: {
                marginLeft: 36
              }
            }}>
          </DatePicker>
        </View>

        <TextInput label="Email (Optional)"
          value={email}
          onChangeText={(text) => setEmail(text)}
          style={{ flex: 0, marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#0FDFB0", }}
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        <View style={styles.container}>
          <Text style={{ color: "grey", marginTop: 5, marginLeft: 10 }}>Some extra details (Optional)</Text>
          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={high_bp_check}
                onValueChange={set_high_bp_check}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>History of high blood pressure prior to pregnancy</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={history_pre}
                onValueChange={set_history_pre}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>A history of preeclampsia</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={mother_sis}
                onValueChange={set_mother_sis}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>Having a mother or sister who had preeclampsia</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={obesity}
                onValueChange={set_obesity}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>A history of obesity</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={baby}
                onValueChange={set_baby}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>Carrying more than one baby</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={any_history}
                onValueChange={set_any_history}
                style={styles.checkbox}
              />
              <Text style={{ marginRight: 10, marginLeft: 5, marginTop: 5, marginBottom: 5 }}>History of diabetes, kidney disease, lupus, or rheumatoid arthritis</Text>
            </View>
          </View>

          <Text style={{ marginTop: 5, marginBottom: 0, marginLeft: 5, marginRight: 5 }}>Education </Text>
          <Picker style={{ marginTop: 0 }}
            selectedValue={education}
            onValueChange={(itemValue, itemIndex) => setEducation(itemValue)}>
            <Picker.Item label="Select" value="Select" color="#f06292"></Picker.Item>
            <Picker.Item label="Primary" value="Primary"></Picker.Item>
            <Picker.Item label="Secondary" value="Secondary"></Picker.Item>
            <Picker.Item label="Graduation" value="Graduation"></Picker.Item>
            <Picker.Item label="Post-Graduation" value="Post-Graduation"></Picker.Item>
          </Picker>

          <Text style={{ marginTop: 5, marginBottom: 0, marginLeft: 5, marginRight: 5 }}>SE Status </Text>
          <Picker style={{ marginTop: 0 }}
            selectedValue={sestatus}
            onValueChange={(itemValue, itemIndex) => setSestatus(itemValue)}>
            <Picker.Item label="Select" value="Select" color="#f06292"></Picker.Item>
            <Picker.Item label="Lower" value="Lower"></Picker.Item>
            <Picker.Item label="Upper Lower" value="Upper Lower"></Picker.Item>
            <Picker.Item label="Lower Middle" value="Lower Middle"></Picker.Item>
            <Picker.Item label="Upper Middle" value="Upper Middle"></Picker.Item>
            <Picker.Item label="Upper" value="Upper"></Picker.Item>
          </Picker>

        </View>


        <TouchableOpacity
          style={styles.my_Button}
          onPress={() => sendCred(props)}>
          <Text style={{ color: "black", fontweight: "bold", paddingTop: 10, paddingBottom: 10, paddingLeft: 30, paddingRight: 30 }}>Register</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </ScrollView>

  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    paddingBottom: 10,
    marginBottom: 5,
    borderRadius: 15,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    backgroundColor: "white",
  },
  my_Button: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    marginLeft: 5,
    marginBottom: 10,
    marginRight: 15,
    marginTop: 10,
    alignSelf: 'center',
    backgroundColor: '#FAE3E9',
  },
  container_calender: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    padding: 16
  },

  container_checkbox: {
    marginTop: 5,
    paddingLeft: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  checkbox: {
    alignSelf: "flex-start",
  },
  checkboxContainer: {
    flexDirection: "row",
    alignSelf: "flex-start",
    marginBottom: 1,
  }
})

export default patient_regdoc;