import React, { Component, useState } from 'react';
import { Button, Appbar, TextInput, Dialog, Portal, Provider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import Modal from 'react-native-modal';
import Toast from 'react-native-simple-toast';
import DatePicker from 'react-native-datepicker';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import {
  SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Image,
  KeyboardAvoidingView, TouchableHighlight,
  TouchableOpacity, Switch
} from 'react-native';
import Overlay from 'react-native-modal-overlay';

var plus_icon = require('./drawables/hospital.png');
var dustbin_icon = require('./drawables/dustbin.png')
export default class overlay_medicine extends Component {
  state = {
    med_data: [],

  }

  constructor(props) {
    super(props)
    this.state = {
      data: ['Daily', 'Weekly', 'Monthly'],
      checked: this.props.route.params.datas.checked,
      modalVisible: false,
      medicine_name: this.props.route.params.datas.med_name,
      from_date: this.props.route.params.datas.from_date,
      to_date: this.props.route.params.datas.to_date,
      extra_med: this.props.route.params.datas.extra_med,
      sos_switch: false,
      show: true,
      mobile: '',
      doc_mob: '',
      period: '',
      updateVisible: true,
      isDialogVisible: false
    }
  }



  ShowHideComponent = () => {
      
        (sos_switch) => this.setState({ sos_switch})
        if (this.state.show == true) {
          this.setState({ show: false });
        } else {
          this.setState({ show: true });
        }
      
  };

  handleFromdate = (text) => {
    this.setState({ from_date: text })
  }
  handleTodate = (text) => {
    this.setState({ to_date: text })
  }

  _onPressButton() {
    Toast.show('Long Press to edit/delete');
  }
  updateClose = () => this.props.navigation.navigate("patient_medicine");
  updateOpen = () => { this.setState({ updateVisible: true }) };

  render() {
    return (
      <Provider>
        <>
            <Overlay visible={this.state.updateVisible} onClose={this.updateClose} closeOnTouchOutside>
              <View style={{ height: 50, alignSelf: "flex-start", width: "95%", marginLeft: 8 }}>
                <Image source={dustbin_icon} style={{ height: 20, width: 20, alignSelf: "flex-end", marginTop: 10 }}></Image>
                <TextInput placeholder="Medicine Name"
                  onChangeText={(medicine_name) => {
                    this.setState({medicine_name});
                  }}
                  // value={this.navigation.state.params.P1.med_name}
                  value={this.state.medicine_name}
                  style={{ flex: 0, flexDirection: "column", alignItems: "center", backgroundColor: "white", position: "relative" }}></TextInput>
              </View>


              {this.state.show ? (
                <View style={{ flexDirection: "row" }}>
                  <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                    <Text style={{ color: "grey", fontSize: 18, marginTop: 60, marginRight: 60, }}>From</Text>
                  </View>
                  <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <DatePicker style={{ width: "100%", marginTop: 55, marginLeft: 130 }}
                      date={this.state.from_date}
                      placeholder="Date"
                      format="DD-MM-YYYY"
                      minDate="01-01-2012"
                      maxDate={new Date()}
                      showYearDropdown
                      ScrollableMonthYearDropdown
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 36
                        }
                      }} onDateChange={(date) => { this.setState({ from_date: date }) }} >
                    </DatePicker>
                  </View>
                </View>
              ) : null}

              {this.state.show ? (
                <View style={{ flexDirection: "row", marginHorizontal: 100 }}>
                  <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                    <Text style={{ color: "grey", fontSize: 18, marginTop: 20, marginRight: 80, }}>To</Text>
                  </View>
                  <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <DatePicker style={{ width: "100%", marginTop: 15, marginLeft: 130 }}
                      date={this.state.to_date}
                      placeholder="Date"
                      format="DD-MM-YYYY"
                      minDate="01-01-2012"
                      maxDate={new Date()}
                      showYearDropdown
                      ScrollableMonthYearDropdown
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 36
                        }
                      }} onDateChange={(date) => { this.setState({ to_date: date }) }} >
                    </DatePicker>
                  </View>
                </View>
              ) : null}

              <View style={{ flexDirection: "row", marginTop: 20 }}>
                <Text style={{ marginTop: 5, marginLeft: 10 }}>SOS</Text>
                <Switch
                  value={this.state.sos_switch}
                  onValueChange={(sos_switch) => {
                    this.setState({ sos_switch });
                    this.ShowHideComponent();
                  }}
                  trackColor={{ true: 'pink', false: 'grey' }}
                  thumbColor='pink'
                  style={{ flex: 1 }}
                ></Switch>
              </View>

              {this.state.show ? (
                <View style={{ flexDirection: "row" }}>
                  {
                    this.state.data.map((data, key) => {
                      return (
                        <View>
                          {this.state.checked == key ?
                            <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginHorizontal: 15 }}>
                              <Image style={{ height: 25, width: 25, marginTop: 20, marginRight: 5 }} source={require('./drawables/radio.png')}></Image>
                              <Text style={{ marginRight: 15, marginTop: 20, fontSize: 15 }}>{data}</Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => { this.setState({ checked: key }); console.log(key) }} style={{ flexDirection: "row", alignItems: "center", marginHorizontal: 15 }}>
                              <Image style={{ height: 25, width: 25, marginTop: 20, marginRight: 5 }} source={require('./drawables/radio_button.png')}></Image>
                              <Text style={{ marginRight: 15, marginTop: 20, fontSize: 15 }}>{data}</Text>
                            </TouchableOpacity>
                          }
                        </View>
                      )
                    })
                  }
                </View>) : null}


              <View style={{ height: 50, alignSelf: "flex-start", width: "95%", marginLeft: 8, marginTop: 20 }}>
                <TextInput placeholder="Extra Comments"
                  onChangeText={(extra_med) => {
                    this.setState({ extra_med });
                  }}
                  value={this.state.extra_med}
                  multiline={true}
                  style={{ flex: 1, flexDirection: "column", alignItems: "center", backgroundColor: "white" }}></TextInput>
              </View>
              <TouchableOpacity
                style={{ alignSelf: "center", backgroundColor: "pink", marginTop: 20 }}
                onPress={() => {
                  this.updateClose();
                }}>
                {/* // onPress= {this.ShowHideComponent}> */}
                <Text style={{ color: "black", padding: 15 }}>Update</Text>
              </TouchableOpacity>

            </Overlay>   

        </>
      </Provider>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 0,
    padding: 10,
    paddingBottom: 0,
    marginBottom: 5,
    borderRadius: 15,
    marginLeft: 2,
    marginRight: 2,
    marginTop: 5,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2
  }

})
