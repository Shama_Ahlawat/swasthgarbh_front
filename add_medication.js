import React, { useState, useEffect } from 'react';
import { Button, Appbar } from 'react-native-paper';
import { RadioButton } from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import * as firebase from '@react-native-firebase/app';
import { Picker, ActivityIndicator} from 'react-native';
// import Spinner from 'react-native-loading-spinner-overlay';
// import DeviceInfo from 'react-native-device-info';

// import firebase  from './firebase';
import { createAlarm,getAlarms ,getAlarmById,editAlarm} from 'react-native-simple-alarm';
import moment from 'moment'
// import CheckboxGroup from 'react-native-checkbox-group'
// import TimePicker from 'react-time-picker';
// import 'react-time-picker/dist/TimePicker.css';
// import 'react-clock/dist/Clock.css';


// import DateTimePickerModal from "react-native-modal-datetime-picker";
// import TimePicker from 'react-time-picker';
import DateTimePicker from '@react-native-community/datetimepicker';


import CheckBox from '@react-native-community/checkbox';
// import moment from 'moment';
import 'moment-timezone';
// import PushNotificationIOS from "@react-native-community/push-notification-ios";
import CalendarPicker from 'react-native-calendar-picker';

// var PushNotification = require("react-native-push-notification");

// createAlarm = async () => {
//     try {
//       await createAlarm({
//           active: false,
//           date: moment().format(),
//           message: 'message',
//           snooze: 1,
//         });
//     } catch (e) {}
//   }

//   getAlarms = async () => {
//     try {
//       const alarms = await getAlarms();
//     } catch (e) {}
//   }

//   getAlarms = async () => {
//     let id = '07699912-87d9-11ea-bc55-0242ac130003';
    
//     try {
//       const alarm = await getAlarmById(id);
//     } catch (e) {}
//   }

//   editAlarm = async () => {
//     let id = '07699912-87d9-11ea-bc55-0242ac130003';
    
//     try {
//       await editAlarm({
//           id,
//           date: moment().add(1, 'days').format(),
//           snooze: 1,
//           message: 'Message',
//           active: true
//         });
//     } catch (e) {}
//   }

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const add_medication = (props) => {
  const [sys_BP, set_Sys_BP] = useState('')
  const [page,setPage] = useState(0)
  const [medicationsDetails , setMedicationDetails] = useState({medicineName:'',medicineQuantity:0,medicineReason:''})
  const [unit,setUnit] = useState('g')
  const [selectedStartDate,setselectedStartDate] = useState('')
  const [selectedEndDate,setselectedEndDate] = useState('')
  const [checked, setChecked] = React.useState('');
  const [nooftimesdose,setnooftimesdose] = useState(0)
  const [value, onChange] = useState('10:00');
  const [date, setDate] = useState(new Date(1598051730000));
  const [timer,setTimer] = useState({'timer1':new Date(1598051730000),'timer2':new Date(1598051730000),'timer3':new Date(1598051730000),'timer4':new Date(1598051730000)})
  const [selectedDays,setSelectedDays] = useState([])
  const [patient_data , setPatientData] = useState({})
  const [serverTime,setServerTimer] = useState([])
  const [selectedValue, setSelectedValue] = useState("Location (required)");
  const [showButton,showSaveButton] = useState(false)
  const [loader,setLoader] = useState(false)

  useEffect(() => {
    getpatientdetails()
  }, [])

  const getpatientdetails = async() => {
    const token = await AsyncStorage.getItem("token")
    //console.log(token)
    const patient = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
      headers: new Headers({
        Authorization: "Bearer " + token
      })
    })
    const patient_data = await patient.json();
    console.log('myyyyyyyyyyy',patient_data.mobile)
    setPatientData(patient_data)
  }

  const onDateChange = (date, type) => {
      console.log('on change')
    if (type === 'END_DATE') {
        setselectedEndDate(date)
    } else {
        setselectedStartDate(date)
        setselectedEndDate(null)
    }
  }

  const ContentTitle = ({ title, style }) => (
    <Appbar.Content
      title={<Text style={style}> {title} </Text>}
    />
  );

  const setValue = (name,value) => {
    const copymedicationsDetails = medicationsDetails
    if(name === 'medicineName'){
        setMedicationDetails({...copymedicationsDetails,medicineName:`${value}`})
    }
    if(name === 'medicineQuantity'){
        setMedicationDetails({...copymedicationsDetails,medicineQuantity:`${value} ${unit}`})
    }
    if(name === 'medicineReason'){
        setMedicationDetails({...copymedicationsDetails,medicineReason:value})
    }
  }

  const saveData = async() => {
    const patient_mobile = await AsyncStorage.getItem("patient_mobile")
    const doctor_mobile = AsyncStorage.getItem("doctor_mobile")
    console.log('patient_mobile',patient_mobile)
      if(page < 4){
        setPage(page + 1)
      }
      else{
        setLoader(true)
        console.log('medicationsDetails',medicationsDetails)
        console.log('page',page)
        console.log('unit',unit)
        console.log('checked',checked)
        console.log('nooftimesdose',nooftimesdose)
        console.log('selectedDays $$$$$$$$$$$$',selectedDays)
        console.log('copytimercopytimercopytimer',timer.timer1)
        console.log('copytimercopytimercopytimer',timer.timer2)
        console.log('server time',serverTime)
    
        const payload = {
            med_name : medicationsDetails.medicineName,
            med_quantity:medicationsDetails.medicineQuantity,
            med_reason:medicationsDetails.medicineReason,
            from_date: selectedStartDate ? selectedStartDate.toString() : '',
            to_date : selectedEndDate ? selectedEndDate.toString() : '',
            schedule:checked, //either daily or weekly
            dose_count:nooftimesdose, //twice or thrice
            selected_days:selectedDays,//monday , tuesday
            alarm_timer:serverTime,
            mobile:patient_data.mobile,
            doc_mob:patient_data.doctor_mobile ? patient_data.doctor_mobile : null
        }
  
        const token = await AsyncStorage.getItem("token")
        fetch("https://swasthgarbh-backend.herokuapp.com/patient_medicine/", {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
             Authorization: " secretkey " + token
          },
          body: JSON.stringify({
              "med_name" : medicationsDetails.medicineName,
              "med_quantity":medicationsDetails.medicineQuantity,
              "med_reason":medicationsDetails.medicineReason,
              "from_date": selectedStartDate ? selectedStartDate.toString() : '',
              "to_date" : selectedEndDate ? selectedEndDate.toString() : '',
              "schedule":checked, //either daily or weekly
              "dose_count":nooftimesdose, //twice or thrice
              "selected_days":selectedDays,//monday , tuesday
              "alarm_timer":serverTime,
              "mobile":patient_data.mobile,
              "doc_mob":patient_data.doctor_mobile ? patient_data.doctor_mobile : null
          })
        })
          .then(res => res.json())
          .then(async (data) => {
            setLoader(false)
            console.log('myyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',data)
            // await AsyncStorage.setItem('token', data.token)
            await props.navigation.replace('patient_screen')
          })
      }
  }

  const selectedStrength = (strength) => {
     setUnit(strength)
  }

  const onChangeDosedose = (value) => {
    console.log('on change dropdowwwn')
    setnooftimesdose(value)
  }

  const onTimeChange = (event, selectedDate) => {
    showSaveButton(true)
    console.log('event $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$',event._dispatchInstances.pendingProps.testID)
    const currentDate = selectedDate || date;
    const timerId = event._dispatchInstances.pendingProps.testID
   console.log('timerIdtimerIdtimerIdtimerId',timerId)
    const copytimer = timer
    const dataForServer = []
    console.log('copytimer befireee $$$$$$$$$$$$',copytimer)
    Object.keys(copytimer).forEach((key) => {
      if(key === timerId){
        return copytimer[key] = currentDate
      }
      dataForServer.push(moment(copytimer[key].toString()).format('h:mm a'))
    })
    console.log('copytimer afteerrrrr$$$$$$$$$$$$',copytimer)
    console.log('dataForServer dataForServer dataForServer %%%%%%%%%%%%%%%%%%% ',dataForServer)
        // setShow(Platform.OS === 'ios');
    setDate(currentDate);
    setTimer(copytimer)
    setServerTimer(dataForServer)
  };

  const setDays = (value) => {
     const copyselectedDays = [...selectedDays]
     if(!copyselectedDays.includes(value)){         
        copyselectedDays.push(value);      
    }else{
        copyselectedDays.splice(copyselectedDays.indexOf(value), 1);
    }
     setSelectedDays(copyselectedDays)
    // setSelectedDays(currentArray => [...currentArray, value])
  }

    const units = ['g','IU','MCG','MEQ','MG']

    const minDate = new Date(); // Today
    const maxDate = new Date(2022, 6, 3);
    const startDate  =  selectedStartDate ? selectedStartDate.toString() : '';
    const endDate = selectedEndDate ? selectedEndDate.toString() : '';
    console.log('startDate',moment(startDate).format("MM/DD/YY"))
    console.log('startDate',moment(endDate).format("MM/DD/YY"))
    console.log('medicationsDetails',medicationsDetails)
    console.log('page',page)
    console.log('unit',unit)
    console.log('checked',checked)
    console.log('nooftimesdose',nooftimesdose)
    console.log('selectedDays $$$$$$$$$$$$',selectedDays)
    console.log('selectedDays $$$$$$$$$$$$',selectedDays.length)
    console.log('copytimercopytimercopytimer',moment(timer).format('h:mm a'))
    const patient_mobile = AsyncStorage.getItem("patient_mobile")
    const doctor_mobile = AsyncStorage.getItem("doctor_mobile")
    console.log('patient_mobile',patient_mobile)

  return (
    <>
      <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
      <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
        <ContentTitle title="Add Medication" style={{ color: 'white', fontSize: 20 }} />
      </Appbar.Header>
      <ScrollView>
          <>
          {/* <Spinner
          visible={true}
          textContent={'Loading...'}
          // textStyle={styles.spinnerTextStyle}
        /> */}
          {
              page === 0 && <View>
            <View style={{backgroundColor:'#F8BBD0',height:150,flexDirection:'row',alignItems:'center'}}>
               <Text  style={{color:'white',fontSize:30,paddingLeft:20}}>What med would you like to add?</Text>
            </View>
          <View style={{ flex: 1, marginTop: 10 }}>
              <TextInput placeholder="Enter Medicine Name"
                style={{ borderBottomWidth: 1,borderBottomColor: '#000000',margin:20,padding:20}}
                onChangeText={(value) => {
                  setValue('medicineName',value);
                }}
                // onKeyPress={(keyPress) => console.log('keypress',keyPress)}
                // value={value}
                >  
                </TextInput>
            </View>
        </View>
          }
           {
              page === 1 && <View>
            <View style={{backgroundColor:'#F8BBD0',height:150,flexDirection:'row',alignItems:'center'}}>
               <Text  style={{color:'white',fontSize:30,paddingLeft:20}}>What strength is the med?</Text>
          </View>
          <View style={{ flex: 1, marginTop: 10 }}>
              <TextInput placeholder="Enter Quantity"
                style={{ borderBottomWidth: 1,borderBottomColor: '#000000',margin:20,padding:20}}
                onChangeText={(value) => {
                  setValue('medicineQuantity',value);
                }}
                // onKeyPress={(keyPress) => console.log('keypress',keyPress)}
                // value={value}
                >
                    
                </TextInput>
                <View style={{display:'flex',flexDirection:'row',justifyContent:'space-around',margin:20,padding:20}}>
                   {units.map((ele,index) => {
                       return <View style={{backgroundColor:`${ele === unit ? '#F8BBD0' : 'white'}`,padding:10,borderRadius:50}}><Text key={index} onPress={() => selectedStrength(ele)} >{ele}</Text></View>
                   })}
                </View>
            </View>
              </View>
          }
            {
              page === 2 && <View>
            <View style={{backgroundColor:'#F8BBD0',height:150,flexDirection:'row',alignItems:'center'}}>
               <Text style={{color:'white',fontSize:30,paddingLeft:20}}>What strength is the med?</Text>
          </View>
          <View style={{ flex: 1, marginTop: 10 }}>
              <TextInput placeholder="What are you taking it for ?"
                style={{ borderBottomWidth: 1,borderBottomColor: '#000000',margin:20,padding:20}}
                onChangeText={(value) => {
                  setValue('medicineReason',value);
                }}
                >
                    
                </TextInput>
            </View>
              </View>
          }
          {
              page === 3 && <View>
            <View style={{backgroundColor:'#F8BBD0',height:150,flexDirection:'row',alignItems:'center'}}>
               <Text  style={{color:'white',fontSize:30,paddingLeft:20}}>Treatment duration?</Text>
          </View>
          <View>
            <CalendarPicker
                startFromMonday={true}
                allowRangeSelection={true}
                minDate={minDate}
                maxDate={maxDate}
                todayBackgroundColor="#f2e6ff"
                selectedDayColor="#F8BBD0"
                selectedDayTextColor="#FFFFFF"
                onDateChange={onDateChange}
            />
             </View>
              </View>
          }
        {
            page === 4 && <View>
              <View style={{backgroundColor:'#F8BBD0',height:150,flexDirection:'row',alignItems:'center'}}>
                  <Text  style={{color:'white',fontSize:30,paddingLeft:20}}>Schedule for taking medicine</Text>
              </View>
             {checked === '' && <View style={{ flex: 1, marginTop: 10 }}>
              
              <View style={{display:'flex',flexDirection:'row',justifyContent:'space-around',margin:20,padding:20}}>
              <View style={{flexDirection: "row",alignItems:'center'}}>
              <View style={{borderRadius: 50,
              borderWidth: 1,
              borderColor: '#000'}}>
            <RadioButton
                value="daily"
                status={ checked === 'daily' ? 'checked' : 'unchecked' }
                onPress={() => setChecked('daily')}
                color={'#F8BBD0'}
            />
            </View>
              <Text style={{fontSize:16,marginLeft:10}}>Daily</Text>
            </View>
            <View style={{flexDirection: "row",alignItems:'center'}}>
                <View style={{borderRadius: 50,
                borderWidth: 1,
                borderColor: '#000',}}>
                <RadioButton
                    value="weekly"
                    status={ checked === 'weekly' ? 'checked' : 'unchecked' }
                    onPress={() => setChecked('weekly')}
                    color={'#F8BBD0'}
                />
                </View>
                <Text style={{marginLeft:10}}>Weekly</Text>
                </View>
            </View>
            </View>
            }
            {(checked === 'daily' || checked === 'weekly') &&
            <>
               { nooftimesdose === 0 && <View style={{marginLeft:'10%',marginTop:20}}>
                {/* <DropDownPicker
                items={[
                    {label: 'once', value: 1},
                    {label: 'twice', value: 2},
                    {label: 'thrice', value: 3},
                    {label: 'four times', value: 4},
                ]}
            //   defaultValue={this.state.country}
                // containerStyle={{height: 40}}
                style={{backgroundColor: '#fafafa',width:'80%'}}
                itemStyle={{
                    justifyContent: 'flex-start'
                }}
                dropDownStyle={{backgroundColor: '#fafafa',width:'80%'}}
                onChangeItem={item => onChangeDosedose(item.value)}
                zIndex={5000}
                /> */}
                  <Picker
                        // style={styles.defaultPicker}
                        selectedValue={selectedValue}
                        onValueChange={(itemValue, itemIndex) => onChangeDosedose(itemValue)}
                      >
                        <Picker.Item label="select" value={0} />
                        <Picker.Item label="once" value={1} />
                        <Picker.Item label="twice" value={2} />
                        <Picker.Item label="thrice" value={3}/>
                        <Picker.Item label="four times" value={4} />
                      </Picker>
             </View>}
            <View>
            {
                (nooftimesdose === 1 && selectedDays.length === 0) &&
                <DateTimePicker
                testID="timer1"
                value={timer.timer1}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
                // style={{width:'80%',height:100}}
            />
            }
            {
                (nooftimesdose === 2 && selectedDays.length === 0) &&
                <>
                <DateTimePicker
                testID="timer1"
                value={timer.timer1}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
            <DateTimePicker
                testID="timer2"
                value={timer.timer2}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
            </>
            }
                {
                (nooftimesdose === 3 && selectedDays.length === 0) &&
                <>
                <DateTimePicker
                testID="timer1"
                value={timer.timer1}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
            <DateTimePicker
                testID="timer2"
                value={timer.timer2}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
                <DateTimePicker
                testID="timer3"
                value={timer.timer3}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
            </>
            }
            {
                (nooftimesdose === 4 && selectedDays.length === 0) &&
                <>
                <DateTimePicker
                testID="timer1"
                value={timer.timer1}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
            <DateTimePicker
                testID="timer2"
                value={timer.timer2}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
                <DateTimePicker
                testID="timer3"
                value={timer.timer3}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
            <DateTimePicker
                testID="timer4"
                value={timer.timer4}
                mode='time'
                is24Hour={true}
                display="default"
                onChange={onTimeChange}
            />
            </>
            }
            </View>
            </>

            }
            { (checked === 'weekly' && nooftimesdose !== 0) &&
                <>
                <View style={{flexDirection: "row",flexWrap:'wrap',marginTop:40,marginBottom:10,marginLeft:40,marginRight:20}}>
                    {/* <View style={{ flexDirection: "row" }}> */}
                    <CheckBox
                        value={false}
                        onValueChange={() => setDays('monday')}
                        color={'#F8BBD0'}
                    //   onChange={(ele) => setDays(ele)}
                    //   style={styles.checkbox}
                    />
                    <Text style={{ margin: 5 }}>monday</Text>
                    <CheckBox
                        value={false}
                        onValueChange={() => setDays('tuesday')}
                        color={'#F8BBD0'}
                    //   onChange={(ele) => setDays(ele)}
                    //   style={styles.checkbox}
                    />
                    <Text style={{ margin: 5 }}>tuesday</Text>
                    <CheckBox
                        value={false}
                        onValueChange={() => setDays('wednesday')}
                        color={'#F8BBD0'}
                    //   onChange={(ele) => setDays(ele)}
                    //   style={styles.checkbox}
                    />
                    <Text style={{ margin: 5 }}>wednesday</Text>
                    <CheckBox
                        value={false}
                        onValueChange={() => setDays('thursday')}
                        color={'#F8BBD0'}
                    //   onChange={(ele) => setDays(ele)}
                    //   style={styles.checkbox}
                    />
                    <Text style={{ margin: 5 }}>thursday</Text>
                    <CheckBox
                        value={false}
                        onValueChange={() => setDays('friday')}
                        color={'#F8BBD0'}
                    //   onChange={(ele) => setDays(ele)}
                    //   style={styles.checkbox}
                    />
                    <Text style={{ margin: 5 }}>friday</Text>
                    <CheckBox
                        value={false}
                        onValueChange={() => setDays('saturday')}
                        color={'#F8BBD0'}
                    //   onChange={(ele) => setDays(ele)}
                    //   style={styles.checkbox}
                    />
                    <Text style={{ margin: 5 }}>saturday</Text>
                    <CheckBox
                        value={false}
                        onValueChange={() => setDays('sunday')}
                        color={'#F8BBD0'}
                    //   onChange={(ele) => setDays(ele)}
                    //   style={styles.checkbox}
                    />
                    <Text style={{ margin: 5 }}>sunday</Text>
                    {/* </View> */}

                </View>
            </>
            }
            </View>
        }
        {(showButton || selectedDays.length > 0 ) &&
        <Button mode='contained' style={{ backgroundColor:'#F8BBD0',marginLeft: 18, marginRight: 18, marginTop: 18 }}
            onPress={() => {
            saveData()
            }} 
            > SAVE </Button>
        }
        {
          loader &&
           <ActivityIndicator size="large" color="#F8BBD0" />
        }
        {page !== 4  &&
        <Button mode='contained' style={{ backgroundColor:'#F8BBD0',marginLeft: 18, marginRight: 18, marginTop: 18 }}
            onPress={() => {
            saveData()
            }} 
            > NEXT </Button>
        }
        </>
      </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    marginBottom: 5,
    borderRadius: 15,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2
  },
})
export default add_medication