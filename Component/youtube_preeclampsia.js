import React from 'react';
import YouTube from 'react-native-youtube';
import {
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  StyleSheet
} from 'react-native';


const youtube_preeclampsia= (props) => {

    return (
      <>
        <StatusBar backgroundColor="#F8BBD0" barStyle = "light-content" />
        <View style={{height:600, width:800 }}>
        <YouTube
          apiKey= 'AIzaSyC3dEkrrzVAgFerUH7vx0aQTciWf8aINCo'
          videoId="P9GxHQzwIzk" // The YouTube video ID
          autoPlay={false}
        //   fullscreen= {false}// control whether the video should play in fullscreen or inline
          showFullscreenButton={true}// control whether the video should loop when ended
          showPlayPauseButton={true}
          showSeekBar ={true}
        //   onReady={e => this.setState({ isReady: true })}
          onChangeState={e => this.setState({ status: e.state })}
          onChangeQuality={e => this.setState({ quality: e.quality })}
          onError={e => console.log(e)}
          style={{ height: 250, width: 360, flex: 0 }}
          onChangeState={e => console.log(e)}
          onChangeFullscreen={e => console.log(e)}
      />
      </View>
      </>
    )}
    export default youtube_preeclampsia;
