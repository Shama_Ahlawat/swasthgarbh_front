import React,{Component} from 'react';
import {View, Text, Button, StyleSheet, TouchableOpacity, Image, ScrollView} from 'react-native';
import {widthToDp, heightToDp} from './Responsive'

export default class Signin extends Component{
    render(){
        return(
            <ScrollView>
                <Text style={{fontStyle: 'italic', color:'darkcyan', marginLeft: widthToDp('2%')}} > Description {" \n"} </Text>
                <Text style = {{color: 'grey', marginLeft: widthToDp('2.5%'), marginRight: heightToDp ('1.5%') }}>CompBio@IIT-Roorkee has developed the SwasthGarbh App for the aid of all pregnant women and obstetricians. The Appis available for
                 free and no charges are involved to avail the benefit of App.{'\n \n'}However, the App is not a substitute of a Doctor. In case of any difficulty, kindly contact your doctor. Furthermore, the developers do not owe any legal and medical liability, in case of any untoward consequences.{'\n\n'}
                 Privacy Policy{'\n\n'}Computaional Lab IIT-Roorkee built the SwasthGarbhApp as a Free App. This SERVICE is provided by Computational Lab IIT-Roorkee at no cost and is intended for use as is.{'\n\n'}
                 This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.{'\n\n'}
                 If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.
                 {'\n\n'}Information Collection and Use {"\n\n"}
                 For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to blood pressure, weight and other medical information. The information that we request will be retained by us and used as described in this Privacy Policy.{"\n\n"}
                 The App does use third party services that may collect information used to identified you.{"\n\n"}Link to Privacy Policy of third party service providers used by the App{"\n\n"}
                 [Google Play Services](https://www.google.com/policies/privacy/){"\n\n"}
                 [Firebase Analytics](https://firebase.google.com/policies/analytics){"\n\n"} Log Data{"\n\n"}
                 We want to inform you that whenever you use our Service, in a case of an error in the App we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol ("IP") address, device name, operating system version, the configuration of the App when utilizing our Service,
                 the time and date of your use of the Service, and other statistics.{"\n\n"}
                 Cookies{"\n\n"}Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. these are sent to your browser from the websites that you visit and are stored on your device's internal memory.
                 {"\n\n"}This Service does not use these "cookies" explicitly. However, the App may use third party code and libraries that use "cookies" to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a coockie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service. {"\n\n"}
                 Service Providers{"\n\n"}
                 We may employ third-party companies and individuals due to the following reasons:{"\n\n"}
                 To facilitate our Service{"\n\n"}To provide the Service on our behalf{"\n\n"}To perform Service-related services, or{"\n"}To assist us in analyzing how our Service is used.{"\n\n"}
                 We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated
                 not to disclose or use the information for any other purpose.{"\n\n"}
                 Security{"\n\n"}We value our trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute Security.{"\n\n"}
                 Links to Other Sites{"\n\n"}This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have 
                 no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.{"\n\n"}
                 Children's Privacy{"\n\n"}
                 These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we 
                 immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.{"\n\n"}
                 Changes to This Privacy Policy{"\n\n"}
                 We may update our privacy policy time to time. Thus you are advised to review this page periodically for any changes.
                 We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.{"\n"}</Text>
                </ScrollView>
        );
    }
}