import React, { useState, useEffect } from 'react';
import { Picker, Switch } from 'react-native';
import { Button, Appbar, } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';
import DatePicker from 'react-native-datepicker';
import 'moment-timezone';
import Toast from 'react-native-simple-toast';
import moment from 'moment';


import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


const patient_screenbydoc = (props) => {

  const [his, set_his] = useState(false);
  const [diabetes2, set_diabetes2] = useState(false);
  const [liver, set_liver] = useState(false);
  const [kidney, set_kidney] = useState(false);
  const [apla, set_apla] = useState(false);
  const [hyper, set_hyper] = useState(false);
  const [sle, set_sle] = useState(false);
  const [native, set_native] = useState(false);
  const [postvalve, set_postvalve] = useState(false);
  const [acy, set_acy] = useState(false);
  const [cyn, set_cyn] = useState(false);
  const [ft_text, set_ft_text] = useState('');
  const [anc_text, set_anc_text] = useState('');
  const [mod_text, set_mod_text] = useState('');
  const [babyout_text, set_babyout_text] = useState('');
  const [his_other, set_his_other] = useState('');
  const [drug, set_drug] = useState('');
  const [show, set_show] = useState(false);
  const [visible, set_visible] = useState(false);
  const [dikhao, set_dikhao] = useState(false);
  const [dikhao_anc3, set_dikhao_anc3] = useState(false);
  const [dikhao_anc4, set_dikhao_anc4] = useState(false);
  const [dikhao_anc5, set_dikhao_anc5] = useState(false);
  const [dikhao_anc6, set_dikhao_anc6] = useState(false);
  const [dikhao_anc7, set_dikhao_anc7] = useState(false);
  const [dikhao_anc8, set_dikhao_anc8] = useState(false);

  const [anc1, set_anc1] = useState(false);
  const [anc1date, set_anc1date] = useState('');
  const [anc1_pog, set_anc1_pog] = useState('');
  const [anc1_fever, set_anc1_fever] = useState(false);
  const [anc1_rash, set_anc1_rash] = useState(false);
  const [anc1_vomit, set_anc1_vomit] = useState(false);
  const [anc1_bleed, set_anc1_bleed] = useState(false);
  const [anc1_pain, set_anc1_pain] = useState(false);
  const [anc1_drug, set_anc1_drug] = useState(false);
  const [anc1_smoke, set_anc1_smoke] = useState(false);
  const [anc1_alcohol, set_anc1_alcohol] = useState(false);
  const [anc1_tob, set_anc1_tob] = useState(false);
  const [anc1_caff, set_anc1_caff] = useState(false);
  const [anc1_intimate, set_anc1_intimate] = useState(false);
  const [anc1his_other, set_anc1_hisother] = useState('');
  const [anc1_pallor, set_anc1_pallor] = useState(false);
  const [anc1_ic, set_anc1_ic] = useState(false);
  const [anc1_club, set_anc1_club] = useState(false);
  const [anc1_cyn, set_anc1_cyn] = useState(false);
  const [anc1_edema, set_anc1_edema] = useState(false);
  const [anc1_lym, set_anc1_lym] = useState(false);
  const [anc1_height, set_anc1_height] = useState('');
  const [anc1_weight, set_anc1_weight] = useState('');
  const [anc1_bmi, set_anc1_bmi] = useState('');
  const [anc1_PR, set_anc1_PR] = useState('');
  const [anc1_BP, set_anc1_BP] = useState('');
  const [anc1_RR, set_anc1_RR] = useState('');
  const [anc1_temp, set_anc1_temp] = useState('');
  const [anc1_chest, set_anc1_chest] = useState('');
  const [anc1_PA, set_anc1_PA] = useState('');
  const [anc1_prot, set_anc1_prot] = useState(null);
  const [anc1_bloodgroup, set_anc1_bloodgroup] = useState(null);
  const [anc1_hus_blood, set_anc1_hus_blood] = useState(null);
  const [anc1_bgICT, set_anc1_bgICT] = useState('');
  const [anc1_hiv, set_anc1_hiv] = useState(false);
  const [anc1_hbsag, set_anc1_hbsag] = useState(false);
  const [anc1_vdrl, set_anc1_vdrl] = useState(false);
  const [hiv_switch, set_hiv_switch] = useState(false);
  const [hbsag_switch, set_hbsag_switch] = useState(false);
  const [vdrl_switch, set_vdrl_switch] = useState(false);
  const [anc1_urinerm, set_anc1_urinerm] = useState('');
  const [anc1_urinecs, set_anc1_urinecs] = useState('');
  const [anc1_hem, set_anc1_hem] = useState('');
  const [anc1_TSH, set_anc1_TSH] = useState('');
  const [anc1_bloodfast, set_anc1_bloodfast] = useState('');
  const [anc1_bloodpost, set_anc1_bloodpost] = useState('');
  const [anc1_GTTfast, set_anc1_GTTfast] = useState('');
  const [anc1_GTT1, set_anc1_GTT1] = useState('');
  const [anc1_GTT2, set_anc1_GTT2] = useState('');
  const [anc1_NTdone, set_anc1_NTdone] = useState('');
  const [anc1_NTCRL, set_anc1_NTCRL] = useState('');
  const [anc1_NT, set_anc1_NT] = useState('');
  const [anc1_NTcen, set_anc1_NTcen] = useState('');
  const [anc1_NTtext, set_anc1_NTtext] = useState('');
  const [anc1_left, set_anc1_left] = useState('');
  const [anc1_right, set_anc1_right] = useState('');
  const [anc1_PIGF, set_anc1_PIGF] = useState('');
  const [anc1_PAPP, set_anc1_PAPP] = useState('');
  const [anc1_HCG, set_anc1_HCG] = useState('');
  const [anc1_USGnormal, set_anc1_USGnormal] = useState('');
  const [anc1_USGdone, set_anc1_USGdone] = useState('');
  const [anc1_investother, set_anc1_investother] = useState('');
  const [anc1_examother, set_anc1_examother] = useState('');
  const [anc1_adviceGTT, set_anc1_adviceGTT] = useState(false);
  const [anc1_adviceblood, set_anc1_adviceblood] = useState(false);
  const [anc1_adviceNT, set_anc1_adviceNT] = useState(false);
  const [anc1_advicedual, set_anc1_advicedual] = useState(false);
  const [anc1_advicefolate, set_anc1_advicefolate] = useState(false);
  const [anc1_adviceFeCa, set_anc1_adviceFeCa] = useState(false);
  const [anc1_adviceUSG, set_anc1_adviceUSG] = useState(false);
  const [anc1_advicenut, set_anc1_advicenut] = useState(false);
  const [anc1_advicenaus, set_anc1_advicenaus] = useState(false);
  const [anc1_adviceheart, set_anc1_adviceheart] = useState(false);
  const [anc1_adviceconst, set_anc1_adviceconst] = useState(false);
  const [anc1_advicepedal, set_anc1_advicepedal] = useState(false);
  const [anc1_adviceleg, set_anc1_adviceleg] = useState(false);
  const [anc1_adviceICT, set_anc1_adviceICT] = useState(false);
  const [anc1_advicediabetic, set_anc1_advicediabetic] = useState(false);
  const [anc1_TSHnew, set_anc1_TSHnew] = useState(false);
  const [anc1_nitro, set_anc1_nitro] = useState(false);
  const [anc1_syp, set_anc1_syp] = useState(false);
  const [anc1_Tvit, set_anc1_Tvit] = useState(false);
  const [anc1_fluid, set_anc1_fluid] = useState(false);
  const [anc1_others, set_anc1_others] = useState(false);

  const [anc2_head, set_anc2_head] = useState(false);
  const [anc2_mict, set_anc2_mict] = useState(false);
  const [anc2_per, set_anc2_per] = useState(false);
  const [anc2_pog, set_anc2_pog] = useState('');
  const [anc2_easy, set_anc2_easy] = useState(false);
  const [anc2_short, set_anc2_short] = useState(false);
  const [anc2_spot, set_anc2_spot] = useState(false);
  const [anc2date, set_anc2date] = useState('');
  const [anc2, set_anc2] = useState(false);
  const [anc2_pallor, set_anc2_pallor] = useState(false);
  const [anc2_pedal, set_anc2_pedal] = useState(false);
  const [anc2_PR, set_anc2_PR] = useState('');
  const [anc2_BP, set_anc2_BP] = useState('');
  const [anc2_weight, set_anc2_weight] = useState('');
  const [anc2_PA, set_anc2_PA] = useState('');
  const [anc2_hisother, set_anc2_hisother] = useState('');
  const [anc2_examothers, set_anc2_examothers] = useState('');
  const [anc2_investothers, set_anc2_investothers] = useState('');
  const [anc2_adviceothers, set_anc2_adviceothers] = useState('');
  const [anc2_quad, set_anc2_quad] = useState('');
  const [anc2_adviceGTT, set_anc2_adviceGTT] = useState(false);
  const [anc2_adviceTFe, set_anc2_adviceTFe] = useState(false);
  const [anc2_adviceTCa, set_anc2_adviceTCa] = useState(false);
  const [anc2_adviceinj, set_anc2_adviceinj] = useState(false);
  const [anc2_advicequad, set_anc2_advicequad] = useState(false);
  const [anc2_advicefetal, set_anc2_advicefetal] = useState(false);
  const [anc2_alb, set_anc2_alb] = useState(false);
  const [anc2_Tfe, set_anc2_Tfe] = useState(false);
  const [anc2_HPLC, set_anc2_HPLC] = useState(false);
  const [anc2_peri, set_anc2_peri] = useState(false);
  const [anc2_serum, set_anc2_serum] = useState(false);
  const [anc2_nutadvice, set_anc2_nutadvice] = useState(false);
  const [anc2_advicenausea, set_anc2_advicenausea] = useState(false);
  const [anc2_adviceheart, set_anc2_adviceheart] = useState(false);
  const [anc2_adviceconst, set_anc2_adviceconst] = useState(false);
  const [anc2_advicepedal, set_anc2_advicepedal] = useState(false);
  const [anc2_adviceleg, set_anc2_adviceleg] = useState(false);
  const [anc2_advicediabetic, set_anc2_advicediabetic] = useState(false);

  const [anc3_head, set_anc3_head] = useState(false);
  const [anc3_mict, set_anc3_mict] = useState(false);
  const [anc3_short, set_anc3_short] = useState(false);
  const [anc3_easy, set_anc3_easy] = useState(false);
  const [anc3_spot, set_anc3_spot] = useState(false);
  const [anc3_leak, set_anc3_leak] = useState(false);
  const [anc3_adequate, set_anc3_adequate] = useState(false);
  const [anc3_itching, set_anc3_itching] = useState(false);
  const [anc3date, set_anc3date] = useState('');
  const [anc3_pog, set_anc3_pog] = useState('');
  const [anc3, set_anc3] = useState(false);
  const [anc3_pallor, set_anc3_pallor] = useState(false);
  const [anc3_pedal, set_anc3_pedal] = useState(false);
  const [anc3_PR, set_anc3_PR] = useState('');
  const [anc3_BP, set_anc3_BP] = useState('');
  const [anc3_weight, set_anc3_weight] = useState('');
  const [anc3_PA, set_anc3_PA] = useState('');
  const [anc3_fetalecho, set_anc3_fetalecho] = useState('');
  const [anc3_hisother, set_anc3_hisother] = useState('');
  const [anc3_examothers, set_anc3_examothers] = useState('');
  const [anc3_investothers, set_anc3_investothers] = useState('');
  const [anc3_adviceothers, set_anc3_adviceothers] = useState('');
  const [anc3_adviceurine, set_anc3_adviceurine] = useState(false);
  const [anc3_adviceGTT, set_anc3_adviceGTT] = useState(false);
  const [anc3_adviceICT, set_anc3_adviceICT] = useState(false);
  const [anc3_adviceTCa, set_anc3_adviceTCa] = useState(false);
  const [anc3_adviceinj, set_anc3_adviceinj] = useState(false);
  const [anc3_advicelabour, set_anc3_advicelabour] = useState(false);
  const [anc3_adviceDFMC, set_anc3_adviceDFMC] = useState(false);
  const [anc3_inj, set_anc3_inj] = useState(false);
  const [anc3_nutadvice, set_anc3_nutadvice] = useState(false);
  const [anc3_advicenausea, set_anc3_advicenausea] = useState(false);
  const [anc3_adviceheart, set_anc3_adviceheart] = useState(false);
  const [anc3_adviceconst, set_anc3_adviceconst] = useState(false);
  const [anc3_advicepedal, set_anc3_advicepedal] = useState(false);
  const [anc3_adviceleg, set_anc3_adviceleg] = useState(false);
  const [anc3_advicediabetic, set_anc3_advicediabetic] = useState(false);

  const [anc4_head, set_anc4_head] = useState(false);
  const [anc4_mict, set_anc4_mict] = useState(false);
  const [anc4_short, set_anc4_short] = useState(false);
  const [anc4_easy, set_anc4_easy] = useState(false);
  const [anc4_spot, set_anc4_spot] = useState(false);
  const [anc4_adequate, set_anc4_adequate] = useState(false);
  const [anc4_itching, set_anc4_itching] = useState(false);
  const [anc4date, set_anc4date] = useState('');
  const [anc4_pog, set_anc4_pog] = useState('');
  const [anc4, set_anc4] = useState(false);
  const [anc4_pallor, set_anc4_pallor] = useState(false);
  const [anc4_pedal, set_anc4_pedal] = useState(false);
  const [anc4_PR, set_anc4_PR] = useState('');
  const [anc4_BP, set_anc4_BP] = useState('');
  const [anc4_weight, set_anc4_weight] = useState('');
  const [anc4_PA, set_anc4_PA] = useState('');
  const [anc4_culture, set_anc4_culture] = useState('');
  const [anc4_examICT, set_anc4_examICT] = useState('');
  const [anc4_hisother, set_anc4_hisother] = useState('');
  const [anc4_adviceothers, set_anc4_adviceothers] = useState('');
  const [anc4_adviceTFe, set_anc4_adviceTFe] = useState(false);
  const [anc4_adviceTCa, set_anc4_adviceTCa] = useState(false);
  const [anc4_adviceUSG, set_anc4_adviceUSG] = useState(false);
  const [anc4_adviceCBC, set_anc4_adviceCBC] = useState(false);
  const [anc4_adviceLFT, set_anc4_adviceLFT] = useState(false);
  const [anc4_adviceKFT, set_anc4_adviceKFT] = useState(false);
  const [anc4_advicelabour, set_anc4_advicelabour] = useState(false);
  const [anc4_adviceDFMC, set_anc4_adviceDFMC] = useState(false);
  const [anc4_nutadvice, set_anc4_nutadvice] = useState(false);
  const [anc4_advicenausea, set_anc4_advicenausea] = useState(false);
  const [anc4_adviceheart, set_anc4_adviceheart] = useState(false);
  const [anc4_adviceconst, set_anc4_adviceconst] = useState(false);
  const [anc4_advicepedal, set_anc4_advicepedal] = useState(false);
  const [anc4_adviceleg, set_anc4_adviceleg] = useState(false);
  const [anc4_advicediabetic, set_anc4_advicediabetic] = useState(false);

  const [anc5_head, set_anc5_head] = useState(false);
  const [anc5_mict, set_anc5_mict] = useState(false);
  const [anc5_short, set_anc5_short] = useState(false);
  const [anc5_easy, set_anc5_easy] = useState(false);
  const [anc5_spot, set_anc5_spot] = useState(false);
  const [anc5_adequate, set_anc5_adequate] = useState(false);
  const [anc5_itching, set_anc5_itching] = useState(false);
  const [anc5date, set_anc5date] = useState('');
  const [anc5_pog, set_anc5_pog] = useState('');
  const [anc5, set_anc5] = useState(false);
  const [anc5_pallor, set_anc5_pallor] = useState(false);
  const [anc5_pedal, set_anc5_pedal] = useState(false);
  const [anc5_PR, set_anc5_PR] = useState('');
  const [anc5_BP, set_anc5_BP] = useState('');
  const [anc5_weight, set_anc5_weight] = useState('');
  const [anc5_PA, set_anc5_PA] = useState('');
  const [anc5_HB, set_anc5_HB] = useState('');
  const [anc5_TLC, set_anc5_TLC] = useState('');
  const [anc5_platelets, set_anc5_platelets] = useState('');
  const [anc5_OT, set_anc5_OT] = useState('');
  const [anc5_PT, set_anc5_PT] = useState('');
  const [anc5_ALP, set_anc5_ALP] = useState('');
  const [anc5_urea, set_anc5_urea] = useState('');
  const [anc5_Creat, set_anc5_Creat] = useState('');
  const [anc5_vaginal, set_anc5_vaginal] = useState(false);
  const [anc5_lscs, set_anc5_lscs] = useState(false);
  const [anc5_timing, set_anc5_timing] = useState('');
  const [anc5_adviceothers, set_anc5_adviceothers] = useState('');
  const [anc5_hisother, set_anc5_hisother] = useState('');
  const [anc5_atten, set_anc5_atten] = useState('');
  const [anc5_examothers, set_anc5_examothers] = useState('');
  const [anc5_investcbc, set_anc5_investcbc] = useState('');
  const [anc5_investlft, set_anc5_investlft] = useState('');
  const [anc5_investkft, set_anc5_investkft] = useState('');
  const [anc5_investothers, set_anc5_investothers] = useState('');
  const [anc5_HCweeks, set_anc5_HCweeks] = useState('');
  const [anc5_HCcm, set_anc5_HCcm] = useState('');
  const [anc5_HCcen, set_anc5_HCcen] = useState('');
  const [anc5_usgBPDcm, set_anc5_usgBPDcm] = useState('');
  const [anc5_usgBPDweeks, set_anc5_usgBPDweeks] = useState('');
  const [anc5_usgBPDcentile, set_anc5_usgBPDcentile] = useState('');
  const [anc5_ACweeks, set_anc5_ACweeks] = useState('');
  const [anc5_ACcm, set_anc5_ACcm] = useState('');
  const [anc5_ACcen, set_anc5_ACcen] = useState('');
  const [anc5_FLweeks, set_anc5_FLweeks] = useState('');
  const [anc5_FLcm, set_anc5_FLcm] = useState('');
  const [anc5_FLcen, set_anc5_FLcen] = useState('');
  const [anc5_EFWweeks, set_anc5_EFWweeks] = useState('');
  const [anc5_EFWgm, set_anc5_EFWgm] = useState('');
  const [anc5_EFWcen, set_anc5_EFWcen] = useState('');
  const [anc5_liquorafi, set_anc5_liquorafi] = useState('');
  const [anc5_liquorslp, set_anc5_liquorslp] = useState('');
  const [anc5_UAPI, set_anc5_UAPI] = useState('');
  const [anc5_UAPIcen, set_anc5_UAPIcen] = useState('');
  const [anc5_MCAPI, set_anc5_MCAPI] = useState('');
  const [anc5_MCAPIcen, set_anc5_MCAPIcen] = useState('');
  const [anc5_CPR, set_anc5_CPR] = useState('');
  const [anc5_adviceTFe, set_anc5_adviceTFe] = useState(false);
  const [anc5_adviceman, set_anc5_adviceman] = useState(false);
  const [anc5_advicelabour, set_anc5_advicelabour] = useState(false);
  const [anc5_adviceDFMC, set_anc5_adviceDFMC] = useState(false);
  const [anc5_nutadvice, set_anc5_nutadvice] = useState(false);
  const [anc5_advicenausea, set_anc5_advicenausea] = useState(false);
  const [anc5_adviceheart, set_anc5_adviceheart] = useState(false);
  const [anc5_adviceconst, set_anc5_adviceconst] = useState(false);
  const [anc5_advicepedal, set_anc5_advicepedal] = useState(false);
  const [anc5_adviceleg, set_anc5_adviceleg] = useState(false);
  const [anc5_advicediabetic, set_anc5_advicediabetic] = useState(false);

  const [anc6_head, set_anc6_head] = useState(false);
  const [anc6_mict, set_anc6_mict] = useState(false);
  const [anc6_short, set_anc6_short] = useState(false);
  const [anc6_easy, set_anc6_easy] = useState(false);
  const [anc6_spot, set_anc6_spot] = useState(false);
  const [anc6_adequate, set_anc6_adequate] = useState(false);
  const [anc6_itching, set_anc6_itching] = useState(false);
  const [anc6date, set_anc6date] = useState('');
  const [anc6_pog, set_anc6_pog] = useState('');
  const [anc6, set_anc6] = useState(false);
  const [anc6_pallor, set_anc6_pallor] = useState(false);
  const [anc6_pedal, set_anc6_pedal] = useState(false);
  const [anc6_PR, set_anc6_PR] = useState('');
  const [anc6_BP, set_anc6_BP] = useState('');
  const [anc6_weight, set_anc6_weight] = useState('');
  const [anc6_PA, set_anc6_PA] = useState('');
  const [anc6_adviceothers, set_anc6_adviceothers] = useState('');
  const [anc6_hisother, set_anc6_hisother] = useState('');
  const [anc6_pelvic, set_anc6_pelvic] = useState('');
  const [anc6_examothers, set_anc6_examothers] = useState('');
  const [anc6_adviceTFe, set_anc6_adviceTFe] = useState(false);
  const [anc6_adviceman, set_anc6_adviceman] = useState(false);
  const [anc6_advicelabour, set_anc6_advicelabour] = useState(false);
  const [anc6_adviceDFMC, set_anc6_adviceDFMC] = useState(false);
  const [anc6_nutadvice, set_anc6_nutadvice] = useState(false);
  const [anc6_advicenausea, set_anc6_advicenausea] = useState(false);
  const [anc6_adviceheart, set_anc6_adviceheart] = useState(false);
  const [anc6_adviceconst, set_anc6_adviceconst] = useState(false);
  const [anc6_advicepedal, set_anc6_advicepedal] = useState(false);
  const [anc6_adviceleg, set_anc6_adviceleg] = useState(false);
  const [anc6_advicediabetic, set_anc6_advicediabetic] = useState(false);

  const [anc7_head, set_anc7_head] = useState(false);
  const [anc7_mict, set_anc7_mict] = useState(false);
  const [anc7_short, set_anc7_short] = useState(false);
  const [anc7_easy, set_anc7_easy] = useState(false);
  const [anc7_spot, set_anc7_spot] = useState(false);
  const [anc7_adequate, set_anc7_adequate] = useState(false);
  const [anc7_itching, set_anc7_itching] = useState(false);
  const [anc7date, set_anc7date] = useState('');
  const [anc7_pog, set_anc7_pog] = useState('');
  const [anc7, set_anc7] = useState(false);
  const [anc7_pallor, set_anc7_pallor] = useState(false);
  const [anc7_pedal, set_anc7_pedal] = useState(false);
  const [anc7_PR, set_anc7_PR] = useState('');
  const [anc7_BP, set_anc7_BP] = useState('');
  const [anc7_weight, set_anc7_weight] = useState('');
  const [anc7_PA, set_anc7_PA] = useState('');
  const [anc7_adviceothers, set_anc7_adviceothers] = useState('');
  const [anc7_hisother, set_anc7_hisother] = useState('');
  const [anc7_examothers, set_anc7_examothers] = useState('');
  const [anc7_adviceTFe, set_anc7_adviceTFe] = useState(false);
  const [anc7_adviceman, set_anc7_adviceman] = useState(false);
  const [anc7_advicelabour, set_anc7_advicelabour] = useState(false);
  const [anc7_adviceDFMC, set_anc7_adviceDFMC] = useState(false);
  const [anc7_nutadvice, set_anc7_nutadvice] = useState(false);
  const [anc7_advicenausea, set_anc7_advicenausea] = useState(false);
  const [anc7_adviceheart, set_anc7_adviceheart] = useState(false);
  const [anc7_adviceconst, set_anc7_adviceconst] = useState(false);
  const [anc7_advicepedal, set_anc7_advicepedal] = useState(false);
  const [anc7_adviceleg, set_anc7_adviceleg] = useState(false);
  const [anc7_advicediabetic, set_anc7_advicediabetic] = useState(false);

  const [anc8_head, set_anc8_head] = useState(false);
  const [anc8_mict, set_anc8_mict] = useState(false);
  const [anc8_short, set_anc8_short] = useState(false);
  const [anc8_easy, set_anc8_easy] = useState(false);
  const [anc8_spot, set_anc8_spot] = useState(false);
  const [anc8_adequate, set_anc8_adequate] = useState(false);
  const [anc8_itching, set_anc8_itching] = useState(false);
  const [anc8date, set_anc8date] = useState('');
  const [anc8_pog, set_anc8_pog] = useState('');
  const [anc8, set_anc8] = useState(false);
  const [anc8_pallor, set_anc8_pallor] = useState(false);
  const [anc8_pedal, set_anc8_pedal] = useState(false);
  const [anc8_PR, set_anc8_PR] = useState('');
  const [anc8_BP, set_anc8_BP] = useState('');
  const [anc8_weight, set_anc8_weight] = useState('');
  const [anc8_PA, set_anc8_PA] = useState('');
  const [anc8_adviceothers, set_anc8_adviceothers] = useState('');
  const [anc8_hisother, set_anc8_hisother] = useState('');
  const [anc8_examothers, set_anc8_examothers] = useState('');
  const [anc8_adviceTFe, set_anc8_adviceTFe] = useState(false);
  const [anc8_adviceman, set_anc8_adviceman] = useState(false);
  const [anc8_adviceDFMC, set_anc8_adviceDFMC] = useState(false);
  const [anc8_nutadvice, set_anc8_nutadvice] = useState(false);
  const [anc8_advicenausea, set_anc8_advicenausea] = useState(false);
  const [anc8_adviceheart, set_anc8_adviceheart] = useState(false);
  const [anc8_adviceconst, set_anc8_adviceconst] = useState(false);
  const [anc8_advicepedal, set_anc8_advicepedal] = useState(false);
  const [anc8_adviceleg, set_anc8_adviceleg] = useState(false);
  const [anc8_advicediabetic, set_anc8_advicediabetic] = useState(false);

  const ContentTitle = ({ title, style }) => (
    <Appbar.Content
      title={<Text style={style}> {title} </Text>}
    />
  );

  const toggleSwitch1 = () => set_hiv_switch(previousState => !previousState);
  const toggleSwitch2 = () => set_hbsag_switch(previousState => !previousState);
  const toggleSwitch3 = () => set_vdrl_switch(previousState => !previousState);

  const ShowHideComponent = () => {
    () => set_his()
    if (show == true) {
      set_show(false);
    } else {
      set_show(true);
    }
  };

  const ShowHideComponent_two = () => {
    () => set_anc1()
    if (visible == true) {
      set_visible(false);
    } else {
      set_visible(true);
    }
  };

  const ShowHideComponent_three = () => {
    () => set_anc2()
    if (dikhao == true) {
      set_dikhao(false);
    } else {
      set_dikhao(true);
    }
  };

  const ShowHideComponent_four = () => {
    () => set_anc3()
    if (dikhao_anc3 == true) {
      set_dikhao_anc3(false);
    } else {
      set_dikhao_anc3(true);
    }
  };

  const ShowHideComponent_five = () => {
    () => set_anc4()
    if (dikhao_anc4 == true) {
      set_dikhao_anc4(false);
    } else {
      set_dikhao_anc4(true);
    }
  };

  const ShowHideComponent_six = () => {
    () => set_anc5()
    if (dikhao_anc5 == true) {
      set_dikhao_anc5(false);
    } else {
      set_dikhao_anc5(true);
    }
  };

  const ShowHideComponent_seven = () => {
    () => set_anc6()
    if (dikhao_anc6 == true) {
      set_dikhao_anc6(false);
    } else {
      set_dikhao_anc6(true);
    }
  };

  const ShowHideComponent_eight = () => {
    () => set_anc7()
    if (dikhao_anc7 == true) {
      set_dikhao_anc7(false);
    } else {
      set_dikhao_anc7(true);
    }
  };

  const ShowHideComponent_nine = () => {
    () => set_anc8()
    if (dikhao_anc8 == true) {
      set_dikhao_anc8(false);
    } else {
      set_dikhao_anc8(true);
    }
  };


  const addData = async () => {
    fetch("https://swasthgarbh-backend.herokuapp.com/patientuhid_adddata", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "diabetes2": diabetes2,
        "liver": liver,
        "kidney": kidney,
        "apla": apla,
        "hyper": hyper,
        "sle": sle,
        "mobile_pat": props.route.params.P3,
        "doc_mobile": props.route.params.P4,
        "native": native,
        "postvalve": postvalve,
        "acy": acy,
        "cyn": cyn,
        "ft_text": ft_text,
        "anc_text": anc_text,
        "mod_text": mod_text,
        "babyout_text": babyout_text,
        "his_other": his_other,
        "drug": drug,
        "anc1": anc1,
        "anc1date": anc1date,
        "anc1_pog": anc1_pog,
        "anc1_fever": anc1_fever,
        "anc1_rash": anc1_rash,
        "anc1_vomit": anc1_vomit,
        "anc1_bleed": anc1_bleed,
        "anc1_pain": anc1_pain,
        "anc1_drug": anc1_drug,
        "anc1_smoke": anc1_smoke,
        "anc1_alcohol": anc1_alcohol,
        "anc1_tob": anc1_tob,
        "anc1_caff": anc1_caff,
        "anc1_intimate": anc1_intimate,
        "anc1his_other": anc1his_other,
        "anc1_pallor": anc1_pallor,
        "anc1_ic": anc1_ic,
        "anc1_club": anc1_club,
        "anc1_cyn": anc1_cyn,
        "anc1_edema": anc1_edema,
        "anc1_lym": anc1_lym,
        "anc1_height": anc1_height,
        "anc1_weight": anc1_weight,
        "anc1_bmi": anc1_bmi,
        "anc1_PR": anc1_PR,
        "anc1_BP": anc1_BP,
        "anc1_PR": anc1_RR,
        "anc1_temp": anc1_temp,
        "anc1_chest": anc1_chest,
        "anc1_PA": anc1_PA,
        "anc1_prot": anc1_prot,
        "anc1_bloodgroup": anc1_bloodgroup,
        "anc1_hus_blood": anc1_hus_blood,
        "anc1_bgICT": anc1_bgICT,
        "anc1_hiv": anc1_hiv,
        "anc1_hbsag": anc1_hbsag,
        "anc1_vdrl": anc1_vdrl,
        "hiv_switch": hiv_switch,
        "hbsag_switch": hbsag_switch,
        "vdrl_switch": vdrl_switch,
        "anc1_urinerm": anc1_urinerm,
        "anc1_urinecs": anc1_urinecs,
        "anc1_hem": anc1_hem,
        "anc1_TSH": anc1_TSH,
        "anc1_bloodfast": anc1_bloodfast,
        "anc1_bloodpost": anc1_bloodpost,
        "anc1_GTTfast": anc1_GTTfast,
        "anc1_GTT1": anc1_GTT1,
        "anc1_GTT2": anc1_GTT2,
        "anc1_NTdone": anc1_NTdone,
        "anc1_NTCRL": anc1_NTCRL,
        "anc1_NT": anc1_NT,
        "anc1_NTcen": anc1_NTcen,
        "anc1_NTtext": anc1_NTtext,
        "anc1_left": anc1_left,
        "anc1_right": anc1_right,
        "anc1_PIGF": anc1_PIGF,
        "anc1_PAPP": anc1_PAPP,
        "anc1_HCG": anc1_HCG,
        "anc1_USGnormal": anc1_USGnormal,
        "anc1_USGdone": anc1_USGdone,
        "anc1_investother": anc1_investother,
        "anc1_examother": anc1_examother,
        "anc1_adviceGTT": anc1_adviceGTT,
        "anc1_adviceblood": anc1_adviceblood,
        "anc1_adviceNT": anc1_adviceNT,
        "anc1_advicedual": anc1_advicedual,
        "anc1_advicefolate": anc1_advicefolate,
        "anc1_adviceFeCa": anc1_adviceFeCa,
        "anc1_adviceUSG": anc1_adviceUSG,
        "anc1_advicenut": anc1_advicenut,
        "anc1_advicenaus": anc1_advicenaus,
        "anc1_adviceheart": anc1_adviceheart,
        "anc1_adviceconst": anc1_adviceconst,
        "anc1_advicepedal": anc1_advicepedal,
        "anc1_adviceleg": anc1_adviceleg,
        "anc1_adviceICT": anc1_adviceICT,
        "anc1_advicediabetic": anc1_advicediabetic,
        "anc1_TSHnew": anc1_TSHnew,
        "anc1_nitro": anc1_nitro,
        "anc1_syp": anc1_syp,
        "anc1_Tvit": anc1_Tvit,
        "anc1_fluid": anc1_fluid,
        "anc1_others": anc1_others,
        "anc2_head": anc2_head,
        "anc2_mict": anc2_mict,
        "anc2_per": anc2_per,
        "anc2_pog": anc2_pog,
        "anc2_easy": anc2_easy,
        "anc2_short": anc2_short,
        "anc2_spot": anc2_spot,
        "anc2date": anc2date,
        "anc2_pallor": anc2_pallor,
        "anc2_pedal": anc2_pedal,
        "anc2_PR": anc2_PR,
        "anc2_BP": anc2_BP,
        "anc2_weight": anc2_weight,
        "anc2_PA": anc2_PA,
        "anc2_hisother": anc2_hisother,
        "anc2_examothers": anc2_examothers,
        "anc2_investothers": anc2_investothers,
        "anc2_adviceothers": anc2_adviceothers,
        "anc2_quad": anc2_quad,
        "anc2_adviceGTT": anc2_adviceGTT,
        "anc2_adviceTFe": anc2_adviceTFe,
        "anc2_adviceTCa": anc2_adviceTCa,
        "anc2_adviceinj": anc2_adviceinj,
        "anc2_advicequad": anc2_advicequad,
        "anc2_advicefetal": anc2_advicefetal,
        "anc2_alb": anc2_alb,
        "anc2_Tfe": anc2_Tfe,
        "anc2_HPLC": anc2_HPLC,
        "anc2_peri": anc2_peri,
        "anc2_serum": anc2_serum,
        "anc2_nutadvice": anc2_nutadvice,
        "anc2_advicenausea": anc2_advicenausea,
        "anc2_adviceheart": anc2_adviceheart,
        "anc2_adviceconst": anc2_adviceconst,
        "anc2_advicepedal": anc2_advicepedal,
        "anc2_adviceled": anc2_adviceleg,
        "anc2_advicediabetic": anc2_advicediabetic,
        "anc3_head": anc3_head,
        "anc3_mict": anc3_mict,
        "anc3_short": anc3_short,
        "anc3_easy": anc3_easy,
        "anc3_spot": anc3_spot,
        "anc3_leak": anc3_leak,
        "anc3_adequate": anc3_adequate,
        "anc3_itching": anc3_itching,
        "anc3date": anc3date,
        "anc3_pog": anc3_pog,
        "anc3_pallor": anc3_pallor,
        "anc3_pedal": anc3_pedal,
        "anc3_PR": anc3_PR,
        "anc3_BP": anc3_BP,
        "anc33_weight": anc3_weight,
        "anc3_PA": anc3_PA,
        "anc3_fetalecho": anc3_fetalecho,
        "anc3_hisother": anc3_hisother,
        "anc3_examothers": anc3_examothers,
        "anc3_adviceothers": anc3_adviceothers,
        "anc3_inestothers": anc3_investothers,
        "anc3_adviceurine": anc3_adviceurine,
        "anc3_adviceGTT": anc3_adviceGTT,
        "anc3_adviceICT": anc3_adviceICT,
        "anc3_adviceTCa": anc3_adviceTCa,
        "anc3_adviceinj": anc3_adviceinj,
        "anc3_advicelabour": anc3_advicelabour,
        "anc3_adviceDFMC": anc3_adviceDFMC,
        "anc3_inj": anc3_inj,
        "anc3_nutadvice": anc3_nutadvice,
        "anc3_advicenausea": anc3_advicenausea,
        "anc3_adviceheart": anc3_adviceheart,
        "anc3_adviceconst": anc3_adviceconst,
        "anc3_advicepedal": anc3_advicepedal,
        "anc3_adviceleg": anc3_adviceleg,
        "anc3_advicediabetic": anc3_advicediabetic,
        "anc4_head": anc4_head,
        "anc4_mict": anc4_mict,
        "anc4_short": anc4_short,
        "anc4_easy": anc4_easy,
        "anc4_spot": anc4_spot,
        "anc4_adequate": anc4_adequate,
        "anc4_itching": anc4_itching,
        "anc4date": anc4date,
        "anc4_pog": anc4_pog,
        "anc4_pallor": anc4_pallor,
        "anc4_pedal": anc4_pedal,
        "anc4_PR": anc4_PR,
        "anc4_BP": anc4_BP,
        "anc4_weight": anc4_weight,
        "anc4_PA": anc4_PA,
        "anc4_culture": anc4_culture,
        "anc4_examICT": anc4_examICT,
        "anc4_hisother": anc4_hisother,
        "anc4_adviceothers": anc4_adviceothers,
        "anc4_adviceTFe": anc4_adviceTFe,
        "anc4_adviceTCa": anc4_adviceTCa,
        "anc4_adviceUSG": anc4_adviceUSG,
        "anc4_adviceCBC": anc4_adviceCBC,
        "anc4_adviceKFT": anc4_adviceKFT,
        "anc4_adviceLFT": anc4_adviceLFT,
        "anc4_advicelabour": anc4_advicelabour,
        "anc4_adviceDFMC": anc4_adviceDFMC,
        "anc4_nutadvice": anc4_nutadvice,
        "anc4_advicenausea": anc4_advicenausea,
        "anc4_adviceheart": anc4_adviceheart,
        "anc4_adviceconst": anc4_adviceconst,
        "anc4_advicepedal": anc4_advicepedal,
        "anc4_adviceleg": anc4_adviceleg,
        "anc4_advicediabetic": anc4_advicediabetic,
        "anc5_head": anc5_head,
        "anc5_mict": anc5_mict,
        "anc5_short": anc5_short,
        "anc5_easy": anc5_easy,
        "anc5_spot": anc5_spot,
        "anc5_adequate": anc5_adequate,
        "anc5_itching": anc5_itching,
        "anc5date": anc5date,
        "anc5_pog": anc5_pog,
        "anc5_pallor": anc5_pallor,
        "anc5_pedal": anc5_pedal,
        "anc5_PR": anc5_PR,
        "anc5_BP": anc5_BP,
        "anc5_weight": anc5_weight,
        "anc5_PA": anc5_PA,
        "anc5_HB": anc5_HB,
        "anc5_TLC": anc5_TLC,
        "anc5_platelets": anc5_platelets,
        "anc5_OT": anc5_OT,
        "anc5_PT": anc5_PT,
        "anc5_ALP": anc5_ALP,
        "anc5_urea": anc5_urea,
        "anc5_Creat": anc5_Creat,
        "anc5_vaginal": anc5_vaginal,
        "anc5_lscs": anc5_lscs,
        "anc5_timing": anc5_timing,
        "anc5_adviceothers": anc5_adviceothers,
        "anc5_hisother": anc5_hisother,
        "anc5_atten": anc5_atten,
        "anc5_examothers": anc5_examothers,
        "anc5_investcbc": anc5_investcbc,
        "anc5_investlft": anc5_investlft,
        "anc5_investkft": anc5_investkft,
        "anc5_investothers": anc5_investothers,
        "anc5_HCweeks": anc5_HCweeks,
        "anc5_HCcm": anc5_HCcm,
        "anc5_HCcen": anc5_HCcen,
        "anc5_usgBPDcm": anc5_usgBPDcm,
        "anc5_usgBPDweeks": anc5_usgBPDweeks,
        "anc5_usgBPDcentile": anc5_usgBPDcentile,
        "anc5_ACweeks": anc5_ACweeks,
        "anc5_ACcm": anc5_ACcm,
        "anc5_ACcen": anc5_ACcen,
        "anc5_FLweeks": anc5_FLweeks,
        "anc5_FLcm": anc5_FLcm,
        "anc5_FLcen": anc5_FLcen,
        "anc5_EFWweeks": anc5_EFWweeks,
        "anc5_EFWgm": anc5_EFWgm,
        "anc5_EFWcen": anc5_EFWcen,
        "anc5_liquorafi": anc5_liquorafi,
        "anc5_liqourslp": anc5_liquorslp,
        "anc5_UAPI": anc5_UAPI,
        "anc5_UAPIcen": anc5_UAPIcen,
        "anc5_MCAPI": anc5_MCAPI,
        "anc5_MCAPIcen": anc5_MCAPIcen,
        "anc5_CPR": anc5_CPR,
        "anc5_adviceTFe": anc5_adviceTFe,
        "anc5_adviceman": anc5_adviceman,
        "anc5_advicelabour": anc5_advicelabour,
        "anc5_adviceDFMC": anc5_adviceDFMC,
        "anc5_nutadvice": anc5_nutadvice,
        "anc5_advicenausea": anc5_advicenausea,
        "anc5_adviceheart": anc5_adviceheart,
        "anc5_adviceconst": anc5_adviceconst,
        "anc5_advicepedal": anc5_advicepedal,
        "anc5_adviceleg": anc5_adviceleg,
        "anc5_advicediabetic": anc5_advicediabetic,
        "anc6_head": anc6_head,
        "anc6_mict": anc6_mict,
        "anc6_short": anc6_short,
        "anc6_easy": anc6_easy,
        "anc6_spot": anc6_spot,
        "anc6_adequate": anc6_adequate,
        "anc6_itching": anc6_itching,
        "anc6date": anc6date,
        "anc6_pog": anc6_pog,
        "anc6_pallor": anc6_pallor,
        "anc6_pedal": anc6_pedal,
        "anc6_PR": anc6_PR,
        "anc6_BP": anc6_BP,
        "anc6_weight": anc6_weight,
        "anc6_PA": anc6_PA,
        "anc6_adviceothers": anc6_adviceothers,
        "anc6_hisother": anc6_hisother,
        "anc6_pelvic": anc6_pelvic,
        "anc6_examothers": anc6_examothers,
        "anc6_adviceTFe": anc6_adviceTFe,
        "anc6_adviceman": anc6_adviceman,
        "anc6_advicelabour": anc6_advicelabour,
        "anc6_adviceDFMC": anc6_adviceDFMC,
        "anc6_nutadvice": anc6_nutadvice,
        "anc6_advicenausea": anc6_advicenausea,
        "anc6_adviceheart": anc6_adviceheart,
        "anc6_adviceconst": anc6_adviceconst,
        "anc6_advicepedal": anc6_advicepedal,
        "anc6_adviceleg": anc6_adviceleg,
        "anc6_advicediabetic": anc6_advicediabetic,
        "anc7_head": anc7_head,
        "anc7_mict": anc7_mict,
        "anc7_short": anc7_short,
        "anc7_easy": anc7_easy,
        "anc7_spot": anc7_spot,
        "anc7_adequate": anc7_adequate,
        "anc7_itching": anc7_itching,
        "anc7_pog": anc7_pog,
        "anc7_pallor": anc7_pallor,
        "anc7_pedal": anc7_pedal,
        "anc7_PR": anc7_PR,
        "anc7_BP": anc7_BP,
        "anc7_weight": anc7_weight,
        "anc7_adviceothers": anc7_adviceothers,
        "anc7_adviceTFe": anc7_adviceTFe,
        "anc7_adviceman": anc7_adviceman,
        "anc7_advicelabour": anc7_advicelabour,
        "anc7_adviceDFMC": anc7_adviceDFMC,
        "anc7_advicenausea": anc7_advicenausea,
        "anc7_nutadvice": anc7_nutadvice,
        "anc7_adviceheart": anc7_adviceheart,
        "anc7_adviceheart": anc7_adviceconst,
        "anc7_advicepedal": anc7_advicepedal,
        "anc7_adviceleg": anc7_adviceleg,
        "anc7_advicediabetic": anc7_advicediabetic,
        "anc8_head": anc8_head,
        "anc8_mict": anc8_mict,
        "anc8_short": anc8_short,
        "anc8_easy": anc8_easy,
        "anc8_spot": anc8_spot,
        "anc8_adequate": anc8_adequate,
        "anc8_itching": anc8_itching,
        "anc8_pog": anc8_pog,
        "anc8_pallor": anc8_pallor,
        "anc8_pedal": anc8_pedal,
        "anc8_PR": anc8_PR,
        "anc8_BP": anc8_BP,
        "anc8_weight": anc8_weight,
        "anc8_PA": anc8_PA,
        "anc8_adviceothers": anc8_adviceothers,
        "anc8_hisother": anc8_hisother,
        "anc8_examothers": anc8_examothers,
        "anc8_adviceTFe": anc8_adviceTFe,
        "anc8_adviceman": anc8_adviceman,
        "anc8_adviceDFMC": anc8_adviceDFMC,
        "anc8_advicenausea": anc8_advicenausea,
        "anc8_nutadvice": anc8_nutadvice,
        "anc8_adviceheart": anc8_adviceheart,
        "anc8_adviceconst": anc8_adviceconst,
        "anc8_advicepedal": anc8_advicepedal,
        "anc8_adviceleg": anc8_adviceleg,
        "anc8_advicediabetic": anc8_advicediabetic
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log(data)
      })
  }

  const updateData = async () => {

    let n = []
    const patientuhiddata = await fetch('https://swasthgarbh-backend.herokuapp.com/patientuhid_datalist')
    const patientuhid_data = await patientuhiddata.json();
    for (let i = 0; i < patientuhid_data.length; i++) {
      if (patientuhid_data[i].mobile_pat == props.route.params.P3) {
        n.push(patientuhid_data[i])
        console.log["whats there", n]
        console.log("update patient uhid data")

        fetch("https://swasthgarbh-backend.herokuapp.com/patient_udatedata/" + patientuhid_data[i]._id, {
          method: "PUT",
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "diabetes2": diabetes2,
            "liver": liver,
            "kidney": kidney,
            "apla": apla,
            "hyper": hyper,
            "sle": sle,
            "native": native,
            "postvalve": postvalve,
            "acy": acy,
            "cyn": cyn,
            "ft_text": ft_text,
            "anc_text": anc_text,
            "mod_text": mod_text,
            "babyout_text": babyout_text,
            "his_other": his_other,
            "drug": drug,
            "anc1": anc1,
            "anc1date": anc1date,
            "anc1_pog": anc1_pog,
            "anc1_fever": anc1_fever,
            "anc1_rash": anc1_rash,
            "anc1_vomit": anc1_vomit,
            "anc1_bleed": anc1_bleed,
            "anc1_pain": anc1_pain,
            "anc1_drug": anc1_drug,
            "anc1_smoke": anc1_smoke,
            "anc1_alcohol": anc1_alcohol,
            "anc1_tob": anc1_tob,
            "anc1_caff": anc1_caff,
            "anc1_intimate": anc1_intimate,
            "anc1his_other": anc1his_other,
            "anc1_pallor": anc1_pallor,
            "anc1_ic": anc1_ic,
            "anc1_club": anc1_club,
            "anc1_cyn": anc1_cyn,
            "anc1_edema": anc1_edema,
            "anc1_lym": anc1_lym,
            "anc1_height": anc1_height,
            "anc1_weight": anc1_weight,
            "anc1_bmi": anc1_bmi,
            "anc1_PR": anc1_PR,
            "anc1_BP": anc1_BP,
            "anc1_PR": anc1_RR,
            "anc1_temp": anc1_temp,
            "anc1_chest": anc1_chest,
            "anc1_PA": anc1_PA,
            "anc1_prot": anc1_prot,
            "anc1_bloodgroup": anc1_bloodgroup,
            "anc1_hus_blood": anc1_hus_blood,
            "anc1_bgICT": anc1_bgICT,
            "anc1_hiv": anc1_hiv,
            "anc1_hbsag": anc1_hbsag,
            "anc1_vdrl": anc1_vdrl,
            "hiv_switch": hiv_switch,
            "hbsag_switch": hbsag_switch,
            "vdrl_switch": vdrl_switch,
            "anc1_urinerm": anc1_urinerm,
            "anc1_urinecs": anc1_urinecs,
            "anc1_hem": anc1_hem,
            "anc1_TSH": anc1_TSH,
            "anc1_bloodfast": anc1_bloodfast,
            "anc1_bloodpost": anc1_bloodpost,
            "anc1_GTTfast": anc1_GTTfast,
            "anc1_GTT1": anc1_GTT1,
            "anc1_GTT2": anc1_GTT2,
            "anc1_NTdone": anc1_NTdone,
            "anc1_NTCRL": anc1_NTCRL,
            "anc1_NT": anc1_NT,
            "anc1_NTcen": anc1_NTcen,
            "anc1_NTtext": anc1_NTtext,
            "anc1_left": anc1_left,
            "anc1_right": anc1_right,
            "anc1_PIGF": anc1_PIGF,
            "anc1_PAPP": anc1_PAPP,
            "anc1_HCG": anc1_HCG,
            "anc1_USGnormal": anc1_USGnormal,
            "anc1_USGdone": anc1_USGdone,
            "anc1_investother": anc1_investother,
            "anc1_examother": anc1_examother,
            "anc1_adviceGTT": anc1_adviceGTT,
            "anc1_adviceblood": anc1_adviceblood,
            "anc1_adviceNT": anc1_adviceNT,
            "anc1_advicedual": anc1_advicedual,
            "anc1_advicefolate": anc1_advicefolate,
            "anc1_adviceFeCa": anc1_adviceFeCa,
            "anc1_adviceUSG": anc1_adviceUSG,
            "anc1_advicenut": anc1_advicenut,
            "anc1_advicenaus": anc1_advicenaus,
            "anc1_adviceheart": anc1_adviceheart,
            "anc1_adviceconst": anc1_adviceconst,
            "anc1_advicepedal": anc1_advicepedal,
            "anc1_adviceleg": anc1_adviceleg,
            "anc1_adviceICT": anc1_adviceICT,
            "anc1_advicediabetic": anc1_advicediabetic,
            "anc1_TSHnew": anc1_TSHnew,
            "anc1_nitro": anc1_nitro,
            "anc1_syp": anc1_syp,
            "anc1_Tvit": anc1_Tvit,
            "anc1_fluid": anc1_fluid,
            "anc1_others": anc1_others,
            "anc2_head": anc2_head,
            "anc2_mict": anc2_mict,
            "anc2_per": anc2_per,
            "anc2_pog": anc2_pog,
            "anc2_easy": anc2_easy,
            "anc2_short": anc2_short,
            "anc2_spot": anc2_spot,
            "anc2date": anc2date,
            "anc2_pallor": anc2_pallor,
            "anc2_pedal": anc2_pedal,
            "anc2_PR": anc2_PR,
            "anc2_BP": anc2_BP,
            "anc2_weight": anc2_weight,
            "anc2_PA": anc2_PA,
            "anc2_hisother": anc2_hisother,
            "anc2_examothers": anc2_examothers,
            "anc2_investothers": anc2_investothers,
            "anc2_adviceothers": anc2_adviceothers,
            "anc2_quad": anc2_quad,
            "anc2_adviceGTT": anc2_adviceGTT,
            "anc2_adviceTFe": anc2_adviceTFe,
            "anc2_adviceTCa": anc2_adviceTCa,
            "anc2_adviceinj": anc2_adviceinj,
            "anc2_advicequad": anc2_advicequad,
            "anc2_advicefetal": anc2_advicefetal,
            "anc2_alb": anc2_alb,
            "anc2_Tfe": anc2_Tfe,
            "anc2_HPLC": anc2_HPLC,
            "anc2_peri": anc2_peri,
            "anc2_serum": anc2_serum,
            "anc2_nutadvice": anc2_nutadvice,
            "anc2_advicenausea": anc2_advicenausea,
            "anc2_adviceheart": anc2_adviceheart,
            "anc2_adviceconst": anc2_adviceconst,
            "anc2_advicepedal": anc2_advicepedal,
            "anc2_adviceled": anc2_adviceleg,
            "anc2_advicediabetic": anc2_advicediabetic,
            "anc3_head": anc3_head,
            "anc3_mict": anc3_mict,
            "anc3_short": anc3_short,
            "anc3_easy": anc3_easy,
            "anc3_spot": anc3_spot,
            "anc3_leak": anc3_leak,
            "anc3_adequate": anc3_adequate,
            "anc3_itching": anc3_itching,
            "anc3date": anc3date,
            "anc3_pog": anc3_pog,
            "anc3_pallor": anc3_pallor,
            "anc3_pedal": anc3_pedal,
            "anc3_PR": anc3_PR,
            "anc3_BP": anc3_BP,
            "anc33_weight": anc3_weight,
            "anc3_PA": anc3_PA,
            "anc3_fetalecho": anc3_fetalecho,
            "anc3_hisother": anc3_hisother,
            "anc3_examothers": anc3_examothers,
            "anc3_adviceothers": anc3_adviceothers,
            "anc3_inestothers": anc3_investothers,
            "anc3_adviceurine": anc3_adviceurine,
            "anc3_adviceGTT": anc3_adviceGTT,
            "anc3_adviceICT": anc3_adviceICT,
            "anc3_adviceTCa": anc3_adviceTCa,
            "anc3_adviceinj": anc3_adviceinj,
            "anc3_advicelabour": anc3_advicelabour,
            "anc3_adviceDFMC": anc3_adviceDFMC,
            "anc3_inj": anc3_inj,
            "anc3_nutadvice": anc3_nutadvice,
            "anc3_advicenausea": anc3_advicenausea,
            "anc3_adviceheart": anc3_adviceheart,
            "anc3_adviceconst": anc3_adviceconst,
            "anc3_advicepedal": anc3_advicepedal,
            "anc3_adviceleg": anc3_adviceleg,
            "anc3_advicediabetic": anc3_advicediabetic,
            "anc4_head": anc4_head,
            "anc4_mict": anc4_mict,
            "anc4_short": anc4_short,
            "anc4_easy": anc4_easy,
            "anc4_spot": anc4_spot,
            "anc4_adequate": anc4_adequate,
            "anc4_itching": anc4_itching,
            "anc4date": anc4date,
            "anc4_pog": anc4_pog,
            "anc4_pallor": anc4_pallor,
            "anc4_pedal": anc4_pedal,
            "anc4_PR": anc4_PR,
            "anc4_BP": anc4_BP,
            "anc4_weight": anc4_weight,
            "anc4_PA": anc4_PA,
            "anc4_culture": anc4_culture,
            "anc4_examICT": anc4_examICT,
            "anc4_hisother": anc4_hisother,
            "anc4_adviceothers": anc4_adviceothers,
            "anc4_adviceTFe": anc4_adviceTFe,
            "anc4_adviceTCa": anc4_adviceTCa,
            "anc4_adviceUSG": anc4_adviceUSG,
            "anc4_adviceCBC": anc4_adviceCBC,
            "anc4_adviceKFT": anc4_adviceKFT,
            "anc4_adviceLFT": anc4_adviceLFT,
            "anc4_advicelabour": anc4_advicelabour,
            "anc4_adviceDFMC": anc4_adviceDFMC,
            "anc4_nutadvice": anc4_nutadvice,
            "anc4_advicenausea": anc4_advicenausea,
            "anc4_adviceheart": anc4_adviceheart,
            "anc4_adviceconst": anc4_adviceconst,
            "anc4_advicepedal": anc4_advicepedal,
            "anc4_adviceleg": anc4_adviceleg,
            "anc4_advicediabetic": anc4_advicediabetic,
            "anc5_head": anc5_head,
            "anc5_mict": anc5_mict,
            "anc5_short": anc5_short,
            "anc5_easy": anc5_easy,
            "anc5_spot": anc5_spot,
            "anc5_adequate": anc5_adequate,
            "anc5_itching": anc5_itching,
            "anc5date": anc5date,
            "anc5_pog": anc5_pog,
            "anc5_pallor": anc5_pallor,
            "anc5_pedal": anc5_pedal,
            "anc5_PR": anc5_PR,
            "anc5_BP": anc5_BP,
            "anc5_weight": anc5_weight,
            "anc5_PA": anc5_PA,
            "anc5_HB": anc5_HB,
            "anc5_TLC": anc5_TLC,
            "anc5_platelets": anc5_platelets,
            "anc5_OT": anc5_OT,
            "anc5_PT": anc5_PT,
            "anc5_ALP": anc5_ALP,
            "anc5_urea": anc5_urea,
            "anc5_Creat": anc5_Creat,
            "anc5_vaginal": anc5_vaginal,
            "anc5_lscs": anc5_lscs,
            "anc5_timing": anc5_timing,
            "anc5_adviceothers": anc5_adviceothers,
            "anc5_hisother": anc5_hisother,
            "anc5_atten": anc5_atten,
            "anc5_examothers": anc5_examothers,
            "anc5_investcbc": anc5_investcbc,
            "anc5_investlft": anc5_investlft,
            "anc5_investkft": anc5_investkft,
            "anc5_investothers": anc5_investothers,
            "anc5_HCweeks": anc5_HCweeks,
            "anc5_HCcm": anc5_HCcm,
            "anc5_HCcen": anc5_HCcen,
            "anc5_usgBPDcm": anc5_usgBPDcm,
            "anc5_usgBPDweeks": anc5_usgBPDweeks,
            "anc5_usgBPDcentile": anc5_usgBPDcentile,
            "anc5_ACweeks": anc5_ACweeks,
            "anc5_ACcm": anc5_ACcm,
            "anc5_ACcen": anc5_ACcen,
            "anc5_FLweeks": anc5_FLweeks,
            "anc5_FLcm": anc5_FLcm,
            "anc5_FLcen": anc5_FLcen,
            "anc5_EFWweeks": anc5_EFWweeks,
            "anc5_EFWgm": anc5_EFWgm,
            "anc5_EFWcen": anc5_EFWcen,
            "anc5_liquorafi": anc5_liquorafi,
            "anc5_liqourslp": anc5_liquorslp,
            "anc5_UAPI": anc5_UAPI,
            "anc5_UAPIcen": anc5_UAPIcen,
            "anc5_MCAPI": anc5_MCAPI,
            "anc5_MCAPIcen": anc5_MCAPIcen,
            "anc5_CPR": anc5_CPR,
            "anc5_adviceTFe": anc5_adviceTFe,
            "anc5_adviceman": anc5_adviceman,
            "anc5_advicelabour": anc5_advicelabour,
            "anc5_adviceDFMC": anc5_adviceDFMC,
            "anc5_nutadvice": anc5_nutadvice,
            "anc5_advicenausea": anc5_advicenausea,
            "anc5_adviceheart": anc5_adviceheart,
            "anc5_adviceconst": anc5_adviceconst,
            "anc5_advicepedal": anc5_advicepedal,
            "anc5_adviceleg": anc5_adviceleg,
            "anc5_advicediabetic": anc5_advicediabetic,
            "anc6_head": anc6_head,
            "anc6_mict": anc6_mict,
            "anc6_short": anc6_short,
            "anc6_easy": anc6_easy,
            "anc6_spot": anc6_spot,
            "anc6_adequate": anc6_adequate,
            "anc6_itching": anc6_itching,
            "anc6date": anc6date,
            "anc6_pog": anc6_pog,
            "anc6_pallor": anc6_pallor,
            "anc6_pedal": anc6_pedal,
            "anc6_PR": anc6_PR,
            "anc6_BP": anc6_BP,
            "anc6_weight": anc6_weight,
            "anc6_PA": anc6_PA,
            "anc6_adviceothers": anc6_adviceothers,
            "anc6_hisother": anc6_hisother,
            "anc6_pelvic": anc6_pelvic,
            "anc6_examothers": anc6_examothers,
            "anc6_adviceTFe": anc6_adviceTFe,
            "anc6_adviceman": anc6_adviceman,
            "anc6_advicelabour": anc6_advicelabour,
            "anc6_adviceDFMC": anc6_adviceDFMC,
            "anc6_nutadvice": anc6_nutadvice,
            "anc6_advicenausea": anc6_advicenausea,
            "anc6_adviceheart": anc6_adviceheart,
            "anc6_adviceconst": anc6_adviceconst,
            "anc6_advicepedal": anc6_advicepedal,
            "anc6_adviceleg": anc6_adviceleg,
            "anc6_advicediabetic": anc6_advicediabetic,
            "anc7_head": anc7_head,
            "anc7_mict": anc7_mict,
            "anc7_short": anc7_short,
            "anc7_easy": anc7_easy,
            "anc7_spot": anc7_spot,
            "anc7_adequate": anc7_adequate,
            "anc7_itching": anc7_itching,
            "anc7_pog": anc7_pog,
            "anc7_pallor": anc7_pallor,
            "anc7_pedal": anc7_pedal,
            "anc7_PR": anc7_PR,
            "anc7_BP": anc7_BP,
            "anc7_weight": anc7_weight,
            "anc7_adviceothers": anc7_adviceothers,
            "anc7_adviceTFe": anc7_adviceTFe,
            "anc7_adviceman": anc7_adviceman,
            "anc7_advicelabour": anc7_advicelabour,
            "anc7_adviceDFMC": anc7_adviceDFMC,
            "anc7_advicenausea": anc7_advicenausea,
            "anc7_nutadvice": anc7_nutadvice,
            "anc7_adviceheart": anc7_adviceheart,
            "anc7_adviceheart": anc7_adviceconst,
            "anc7_advicepedal": anc7_advicepedal,
            "anc7_adviceleg": anc7_adviceleg,
            "anc7_advicediabetic": anc7_advicediabetic,
            "anc8_head": anc8_head,
            "anc8_mict": anc8_mict,
            "anc8_short": anc8_short,
            "anc8_easy": anc8_easy,
            "anc8_spot": anc8_spot,
            "anc8_adequate": anc8_adequate,
            "anc8_itching": anc8_itching,
            "anc8_pog": anc8_pog,
            "anc8_pallor": anc8_pallor,
            "anc8_pedal": anc8_pedal,
            "anc8_PR": anc8_PR,
            "anc8_BP": anc8_BP,
            "anc8_weight": anc8_weight,
            "anc8_PA": anc8_PA,
            "anc8_adviceothers": anc8_adviceothers,
            "anc8_hisother": anc8_hisother,
            "anc8_examothers": anc8_examothers,
            "anc8_adviceTFe": anc8_adviceTFe,
            "anc8_adviceman": anc8_adviceman,
            "anc8_adviceDFMC": anc8_adviceDFMC,
            "anc8_advicenausea": anc8_advicenausea,
            "anc8_nutadvice": anc8_nutadvice,
            "anc8_adviceheart": anc8_adviceheart,
            "anc8_adviceconst": anc8_adviceconst,
            "anc8_advicepedal": anc8_advicepedal,
            "anc8_adviceleg": anc8_adviceleg,
            "anc8_advicediabetic": anc8_advicediabetic
          })
        })

      }
    }

  }


  useEffect(() => {
    addData();
  }, [])

  return (
    <>
      <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
      <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
        <ContentTitle title="Add data" style={{ color: 'white', fontSize: 20 }} />
      </Appbar.Header>
      <ScrollView style={{ backgroundColor: "#FAE3E9" }}>

        <View style={{ flexDirection: "row" }}>
          <Text style={{ marginLeft: 80, marginTop: 10, color: "red", fontSize: 15 }}>LMP</Text>
          <Text style={{ marginLeft: 160, marginTop: 10, color: "red", fontSize: 15 }}>EDD</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Text style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: 60, color: "black", marginTop: 20, marginBottom: 8 }}> {props.route.params.P1} </Text>
          <Text style={{ flex: 1, flexDirection: "column", alignItems: "center", marginLeft: 40, color: "black", marginTop: 20, marginBottom: 8 }}> {props.route.params.P2} </Text>
        </View>



        <View>
          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={his}
                onValueChange={(his) => { set_his(his); ShowHideComponent() }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>History</Text>
            </View>
            {show ? (
              <Text style={styles.subheading}>Co-Mobrbidities</Text>
            ) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <CheckBox value={hyper} onValueChange={set_hyper}
                  style={{ alignSelf: "flex-start", marginTop: 2 }} />
                <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Chronic Hypertension</Text>
              </View>
            ) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <CheckBox value={diabetes2} onValueChange={set_diabetes2}
                  style={{ alignSelf: "flex-start", marginTop: 2 }} />
                <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Type 2 diabetes</Text>
              </View>
            ) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <CheckBox value={liver} onValueChange={set_liver}
                  style={{ alignSelf: "flex-start", marginTop: 2 }} />
                <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Chronic Liver Disease</Text>
              </View>
            ) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <CheckBox value={kidney} onValueChange={set_kidney}
                  style={{ alignSelf: "flex-start", marginTop: 2 }} />
                <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Chronic Kidney Disease</Text>
              </View>) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <CheckBox value={apla} onValueChange={set_apla}
                  style={{ alignSelf: "flex-start", marginTop: 2 }} />
                <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>APLA</Text>
              </View>
            ) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <CheckBox value={sle} onValueChange={set_sle}
                  style={{ alignSelf: "flex-start", marginTop: 2 }} />
                <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>SLE</Text>
              </View>
            ) : null}
            {show ? (
              <Text style={styles.subheading}>Obstetric History</Text>) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>FT/PT/Abortion</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={ft_text}
                  onChangeText={(ft_text) => set_ft_text(ft_text)}
                >
                </TextInput>
              </View>) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>ANC Complication</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc_text}
                  onChangeText={(anc_text) => set_anc_text(anc_text)}
                >
                </TextInput>
              </View>) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>MOD</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={mod_text}
                  onChangeText={(mod_text) => set_mod_text(mod_text)}
                >
                </TextInput>

              </View>) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Baby Outcome</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={babyout_text}
                  onChangeText={(babyout_text) => set_babyout_text(babyout_text)}
                >
                </TextInput>
              </View>
            ) : null}
            {show ? (
              <View style={styles.container2}>
                <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>Heart Disease</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={native} onValueChange={set_native}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>RHD (native valve)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={postvalve} onValueChange={set_postvalve}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>RHD (post valve replacement)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={acy} onValueChange={set_acy}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Acyanotic Cogenital Heart Disease</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={cyn} onValueChange={set_cyn}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Cyanotic Congenital Heart Disease</Text>
                </View>
              </View>) : null}

            {show ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={his_other}
                  onChangeText={(his_other) => set_his_other(his_other)}
                >
                </TextInput>
              </View>) : null}
            {show ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, marginBottom: 20 }}>Drug History</Text>
                <TextInput style={styles.textinput}
                  multiline={true}
                  value={drug}
                  onChangeText={(drug) => set_drug(drug)}
                >
                </TextInput>
              </View>) : null}

          </View>

          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={anc1}
                onValueChange={(anc1) => {
                  set_anc1(anc1);
                  ShowHideComponent_two()
                }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>ANC 1</Text>
            </View>
            {visible ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>Date</Text>
                <DatePicker
                  style={{ width: 150, marginTop: 10, marginLeft: 123, position: "absolute" }}
                  date={anc1date}
                  onDateChange={anc1date => {
                    set_anc1date(anc1date);

                  }}
                  //The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 10
                    },
                    dateInput: {
                      marginLeft: 55
                    }
                  }}>
                </DatePicker>
              </View>
            ) : null}

            {visible ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>POG</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc1_pog}
                  onChangeText={(anc1_pog) => set_anc1_pog(anc1_pog)}
                >
                </TextInput>
              </View>
            ) : null}

            {visible ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>History</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_fever} onValueChange={set_anc1_fever}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Fever</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_rash} onValueChange={set_anc1_rash}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Rash</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_vomit} onValueChange={set_anc1_vomit}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea and Vomiting</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_bleed} onValueChange={set_anc1_bleed}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Bleeding</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_pain} onValueChange={set_anc1_pain}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pain Abdomen</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_drug} onValueChange={set_anc1_drug}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Drug Intake</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_smoke} onValueChange={set_anc1_smoke}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Smoking</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_alcohol} onValueChange={set_anc1_alcohol}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Alcohol</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_tob} onValueChange={set_anc1_tob}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Tobacco Intake</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_caff} onValueChange={set_anc1_caff}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Caffeine Intake</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_intimate} onValueChange={set_anc1_intimate}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Intimate Partner Violence</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc1his_other}
                    onChangeText={(anc1his_other) => set_anc1_hisother(anc1his_other)}
                  >
                  </TextInput>
                </View>

              </View>
            ) : null}


            {visible ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Examination</Text>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>General</Text>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_pallor} onValueChange={set_anc1_pallor}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pallor</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_ic} onValueChange={set_anc1_ic}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Icterus</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_club} onValueChange={set_anc1_club}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Clubbing</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_cyn} onValueChange={set_anc1_cyn}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Cyanosis</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_edema} onValueChange={set_anc1_edema}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Edema</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_lym} onValueChange={set_anc1_lym}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Lymphadenopathy</Text>
                  </View>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>Anthropometry</Text>

                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Height</Text>
                    <TextInput style={styles.textinput2}
                      multiline={true}
                      value={anc1_height}
                      onChangeText={(anc1_height) => set_anc1_height(anc1_height)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Weight</Text>
                    <TextInput style={styles.textinput2}
                      multiline={true}
                      value={anc1_weight}
                      onChangeText={(anc1_weight) => set_anc1_weight(anc1_weight)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BMI</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="(wt/ht(in m)^2)"
                      multiline={true}
                      value={anc1_bmi}
                      onChangeText={(anc1_bmi) => set_anc1_bmi(anc1_bmi)}
                    >
                    </TextInput>
                  </View>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>Vitals</Text>

                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PR</Text>
                    <TextInput style={styles.textinput2}
                      multiline={true}
                      value={anc1_PR}
                      onChangeText={(anc1_PR) => set_anc1_PR(anc1_PR)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BP</Text>
                    <TextInput style={styles.textinput2}
                      multiline={true}
                      value={anc1_BP}
                      onChangeText={(anc1_BP) => set_anc1_BP(anc1_BP)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>RR</Text>
                    <TextInput style={styles.textinput2}
                      multiline={true}
                      value={anc1_RR}
                      onChangeText={(anc1_RR) => set_anc1_RR(anc1_RR)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Temp</Text>
                    <TextInput style={styles.textinput2}
                      multiline={true}
                      value={anc1_temp}
                      onChangeText={(anc1_temp) => set_anc1_temp(anc1_temp)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Chest/CSV</Text>
                    <TextInput style={styles.textinput2}
                      multiline={true}
                      value={anc1_chest}
                      onChangeText={(anc1_chest) => set_anc1_chest(anc1_chest)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>P/A</Text>
                    <TextInput style={styles.textinput2}
                      multiline={true}
                      value={anc1_PA}
                      onChangeText={(anc1_PA) => set_anc1_PA(anc1_PA)}
                    >
                    </TextInput>
                  </View>

                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Proteinuria</Text>
                  <Picker style={{ marginTop: 0 }}
                    selectedValue={anc1_prot}
                    onValueChange={(itemValue, itemIndex) => set_anc1_prot(itemValue)}>
                    <Picker.Item label="Select" value="Select" color="#f06292"></Picker.Item>
                    <Picker.Item label="Nil" value="Nil"></Picker.Item>
                    <Picker.Item label="Trace" value="Trace"></Picker.Item>
                    <Picker.Item label="1+" value="1+"></Picker.Item>
                    <Picker.Item label="2+" value="2+"></Picker.Item>
                    <Picker.Item label="3+" value="3+"></Picker.Item>
                  </Picker>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc1_examother}
                    onChangeText={(anc1_examother) => set_anc1_examother(anc1_examother)}
                  >
                  </TextInput>
                </View>

              </View>
            ) : null}

            {visible ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Investigations</Text>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Blood Group</Text>
                <Picker style={{ marginTop: 0 }}
                  selectedValue={anc1_bloodgroup}
                  onValueChange={(itemValue, itemIndex) => set_anc1_bloodgroup(itemValue)}>
                  <Picker.Item label="Select" value="Select" color="#f06292"></Picker.Item>
                  <Picker.Item label="A+" value="A+"></Picker.Item>
                  <Picker.Item label="A-" value="A-"></Picker.Item>
                  <Picker.Item label="B+" value="B+"></Picker.Item>
                  <Picker.Item label="B-" value="B-"></Picker.Item>
                  <Picker.Item label="AB+" value="AB+"></Picker.Item>
                  <Picker.Item label="AB-" value="AB-"></Picker.Item>
                  <Picker.Item label="O+" value="0+"></Picker.Item>
                  <Picker.Item label="O-" value="0-"></Picker.Item>
                </Picker>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 5 }}>Husband's Blood Group</Text>
                <Picker style={{ marginTop: 0 }}
                  selectedValue={anc1_hus_blood}
                  onValueChange={(itemValue, itemIndex) => set_anc1_hus_blood(itemValue)}>
                  <Picker.Item label="Select" value="Select" color="#f06292"></Picker.Item>
                  <Picker.Item label="A+" value="A+"></Picker.Item>
                  <Picker.Item label="A-" value="A-"></Picker.Item>
                  <Picker.Item label="B+" value="B+"></Picker.Item>
                  <Picker.Item label="B-" value="B-"></Picker.Item>
                  <Picker.Item label="AB+" value="AB+"></Picker.Item>
                  <Picker.Item label="AB-" value="AB-"></Picker.Item>
                  <Picker.Item label="O+" value="0+"></Picker.Item>
                  <Picker.Item label="O-" value="0-"></Picker.Item>
                </Picker>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>If BG Negative- ICT (+/-)</Text>
                  <TextInput style={styles.textinput_newinpink}
                    multiline={true}
                    value={anc1_bgICT}
                    onChangeText={(anc1_bgICT) => {
                      set_anc1_bgICT(anc1_bgICT);
                    }}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_hiv} onValueChange={set_anc1_hiv}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>HIV</Text>
                  <Text style={{ marginLeft: 200, marginTop: 8, color: "#525252", fontSize: 14 }}>-</Text>
                  <Switch
                    value={hiv_switch}
                    onValueChange={toggleSwitch1}
                    trackColor={{ true: 'pink', false: 'grey' }}
                    thumbColor='pink'
                    style={{ flex: 1 }}
                  ></Switch>
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>+</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_hbsag} onValueChange={set_anc1_hbsag}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>HbsAg</Text>
                  <Text style={{ marginLeft: 180, marginTop: 8, color: "#525252", fontSize: 14 }}>-</Text>
                  <Switch
                    value={hbsag_switch}
                    onValueChange={toggleSwitch2}
                    trackColor={{ true: 'pink', false: 'grey' }}
                    thumbColor='pink'
                    style={{ flex: 1 }}
                  ></Switch>
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>+</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_vdrl} onValueChange={set_anc1_vdrl}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>VDRL</Text>
                  <Text style={{ marginLeft: 190, marginTop: 8, color: "#525252", fontSize: 14 }}>-</Text>
                  <Switch
                    value={vdrl_switch}
                    onValueChange={toggleSwitch3}
                    trackColor={{ true: 'pink', false: 'grey' }}
                    thumbColor='pink'
                    style={{ flex: 1 }}
                  ></Switch>
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>+</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Urine R/M</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc1_urinerm}
                    onChangeText={(anc1_urinerm) => set_anc1_urinerm(anc1_urinerm)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Urine C/S</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc1_urinecs}
                    onChangeText={(anc1_urinecs) => set_anc1_urinecs(anc1_urinecs)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Hemogram</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc1_hem}
                    onChangeText={(anc1_hem) => set_anc1_hem(anc1_hem)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>TSH</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc1_TSH}
                    onChangeText={(anc1_TSH) => set_anc1_TSH(anc1_TSH)}
                  >
                  </TextInput>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>Blood sugar</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Fasting</Text>
                    <TextInput style={styles.textinput}
                      placeholder="fasting (mg/dL)"
                      multiline={true}
                      value={anc1_bloodfast}
                      onChangeText={(anc1_bloodfast) => set_anc1_bloodfast(anc1_bloodfast)}
                    >
                    </TextInput></View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Post Prandial</Text>
                    <TextInput style={styles.textinput}
                      placeholder="post prandial (mg/dL)"
                      multiline={true}
                      value={anc1_bloodpost}
                      onChangeText={(anc1_bloodpost) => set_anc1_bloodpost(anc1_bloodpost)}
                    >
                    </TextInput>
                  </View>
                </View>

                <View style={styles.container}>

                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>GTT with 75gm glucose</Text>
                  <View style={{ flexDirection: "column" }}>
                    <TextInput
                      placeholder="fasting (mg/dL)"
                      multiline={true}
                      value={anc1_GTTfast}
                      onChangeText={(anc1_GTTfast) => set_anc1_GTTfast(anc1_GTTfast)}
                    >
                    </TextInput>


                    <TextInput
                      placeholder="1 hr (mg/dL)"
                      multiline={true}
                      value={anc1_GTT1}
                      onChangeText={(anc1_GTT1) => set_anc1_GTT1(anc1_GTT1)}
                    >
                    </TextInput>


                    <TextInput
                      placeholder="2 hr (mg/dL)"
                      multiline={true}
                      value={anc1_GTT2}
                      onChangeText={(anc1_GTT2) => set_anc1_GTT2(anc1_GTT2)}
                    >
                    </TextInput>
                  </View>

                </View>


                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>NT/NB scan (between 11-13+6 weeks)</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Done/Not done</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="fasting (mg/dL)"
                      multiline={true}
                      value={anc1_NTdone}
                      onChangeText={(anc1_NTdone) => set_anc1_NTdone(anc1_NTdone)}
                    >
                    </TextInput>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>CRL</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="1 hr (mg/dL)"
                      multiline={true}
                      value={anc1_NTCRL}
                      onChangeText={(anc1_NTCRL) => set_anc1_NTCRL(anc1_NTCRL)}
                    >
                    </TextInput>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>NT</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="2 hr (mg/dL)"
                      multiline={true}
                      value={anc1_NT}
                      onChangeText={(anc1_NT) => set_anc1_NT(anc1_NT)}
                    >
                    </TextInput>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Centile</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="2 hr (mg/dL)"
                      multiline={true}
                      value={anc1_NTcen}
                      onChangeText={(anc1_NTcen) => set_anc1_NTcen(anc1_NTcen)}
                    >
                    </TextInput>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Text</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="2 hr (mg/dL)"
                      multiline={true}
                      value={anc1_NTtext}
                      onChangeText={(anc1_NTtext) => set_anc1_NTtext(anc1_NTtext)}
                    >
                    </TextInput>
                  </View>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Left Uterine Artry PI</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="2 hr (mg/dL)"
                    multiline={true}
                    value={anc1_left}
                    onChangeText={(anc1_left) => set_anc1_left(anc1_left)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Right Uterine Artry PI</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="2 hr (mg/dL)"
                    multiline={true}
                    value={anc1_right}
                    onChangeText={(anc1_right) => set_anc1_right(anc1_right)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PIGF</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="2 hr (mg/dL)"
                    multiline={true}
                    value={anc1_PIGF}
                    onChangeText={(anc1_PIGF) => set_anc1_PIGF(anc1_PIGF)}
                  >
                  </TextInput>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>Dual Screen (between 11-13+6)</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PAPP-A(MOM)</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="fasting (mg/dL)"
                      multiline={true}
                      value={anc1_PAPP}
                      onChangeText={(anc1_PAPP) => set_anc1_PAPP(anc1_PAPP)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>B HCG(MOM)</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="1 hr (mg/dL)"
                      multiline={true}
                      value={anc1_HCG}
                      onChangeText={(anc1_HCG) => set_anc1_HCG(anc1_HCG)}
                    >
                    </TextInput>
                  </View>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>Level II USG (between 18-20 weeks)</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Done/Not done</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="fasting (mg/dL)"
                      multiline={true}
                      value={anc1_USGdone}
                      onChangeText={(anc1_USGdone) => set_anc1_USGdone(anc1_USGdone)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Normal/Abnormal</Text>
                    <TextInput style={styles.textinput2}
                      placeholder="1 hr (mg/dL)"
                      multiline={true}
                      value={anc1_USGnormal}
                      onChangeText={(anc1_USGnormal) => set_anc1_USGnormal(anc1_USGnormal)}
                    >
                    </TextInput>
                  </View>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc1_investother}
                    onChangeText={(anc1_investother) => set_anc1_investother(anc1_investother)}
                  >
                  </TextInput>
                </View>

              </View>) : null}

            {visible ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceGTT} onValueChange={set_anc1_adviceGTT}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>GTT with 75gm glucose</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceblood} onValueChange={set_anc1_adviceblood}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Blood Sugar</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceNT} onValueChange={set_anc1_adviceNT}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>NT/NC Scan</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_advicedual} onValueChange={set_anc1_advicedual}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Dual Screen</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_advicefolate} onValueChange={set_anc1_advicefolate}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T folate 5 mg OD (if less than 14 weeks)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceFeCa} onValueChange={set_anc1_adviceFeCa}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe OD, T Ca BD (if greater than 14 weeks)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceICT} onValueChange={set_anc1_adviceICT}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>ICT</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceUSG} onValueChange={set_anc1_adviceUSG}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Level II USG (between 18-20 weeks)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_advicenut} onValueChange={set_anc1_advicenut}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nutritional Advice</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={diabetes2} onValueChange={set_diabetes2}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Type 2 diabetes</Text>
                </View>

                <Text style={{ marginTop: 10, color: "grey", fontStyle: "italic", fontSize: 15 }}>Common Ailment Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_advicenaus} onValueChange={set_anc1_advicenaus}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea Vomiting</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceheart} onValueChange={set_anc1_adviceheart}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Heart Burn</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceconst} onValueChange={set_anc1_adviceconst}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Constipation</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_advicepedal} onValueChange={set_anc1_advicepedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal Edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_adviceleg} onValueChange={set_anc1_adviceleg}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leg Cramps</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_advicediabetic} onValueChange={set_anc1_advicediabetic}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Diabetic</Text>
                </View>
              </View>) : null}

            {visible ? (
              <View style={styles.container3}>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc1_TSHnew} onValueChange={set_anc1_TSHnew}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>If TSH is greater than 3.5 mU/L- T Eltroxin 25mg</Text>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>If Urine culture +ve</Text>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_nitro} onValueChange={set_anc1_nitro}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Nitrofurantoin 100mg BD X 7 days</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_syp} onValueChange={set_anc1_syp}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Syp Alkasol 2tsp TDS X alt day for 14 days</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_Tvit} onValueChange={set_anc1_Tvit}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Vit C OD</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc1_fluid} onValueChange={set_anc1_fluid}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Plenty of fluids</Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc1_others}
                    onChangeText={(anc1_others) => set_anc1_others(anc1_others)}
                  >
                  </TextInput>
                </View>

              </View>) : null}
          </View>

          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={anc2}
                onValueChange={(anc2) => {
                  set_anc2(anc2);
                  ShowHideComponent_three()
                }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>ANC 2</Text>
            </View>
            {dikhao ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>Date</Text>
                <DatePicker
                  style={{ width: 150, marginTop: 10, marginLeft: 123, position: "absolute" }}
                  date={anc2date}
                  onDateChange={anc2date => {
                    set_anc2date(anc2date);

                  }}
                  //The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 10
                    },
                    dateInput: {
                      marginLeft: 55
                    }
                  }}>
                </DatePicker>
              </View>
            ) : null}

            {dikhao ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>POG</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc2_pog}
                  onChangeText={(anc2_pog) => set_anc2_pog(anc2_pog)}
                >
                </TextInput>
              </View>
            ) : null}

            {dikhao ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>History</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_short} onValueChange={set_anc2_short}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Shortness of breath</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_easy} onValueChange={set_anc2_easy}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Easy fatigability</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_head} onValueChange={set_anc2_head}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Headache/ epigastric pain/ blurring of vision/ {"\n"}
                    nausea</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_spot} onValueChange={set_anc2_spot}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Bleeding/spotting PV</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_mict} onValueChange={set_anc2_mict}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Burning micturition/ increased frequency of {"\n"} micturition</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_per} onValueChange={set_anc2_per}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Quickening Perceived</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc2_hisother}
                    onChangeText={(anc2_hisother) => set_anc2_hisother(anc2_hisother)}
                  >
                  </TextInput>
                </View>

              </View>
            ) : null}

            {dikhao ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Examination</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_pallor} onValueChange={set_anc2_pallor}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pallor</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_pedal} onValueChange={set_anc2_pedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PR</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc2_PR}
                    onChangeText={(anc2_PR) => set_anc2_PR(anc2_PR)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BP</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(Systolic/Diastolic)"
                    multiline={true}
                    value={anc2_BP}
                    onChangeText={(anc2_BP) => set_anc2_BP(anc2_BP)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Weight</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc2_weight}
                    onChangeText={(anc2_weight) => set_anc2_weight(anc2_weight)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>P/A</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(should be within 2 weeks of POG)"
                    multiline={true}
                    value={anc2_PA}
                    onChangeText={(anc2_PA) => set_anc2_PA(anc2_PA)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc2_examothers}
                    onChangeText={(anc2_examothers) => set_anc2_examothers(anc2_examothers)}
                  >
                  </TextInput>
                </View>

              </View>) : null}
            {dikhao ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Investigations</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Quadruple Screen</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc2_quad}
                    onChangeText={(anc2_quad) => set_anc2_quad(anc2_quad)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc2_investothers}
                    onChangeText={(anc2_investothers) => set_anc2_investothers(anc2_investothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}

            {dikhao ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_adviceGTT} onValueChange={set_anc2_adviceGTT}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>GTT with 75gm glucose (24-28 weeks)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_adviceTFe} onValueChange={set_anc2_adviceTFe}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe OD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_adviceTCa} onValueChange={set_anc2_adviceTCa}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Ca BD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_adviceinj} onValueChange={set_anc2_adviceinj}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Inj Tetanus 0.5 ml IM</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_advicequad} onValueChange={set_anc2_advicequad}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Quadruple Screen</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_advicefetal} onValueChange={set_anc2_advicefetal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Fetal Echo</Text>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>If Hb less than 10 g/dl</Text>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc2_alb} onValueChange={set_anc2_alb}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Albendazole 400mg stat</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc2_Tfe} onValueChange={set_anc2_Tfe}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe BD</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc2_HPLC} onValueChange={set_anc2_HPLC}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>HPLC</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc2_peri} onValueChange={set_anc2_peri}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Peripheral smear</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc2_serum} onValueChange={set_anc2_serum}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Serum iron studies</Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_nutadvice} onValueChange={set_anc2_nutadvice}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nutritional Advice</Text>
                </View>
                <Text style={{ marginTop: 10, color: "grey", fontStyle: "italic", fontSize: 15 }}>Common Ailment Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_advicenausea} onValueChange={set_anc2_advicenausea}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea Vomiting</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_adviceheart} onValueChange={set_anc2_adviceheart}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Heart Burn</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_adviceconst} onValueChange={set_anc2_adviceconst}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Constipation</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_advicepedal} onValueChange={set_anc2_advicepedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal Edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_adviceleg} onValueChange={set_anc2_adviceleg}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leg Cramps</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc2_advicediabetic} onValueChange={set_anc2_advicediabetic}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Diabetic</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc2_adviceothers}
                    onChangeText={(anc2_adviceothers) => set_anc2_adviceothers(anc2_adviceothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}

          </View>

          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={anc3}
                onValueChange={(anc3) => {
                  set_anc3(anc3);
                  ShowHideComponent_four()
                }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>ANC 3</Text>
            </View>
            {dikhao_anc3 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>Date</Text>
                <DatePicker
                  style={{ width: 150, marginTop: 10, marginLeft: 123, position: "absolute" }}
                  date={anc3date}
                  onDateChange={anc3date => {
                    set_anc3date(anc3date);

                  }}
                  //The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 10
                    },
                    dateInput: {
                      marginLeft: 55
                    }
                  }}>
                </DatePicker>
              </View>
            ) : null}

            {dikhao_anc3 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>POG</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc3_pog}
                  onChangeText={(anc3_pog) => set_anc3_pog(anc3_pog)}
                >
                </TextInput>
              </View>
            ) : null}

            {dikhao_anc3 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>History</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_short} onValueChange={set_anc3_short}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Shortness of breath</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_easy} onValueChange={set_anc3_easy}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Easy fatigability</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_head} onValueChange={set_anc3_head}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Headache/ epigastric pain/ blurring of vision/ {"\n"}
                    nausea</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_spot} onValueChange={set_anc3_spot}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Bleeding/spotting PV</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_leak} onValueChange={set_anc3_leak}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leaking PV/ Discharge PV</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_mict} onValueChange={set_anc3_mict}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Burning micturition/ increased frequency of {"\n"} micturition</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adequate} onValueChange={set_anc3_adequate}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Adequate fetal movements</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_itching} onValueChange={set_anc3_itching}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Generalized itching</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc3_hisother}
                    onChangeText={(anc3_hisother) => set_anc3_hisother(anc3_hisother)}
                  >
                  </TextInput>
                </View>

              </View>
            ) : null}

            {dikhao_anc3 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Examination</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_pallor} onValueChange={set_anc3_pallor}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pallor</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_pedal} onValueChange={set_anc3_pedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PR</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc3_PR}
                    onChangeText={(anc3_PR) => set_anc3_PR(anc3_PR)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BP</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(Systolic/Diastolic)"
                    multiline={true}
                    value={anc3_BP}
                    onChangeText={(anc3_BP) => set_anc3_BP(anc3_BP)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Weight</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc3_weight}
                    onChangeText={(anc3_weight) => set_anc3_weight(anc3_weight)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>P/A</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(should be within 2 weeks of POG)"
                    multiline={true}
                    value={anc3_PA}
                    onChangeText={(anc3_PA) => set_anc3_PA(anc3_PA)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Fetal Echo</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc3_fetalecho}
                    onChangeText={(anc3_fetalecho) => set_anc3_fetalecho(anc3_fetalecho)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc3_examothers}
                    onChangeText={(anc3_examothers) => set_anc3_examothers(anc3_examothers)}
                  >
                  </TextInput>
                </View>

              </View>) : null}
            {dikhao_anc3 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Investigations</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc3_investothers}
                    onChangeText={(anc3_investothers) => set_anc3_investothers(anc3_investothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}

            {dikhao_anc3 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceTCa} onValueChange={set_anc3_adviceTCa}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe OD/ T Ca BD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceDFMC} onValueChange={set_anc3_adviceDFMC}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>DFMC/ LLP</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceinj} onValueChange={set_anc3_adviceinj}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Inj Tetanus 0.5 ml IM</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceurine} onValueChange={set_anc3_adviceurine}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Urine C/S</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceICT} onValueChange={set_anc3_adviceICT}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>ICT</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceGTT} onValueChange={set_anc3_adviceGTT}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>GTT with 75gm glucose</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_advicelabour} onValueChange={set_anc3_advicelabour}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Review in labour room {"\n"} if (Bleeding PV, Spotting PV, Leaking PV,  {"\n"} Reduced fetal movement, Pain abdomen)</Text>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>If ICT negative</Text>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc3_inj} onValueChange={set_anc3_inj}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Inj Anti D 300mg IM</Text>
                  </View>

                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_nutadvice} onValueChange={set_anc3_nutadvice}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nutritional Advice</Text>
                </View>
                <Text style={{ marginTop: 10, color: "grey", fontStyle: "italic", fontSize: 15 }}>Common Ailment Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_advicenausea} onValueChange={set_anc3_advicenausea}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea Vomiting</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceheart} onValueChange={set_anc3_adviceheart}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Heart Burn</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceconst} onValueChange={set_anc3_adviceconst}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Constipation</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_advicepedal} onValueChange={set_anc3_advicepedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal Edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_adviceleg} onValueChange={set_anc3_adviceleg}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leg Cramps</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc3_advicediabetic} onValueChange={set_anc3_advicediabetic}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Diabetic</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc3_adviceothers}
                    onChangeText={(anc3_adviceothers) => set_anc3_adviceothers(anc3_adviceothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}

          </View>


          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={anc4}
                onValueChange={(anc4) => {
                  set_anc4(anc4);
                  ShowHideComponent_five()
                }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>ANC 4</Text>
            </View>
            {dikhao_anc4 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>Date</Text>
                <DatePicker
                  style={{ width: 150, marginTop: 10, marginLeft: 123, position: "absolute" }}
                  date={anc4date}
                  onDateChange={anc4date => {
                    set_anc4date(anc4date);

                  }}
                  //The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 10
                    },
                    dateInput: {
                      marginLeft: 55
                    }
                  }}>
                </DatePicker>
              </View>
            ) : null}

            {dikhao_anc4 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>POG</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc4_pog}
                  onChangeText={(anc4_pog) => set_anc4_pog(anc4_pog)}
                >
                </TextInput>
              </View>
            ) : null}

            {dikhao_anc4 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>History</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_short} onValueChange={set_anc4_short}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Shortness of breath</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_easy} onValueChange={set_anc4_easy}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Easy fatigability</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_head} onValueChange={set_anc4_head}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Headache/ epigastric pain/ blurring of vision/ {"\n"}
                    nausea</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_spot} onValueChange={set_anc4_spot}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Bleeding/spotting PV</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_mict} onValueChange={set_anc4_mict}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Burning micturition/ increased frequency of {"\n"} micturition</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adequate} onValueChange={set_anc4_adequate}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Adequate fetal movements</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_itching} onValueChange={set_anc4_itching}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Generalized itching</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc4_hisother}
                    onChangeText={(anc4_hisother) => set_anc4_hisother(anc4_hisother)}
                  >
                  </TextInput>
                </View>

              </View>
            ) : null}

            {dikhao_anc4 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Examination</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_pallor} onValueChange={set_anc4_pallor}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pallor</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_pedal} onValueChange={set_anc4_pedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PR</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc4_PR}
                    onChangeText={(anc4_PR) => set_anc4_PR(anc4_PR)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BP</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(Systolic/Diastolic)"
                    multiline={true}
                    value={anc4_BP}
                    onChangeText={(anc4_BP) => set_anc4_BP(anc4_BP)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Weight</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc4_weight}
                    onChangeText={(anc4_weight) => set_anc4_weight(anc4_weight)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>P/A</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(should be within 2 weeks of POG)"
                    multiline={true}
                    value={anc4_PA}
                    onChangeText={(anc4_PA) => set_anc4_PA(anc4_PA)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Urine Culture</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc4_culture}
                    onChangeText={(anc4_culture) => set_anc4_culture(anc4_culture)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>ICT</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc4_examICT}
                    onChangeText={(anc4_examICT) => set_anc4_examICT(anc4_examICT)}
                  >
                  </TextInput>
                </View>

              </View>) : null}


            {dikhao_anc4 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceTFe} onValueChange={set_anc4_adviceTFe}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe OD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceTCa} onValueChange={set_anc4_adviceTCa}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Ca BD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceDFMC} onValueChange={set_anc4_adviceDFMC}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>DFMC/ LLP</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceUSG} onValueChange={set_anc4_adviceUSG}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>USG for growth and liquor at 32 weeks</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceCBC} onValueChange={set_anc4_adviceCBC}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>CBC</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceLFT} onValueChange={set_anc4_adviceLFT}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>LFT</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceKFT} onValueChange={set_anc4_adviceKFT}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>KFT</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_nutadvice} onValueChange={set_anc4_nutadvice}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nutritional Advice</Text>
                </View>
                <Text style={{ marginTop: 10, color: "grey", fontStyle: "italic", fontSize: 15 }}>Common Ailment Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_advicenausea} onValueChange={set_anc4_advicenausea}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea Vomiting</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceheart} onValueChange={set_anc4_adviceheart}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Heart Burn</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceconst} onValueChange={set_anc4_adviceconst}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Constipation</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_advicepedal} onValueChange={set_anc4_advicepedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal Edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_adviceleg} onValueChange={set_anc4_adviceleg}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leg Cramps</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_advicediabetic} onValueChange={set_anc4_advicediabetic}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Diabetic</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc4_advicelabour} onValueChange={set_anc4_advicelabour}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Review in labour room {"\n"} if (Bleeding PV, Spotting PV, Leaking PV,  {"\n"} Reduced fetal movement, Pain abdomen)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc4_adviceothers}
                    onChangeText={(anc4_adviceothers) => set_anc4_adviceothers(anc4_adviceothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}
          </View>

          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={anc5}
                onValueChange={(anc5) => {
                  set_anc5(anc5);
                  ShowHideComponent_six()
                }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>ANC 5</Text>
            </View>
            {dikhao_anc5 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>Date</Text>
                <DatePicker
                  style={{ width: 150, marginTop: 10, marginLeft: 123, position: "absolute" }}
                  date={anc5date}
                  onDateChange={anc5date => {
                    set_anc5date(anc5date);

                  }}
                  //The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 10
                    },
                    dateInput: {
                      marginLeft: 55
                    }
                  }}>
                </DatePicker>
              </View>
            ) : null}

            {dikhao_anc5 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>POG</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc5_pog}
                  onChangeText={(anc5_pog) => set_anc5_pog(anc5_pog)}
                >
                </TextInput>
              </View>
            ) : null}

            {dikhao_anc5 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>History</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_short} onValueChange={set_anc5_short}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Shortness of breath</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_easy} onValueChange={set_anc5_easy}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Easy fatigability</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_head} onValueChange={set_anc5_head}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Headache/ epigastric pain/ blurring of vision/ {"\n"}
                    nausea</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_spot} onValueChange={set_anc5_spot}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Bleeding/spotting PV</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_mict} onValueChange={set_anc5_mict}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Burning micturition/ increased frequency of {"\n"} micturition</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_adequate} onValueChange={set_anc5_adequate}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Adequate fetal movements</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_itching} onValueChange={set_anc5_itching}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Generalized itching</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc5_hisother}
                    onChangeText={(anc5_hisother) => set_anc5_hisother(anc5_hisother)}
                  >
                  </TextInput>
                </View>

                <View style={styles.container}>
                  <Text style={{ fontSize: 13, color: "#525252", fontWeight: "bold", fontStyle: "italic" }}>Councelling regarding Birth Plan</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Mode of delivery</Text>
                    <CheckBox value={anc5_vaginal} onValueChange={set_anc5_vaginal}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Vaginal</Text>
                    <CheckBox value={anc5_lscs} onValueChange={set_anc5_lscs}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>LSCS</Text>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Timing</Text>
                    <TextInput style={styles.textinput3}
                      placeholder="weeks"
                      multiline={true}
                      value={anc5_timing}
                      onChangeText={(anc5_timing) => set_anc5_timing(anc5_timing)}
                    >
                    </TextInput>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <CheckBox value={anc5_atten} onValueChange={set_anc5_atten}
                      style={{ alignSelf: "flex-start", marginTop: 2 }} />
                    <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Birth Attendant</Text>
                  </View>
                </View>

              </View>
            ) : null}

            {dikhao_anc5 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Examination</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_pallor} onValueChange={set_anc5_pallor}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pallor</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_pedal} onValueChange={set_anc5_pedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PR</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_PR}
                    onChangeText={(anc5_PR) => set_anc5_PR(anc5_PR)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BP</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(Systolic/Diastolic)"
                    multiline={true}
                    value={anc5_BP}
                    onChangeText={(anc5_BP) => set_anc5_BP(anc5_BP)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Weight</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_weight}
                    onChangeText={(anc5_weight) => set_anc5_weight(anc5_weight)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>P/A</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(should be within 2 weeks of POG)"
                    multiline={true}
                    value={anc5_PA}
                    onChangeText={(anc5_PA) => set_anc5_PA(anc5_PA)}
                  >
                  </TextInput>
                </View>

                <Text style={styles.subheading2}>CBC</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>HB</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_HB}
                    onChangeText={(anc5_HB) => set_anc5_HB(anc5_HB)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>TLC</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_TLC}
                    onChangeText={(anc5_TLC) => set_anc5_TLC(anc5_TLC)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Platelets</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_platelets}
                    onChangeText={(anc5_platelets) => set_anc5_platelets(anc5_platelets)}
                  >
                  </TextInput>
                </View>

                <Text style={styles.subheading2}>LFT</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>OT</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_OT}
                    onChangeText={(anc5_OT) => set_anc5_OT(anc5_OT)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PT</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_PT}
                    onChangeText={(anc5_PT) => set_anc5_PT(anc5_PT)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>ALP</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_ALP}
                    onChangeText={(anc5_ALP) => set_anc5_ALP(anc5_ALP)}
                  >
                  </TextInput>
                </View>

                <Text style={styles.subheading2}>KFT</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Urea</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_urea}
                    onChangeText={(anc5_urea) => set_anc5_urea(anc5_urea)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Creatinine</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_Creat}
                    onChangeText={(anc5_Creat) => set_anc5_Creat(anc5_Creat)}
                  >
                  </TextInput>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_examothers}
                    onChangeText={(anc5_examothers) => set_anc5_examothers(anc5_examothers)}
                  >
                  </TextInput>
                </View>

              </View>) : null}


            {dikhao_anc5 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Investigations</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>CBC</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_investcbc}
                    onChangeText={(anc5_investcbc) => set_anc5_investcbc(anc5_investcbc)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>LFT</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_investlft}
                    onChangeText={(anc5_investlft) => set_anc5_investlft(anc5_investlft)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>KFT</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_investkft}
                    onChangeText={(anc5_investkft) => set_anc5_investkft(anc5_investkft)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_investothers}
                    onChangeText={(anc5_investothers) => set_anc5_investothers(anc5_investothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}


            {dikhao_anc5 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>USG</Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BPD</Text>
                  <TextInput style={styles.textinputinpinkcm}
                    placeholder="cm"
                    multiline={true}
                    value={anc5_usgBPDcm}
                    onChangeText={(anc5_usgBPDcm) => set_anc5_usgBPDcm(anc5_usgBPDcm)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpink2}
                    placeholder="weeks"
                    multiline={true}
                    value={anc5_usgBPDweeks}
                    onChangeText={(anc5_usgBPDweeks) => set_anc5_usgBPDweeks(anc5_usgBPDweeks)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpinkcen}
                    placeholder="centile"
                    multiline={true}
                    value={anc5_usgBPDcentile}
                    onChangeText={(anc5_usgBPDcentile) => set_anc5_usgBPDcentile(anc5_usgBPDcentile)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>HC</Text>
                  <TextInput style={styles.textinputinpinkcm}
                    placeholder="cm"
                    multiline={true}
                    value={anc5_HCcm}
                    onChangeText={(anc5_HCcm) => set_anc5_HCcm(anc5_HCcm)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpink2}
                    placeholder="weeks"
                    multiline={true}
                    value={anc5_HCweeks}
                    onChangeText={(anc5_HCweeks) => set_anc5_HCweeks(anc5_HCweeks)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpinkcen}
                    placeholder="centile"
                    multiline={true}
                    value={anc5_HCcen}
                    onChangeText={(anc5_HCcen) => set_anc5_HCcen(anc5_HCcen)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>AC</Text>
                  <TextInput style={styles.textinputinpinkcm}
                    placeholder="cm"
                    multiline={true}
                    value={anc5_ACcm}
                    onChangeText={(anc5_ACcm) => set_anc5_ACcm(anc5_ACcm)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpink2}
                    placeholder="weeks"
                    multiline={true}
                    value={anc5_ACweeks}
                    onChangeText={(anc5_ACweeks) => set_anc5_ACweeks(anc5_ACweeks)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpinkcen}
                    placeholder="centile"
                    multiline={true}
                    value={anc5_ACcen}
                    onChangeText={(anc5_ACcen) => set_anc5_ACcen(anc5_ACcen)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>FL</Text>
                  <TextInput style={styles.textinputinpinkcm}
                    placeholder="cm"
                    multiline={true}
                    value={anc5_FLcm}
                    onChangeText={(anc5_FLcm) => set_anc5_FLcm(anc5_FLcm)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpink2}
                    placeholder="weeks"
                    multiline={true}
                    value={anc5_FLweeks}
                    onChangeText={(anc5_FLweeks) => set_anc5_FLweeks(anc5_FLweeks)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpinkcen}
                    placeholder="centile"
                    multiline={true}
                    value={anc5_FLcen}
                    onChangeText={(anc5_FLcen) => set_anc5_FLcen(anc5_FLcen)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>EFW</Text>
                  <TextInput style={styles.textinputinpinkcm}
                    placeholder="gm"
                    multiline={true}
                    value={anc5_EFWgm}
                    onChangeText={(anc5_EFWgm) => set_anc5_EFWgm(anc5_EFWgm)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpink2}
                    placeholder="weeks"
                    multiline={true}
                    value={anc5_EFWweeks}
                    onChangeText={(anc5_EFWweeks) => set_anc5_EFWweeks(anc5_EFWweeks)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpinkcen}
                    placeholder="centile"
                    multiline={true}
                    value={anc5_EFWcen}
                    onChangeText={(anc5_EFWcen) => set_anc5_EFWcen(anc5_EFWcen)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Liquor</Text>
                  <TextInput style={styles.textinputinpinkcm}
                    placeholder="SLP"
                    multiline={true}
                    value={anc5_liquorslp}
                    onChangeText={(anc5_liquorslp) => set_anc5_liquorslp(anc5_liquorslp)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpink2}
                    placeholder="AFI"
                    multiline={true}
                    value={anc5_liquorafi}
                    onChangeText={(anc5_liquorafi) => set_anc5_liquorafi(anc5_liquorafi)}
                  >
                  </TextInput>

                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>UA PI</Text>
                  <TextInput style={styles.textinputinpinkcm}
                    multiline={true}
                    value={anc5_UAPI}
                    onChangeText={(anc5_UAPI) => set_anc5_UAPI(anc5_UAPI)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpink2}
                    placeholder="centile"
                    multiline={true}
                    value={anc5_UAPIcen}
                    onChangeText={(anc5_UAPIcen) => set_anc5_UAPIcen(anc5_UAPIcen)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>MCA PI</Text>
                  <TextInput style={styles.textinputinpinkcm}
                    multiline={true}
                    value={anc5_MCAPI}
                    onChangeText={(anc5_MCAPI) => set_anc5_MCAPI(anc5_MCAPI)}
                  >
                  </TextInput>
                  <TextInput style={styles.textinputinpink2}
                    placeholder="centile"
                    multiline={true}
                    value={anc5_MCAPIcen}
                    onChangeText={(anc5_MCAPIcen) => set_anc5_MCAPIcen(anc5_MCAPIcen)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>CPR</Text>
                  <TextInput style={styles.textinputinpink2}
                    multiline={true}
                    value={anc5_CPR}
                    onChangeText={(anc5_CPR) => set_anc5_CPR(anc5_CPR)}
                  >
                  </TextInput>
                </View>
              </View>) : null}

            {dikhao_anc5 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_adviceDFMC} onValueChange={set_anc5_adviceDFMC}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>DFMC/ LLP</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_adviceTFe} onValueChange={set_anc5_adviceTFe}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe OD/ T Ca BD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_adviceman} onValueChange={set_anc5_adviceman}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>NST/ Manning biweekly</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_nutadvice} onValueChange={set_anc5_nutadvice}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nutritional Advice</Text>
                </View>
                <Text style={{ marginTop: 10, color: "grey", fontStyle: "italic", fontSize: 15 }}>Common Ailment Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_advicenausea} onValueChange={set_anc5_advicenausea}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea Vomiting</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_adviceheart} onValueChange={set_anc5_adviceheart}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Heart Burn</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_adviceconst} onValueChange={set_anc5_adviceconst}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Constipation</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_advicepedal} onValueChange={set_anc5_advicepedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal Edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_adviceleg} onValueChange={set_anc5_adviceleg}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leg Cramps</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_advicediabetic} onValueChange={set_anc5_advicediabetic}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Diabetic</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc5_advicelabour} onValueChange={set_anc5_advicelabour}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Review in labour room {"\n"} if (Bleeding PV, Spotting PV, Leaking PV,  {"\n"} Reduced fetal movement, Pain abdomen)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc5_adviceothers}
                    onChangeText={(anc5_adviceothers) => set_anc5_adviceothers(anc5_adviceothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}
          </View>


          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={anc6}
                onValueChange={(anc6) => {
                  set_anc6(anc6);
                  ShowHideComponent_seven()
                }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>ANC 6</Text>
            </View>
            {dikhao_anc6 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>Date</Text>
                <DatePicker
                  style={{ width: 150, marginTop: 10, marginLeft: 123, position: "absolute" }}
                  date={anc6date}
                  onDateChange={anc6date => {
                    set_anc6date(anc6date);

                  }}
                  //The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 10
                    },
                    dateInput: {
                      marginLeft: 55
                    }
                  }}>
                </DatePicker>
              </View>
            ) : null}

            {dikhao_anc6 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>POG</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc6_pog}
                  onChangeText={(anc6_pog) => set_anc6_pog(anc6_pog)}
                >
                </TextInput>
              </View>
            ) : null}

            {dikhao_anc6 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>History</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_short} onValueChange={set_anc6_short}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Shortness of breath</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_easy} onValueChange={set_anc6_easy}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Easy fatigability</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_head} onValueChange={set_anc6_head}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Headache/ epigastric pain/ blurring of vision/ {"\n"}
                    nausea</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_spot} onValueChange={set_anc6_spot}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Bleeding/spotting PV</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_mict} onValueChange={set_anc6_mict}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Burning micturition/ increased frequency of {"\n"} micturition</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_adequate} onValueChange={set_anc6_adequate}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Adequate fetal movements</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_itching} onValueChange={set_anc6_itching}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Generalized itching</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc6_hisother}
                    onChangeText={(anc6_hisother) => set_anc6_hisother(anc6_hisother)}
                  >
                  </TextInput>
                </View>

              </View>
            ) : null}

            {dikhao_anc6 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Examination</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_pallor} onValueChange={set_anc6_pallor}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pallor</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_pedal} onValueChange={set_anc6_pedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PR</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc6_PR}
                    onChangeText={(anc6_PR) => set_anc6_PR(anc6_PR)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BP</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(Systolic/Diastolic)"
                    multiline={true}
                    value={anc6_BP}
                    onChangeText={(anc6_BP) => set_anc6_BP(anc6_BP)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Weight</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc6_weight}
                    onChangeText={(anc6_weight) => set_anc6_weight(anc6_weight)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>P/A</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(should be within 2 weeks of POG)"
                    multiline={true}
                    value={anc6_PA}
                    onChangeText={(anc6_PA) => set_anc6_PA(anc6_PA)}
                  >
                  </TextInput>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc6_examothers}
                    onChangeText={(anc6_examothers) => set_anc6_examothers(anc6_examothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}

            {dikhao_anc6 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Pelvic assesment</Text>
                <TextInput style={styles.textinput}
                  multiline={true}
                  value={anc6_pelvic}
                  onChangeText={(anc6_pelvic) => set_anc6_pelvic(anc6_pelvic)}
                >
                </TextInput>
              </View>) : null}

            {dikhao_anc6 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_adviceDFMC} onValueChange={set_anc6_adviceDFMC}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>DFMC/ LLP</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_adviceTFe} onValueChange={set_anc6_adviceTFe}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe OD/ T Ca BD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_adviceman} onValueChange={set_anc6_adviceman}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>NST/ Manning biweekly</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_nutadvice} onValueChange={set_anc6_nutadvice}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nutritional Advice</Text>
                </View>
                <Text style={{ marginTop: 10, color: "grey", fontStyle: "italic", fontSize: 15 }}>Common Ailment Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_advicenausea} onValueChange={set_anc6_advicenausea}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea Vomiting</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_adviceheart} onValueChange={set_anc6_adviceheart}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Heart Burn</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_adviceconst} onValueChange={set_anc6_adviceconst}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Constipation</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_advicepedal} onValueChange={set_anc6_advicepedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal Edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_adviceleg} onValueChange={set_anc6_adviceleg}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leg Cramps</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_advicediabetic} onValueChange={set_anc6_advicediabetic}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Diabetic</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc6_advicelabour} onValueChange={set_anc6_advicelabour}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Review in labour room {"\n"} if (Bleeding PV, Spotting PV, Leaking PV,  {"\n"} Reduced fetal movement, Pain abdomen)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc6_adviceothers}
                    onChangeText={(anc6_adviceothers) => set_anc6_adviceothers(anc6_adviceothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}
          </View>

          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={anc7}
                onValueChange={(anc7) => {
                  set_anc7(anc7);
                  ShowHideComponent_eight()
                }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>ANC 7</Text>
            </View>
            {dikhao_anc7 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>Date</Text>
                <DatePicker
                  style={{ width: 150, marginTop: 10, marginLeft: 123, position: "absolute" }}
                  date={anc7date}
                  onDateChange={anc7date => {
                    set_anc7date(anc7date);

                  }}
                  //The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 10
                    },
                    dateInput: {
                      marginLeft: 55
                    }
                  }}>
                </DatePicker>
              </View>
            ) : null}

            {dikhao_anc7 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>POG</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc7_pog}
                  onChangeText={(anc7_pog) => set_anc7_pog(anc7_pog)}
                >
                </TextInput>
              </View>
            ) : null}

            {dikhao_anc7 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>History</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_short} onValueChange={set_anc7_short}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Shortness of breath</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_easy} onValueChange={set_anc7_easy}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Easy fatigability</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_head} onValueChange={set_anc7_head}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Headache/ epigastric pain/ blurring of vision/ {"\n"}
                    nausea</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_spot} onValueChange={set_anc7_spot}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Bleeding/spotting PV</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_mict} onValueChange={set_anc7_mict}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Burning micturition/ increased frequency of {"\n"} micturition</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_adequate} onValueChange={set_anc7_adequate}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Adequate fetal movements</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_itching} onValueChange={set_anc7_itching}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Generalized itching</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc7_hisother}
                    onChangeText={(anc7_hisother) => set_anc7_hisother(anc7_hisother)}
                  >
                  </TextInput>
                </View>

              </View>
            ) : null}

            {dikhao_anc7 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Examination</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_pallor} onValueChange={set_anc7_pallor}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pallor</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_pedal} onValueChange={set_anc7_pedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PR</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc7_PR}
                    onChangeText={(anc7_PR) => set_anc7_PR(anc7_PR)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BP</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(Systolic/Diastolic)"
                    multiline={true}
                    value={anc7_BP}
                    onChangeText={(anc7_BP) => set_anc7_BP(anc7_BP)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Weight</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc7_weight}
                    onChangeText={(anc7_weight) => set_anc7_weight(anc7_weight)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>P/A</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(should be within 2 weeks of POG)"
                    multiline={true}
                    value={anc7_PA}
                    onChangeText={(anc7_PA) => set_anc7_PA(anc7_PA)}
                  >
                  </TextInput>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc7_examothers}
                    onChangeText={(anc7_examothers) => set_anc7_examothers(anc7_examothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}



            {dikhao_anc7 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_adviceDFMC} onValueChange={set_anc7_adviceDFMC}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>DFMC/ LLP</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_adviceTFe} onValueChange={set_anc7_adviceTFe}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe OD/ T Ca BD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_adviceman} onValueChange={set_anc7_adviceman}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>NST/ Manning biweekly</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_nutadvice} onValueChange={set_anc7_nutadvice}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nutritional Advice</Text>
                </View>
                <Text style={{ marginTop: 10, color: "grey", fontStyle: "italic", fontSize: 15 }}>Common Ailment Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_advicenausea} onValueChange={set_anc7_advicenausea}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea Vomiting</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_adviceheart} onValueChange={set_anc7_adviceheart}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Heart Burn</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_adviceconst} onValueChange={set_anc7_adviceconst}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Constipation</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_advicepedal} onValueChange={set_anc7_advicepedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal Edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_adviceleg} onValueChange={set_anc7_adviceleg}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leg Cramps</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_advicediabetic} onValueChange={set_anc7_advicediabetic}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Diabetic</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc7_advicelabour} onValueChange={set_anc7_advicelabour}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Review in labour room {"\n"} if (Bleeding PV, Spotting PV, Leaking PV,  {"\n"} Reduced fetal movement, Pain abdomen)</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc7_adviceothers}
                    onChangeText={(anc7_adviceothers) => set_anc7_adviceothers(anc7_adviceothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}
          </View>

          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <CheckBox value={anc8}
                onValueChange={(anc8) => {
                  set_anc8(anc8);
                  ShowHideComponent_nine()
                }}
                style={{ alignSelf: "flex-start", marginTop: 2 }} />
              <Text style={{ marginLeft: 5, marginTop: 8, color: "#E95078", fontSize: 15 }}>ANC 8</Text>
            </View>
            {dikhao_anc8 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>Date</Text>
                <DatePicker
                  style={{ width: 150, marginTop: 10, marginLeft: 123, position: "absolute" }}
                  date={anc8date}
                  onDateChange={anc8date => {
                    set_anc8date(anc8date);

                  }}
                  //The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-2012"
                  maxDate={new Date()}
                  showYearDropdown
                  ScrollableMonthYearDropdown
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 10
                    },
                    dateInput: {
                      marginLeft: 55
                    }
                  }}>
                </DatePicker>
              </View>
            ) : null}

            {dikhao_anc8 ? (
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18, }}>POG</Text>
                <TextInput style={styles.textinput2}
                  multiline={true}
                  value={anc8_pog}
                  onChangeText={(anc8_pog) => set_anc8_pog(anc8_pog)}
                >
                </TextInput>
              </View>
            ) : null}

            {dikhao_anc8 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>History</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_short} onValueChange={set_anc8_short}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Shortness of breath</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_easy} onValueChange={set_anc8_easy}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Easy fatigability</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_head} onValueChange={set_anc8_head}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Headache/ epigastric pain/ blurring of vision/ {"\n"}
                    nausea</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_spot} onValueChange={set_anc8_spot}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Bleeding/spotting PV</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_mict} onValueChange={set_anc8_mict}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Burning micturition/ increased frequency of {"\n"} micturition</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_adequate} onValueChange={set_anc8_adequate}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Adequate fetal movements</Text>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_itching} onValueChange={set_anc8_itching}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Generalized itching</Text>
                </View>


                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinput3}
                    multiline={true}
                    value={anc8_hisother}
                    onChangeText={(anc8_hisother) => set_anc8_hisother(anc8_hisother)}
                  >
                  </TextInput>
                </View>

              </View>
            ) : null}

            {dikhao_anc8 ? (
              <View style={styles.container3}>
                <Text style={styles.subheading}>Examination</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_pallor} onValueChange={set_anc8_pallor}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pallor</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_pedal} onValueChange={set_anc8_pedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>PR</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc8_PR}
                    onChangeText={(anc8_PR) => set_anc8_PR(anc8_PR)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>BP</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(Systolic/Diastolic)"
                    multiline={true}
                    value={anc8_BP}
                    onChangeText={(anc8_BP) => set_anc8_BP(anc8_BP)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Weight</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc8_weight}
                    onChangeText={(anc8_weight) => set_anc8_weight(anc8_weight)}
                  >
                  </TextInput>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>P/A</Text>
                  <TextInput style={styles.textinputinpink}
                    placeholder="(should be within 2 weeks of POG)"
                    multiline={true}
                    value={anc8_PA}
                    onChangeText={(anc8_PA) => set_anc8_PA(anc8_PA)}
                  >
                  </TextInput>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc8_examothers}
                    onChangeText={(anc8_examothers) => set_anc8_examothers(anc8_examothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}



            {dikhao_anc8 ? (
              <View style={styles.container2}>
                <Text style={styles.subheading}>Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_adviceDFMC} onValueChange={set_anc8_adviceDFMC}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>DFMC/ LLP</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_adviceTFe} onValueChange={set_anc8_adviceTFe}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>T Fe OD/ T Ca BD</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_adviceman} onValueChange={set_anc8_adviceman}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>NST/ Manning biweekly</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_nutadvice} onValueChange={set_anc8_nutadvice}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nutritional Advice</Text>
                </View>
                <Text style={{ marginTop: 10, color: "grey", fontStyle: "italic", fontSize: 15 }}>Common Ailment Advice</Text>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_advicenausea} onValueChange={set_anc8_advicenausea}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Nausea Vomiting</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_adviceheart} onValueChange={set_anc8_adviceheart}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Heart Burn</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_adviceconst} onValueChange={set_anc8_adviceconst}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Constipation</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_advicepedal} onValueChange={set_anc8_advicepedal}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Pedal Edema</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_adviceleg} onValueChange={set_anc8_adviceleg}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Leg Cramps</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <CheckBox value={anc8_advicediabetic} onValueChange={set_anc8_advicediabetic}
                    style={{ alignSelf: "flex-start", marginTop: 2 }} />
                  <Text style={{ marginLeft: 5, marginTop: 8, color: "#525252", fontSize: 14 }}>Diabetic</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#525252", fontSize: 14, marginLeft: 5, marginTop: 18 }}>Others</Text>
                  <TextInput style={styles.textinputinpink}
                    multiline={true}
                    value={anc8_adviceothers}
                    onChangeText={(anc8_adviceothers) => set_anc8_adviceothers(anc8_adviceothers)}
                  >
                  </TextInput>
                </View>
              </View>) : null}

          </View>

          <TouchableOpacity
            style={styles.my_Button}
            onPress={() => { updateData(); Toast.show('Data added successfully'); }}>
            <Text style={{ color: "black" }}>Add Data</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>

  )
}


export default patient_screenbydoc

const styles = StyleSheet.create({
  container: {
    flex: 0,
    padding: 10,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2,

  },
  my_Button: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    marginLeft: 5,
    marginBottom: 10,
    marginRight: 15,
    marginTop: 10,
    alignSelf: 'center',
    backgroundColor: '#FAE3E9'
  },
  container2: {
    flex: 0,
    padding: 5,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "#FAE3E9",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2,
    marginTop: 20
  },
  container3: {
    flex: 0,
    padding: 5,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "#FAE3E9",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2,
    marginTop: 5
  },
  subheading: {
    color: "#3C9382",
    fontSize: 15,
    marginLeft: 5,
    marginTop: 5
  },
  subheading2: {
    color: "#3C9382",
    fontSize: 15,
    marginLeft: 5,
    marginTop: 15
  },
  textinput: {
    width: "60%",
    position: "absolute",
    alignItems: "center",
    marginLeft: 140,
    marginRight: 100,
    borderBottomColor: "pink",
    borderTopColor: "white",
    borderRightColor: "white",
    borderLeftColor: "white",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1,
    marginBottom: 20
  },
  textinput2: {
    width: "60%",
    position: "absolute",
    alignItems: "center",
    marginLeft: 140,
    marginRight: 100,
    borderBottomColor: "pink",
    borderTopColor: "white",
    borderRightColor: "white",
    borderLeftColor: "white",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  textinput_new: {
    width: "30%",
    position: "relative",
    alignItems: "center",
    marginLeft: 20,
    marginRight: -50,
    borderBottomColor: "pink",
    borderTopColor: "white",
    borderRightColor: "white",
    borderLeftColor: "white",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  textinput_newinpink: {
    width: "30%",
    position: "relative",
    alignItems: "center",
    marginLeft: 20,
    marginRight: -50,
    borderBottomColor: "pink",
    borderTopColor: "#FAE3E9",
    borderRightColor: "#FAE3E9",
    borderLeftColor: "#FAE3E9",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  textinput3: {
    width: "60%",
    position: "absolute",
    alignItems: "center",
    marginLeft: 140,
    marginRight: 100,
    borderBottomColor: "pink",
    borderTopColor: "#FAE3E9",
    borderRightColor: "#FAE3E9",
    borderLeftColor: "#FAE3E9",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -20,
    borderWidth: 1

  },
  textinputinpink: {
    width: "60%",
    position: "absolute",
    alignItems: "center",
    marginLeft: 140,
    marginRight: 100,
    borderBottomColor: "pink",
    borderTopColor: "#FAE3E9",
    borderRightColor: "#FAE3E9",
    borderLeftColor: "#FAE3E9",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  textinputt2: {
    marginTop: 20,
    width: "60%",
    position: "absolute",
    alignItems: "center",
    marginLeft: 140,
    marginRight: 100,
    borderBottomColor: "pink",
    borderTopColor: "white",
    borderRightColor: "white",
    borderLeftColor: "white",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  textinputinpinkcm: {
    width: "15%",
    position: "absolute",
    alignItems: "center",
    marginLeft: 70,
    // marginRight: 20,
    borderBottomColor: "pink",
    borderTopColor: "#FAE3E9",
    borderRightColor: "#FAE3E9",
    borderLeftColor: "#FAE3E9",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  textinputinpink2: {
    width: "15%",
    position: "absolute",
    alignItems: "center",
    marginLeft: 150,
    // marginRight: 20,
    borderBottomColor: "pink",
    borderTopColor: "#FAE3E9",
    borderRightColor: "#FAE3E9",
    borderLeftColor: "#FAE3E9",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  },
  textinputinpinkcen: {
    width: "15%",
    position: "absolute",
    alignItems: "center",
    marginLeft: 240,
    // marginRight: 20,
    borderBottomColor: "pink",
    borderTopColor: "#FAE3E9",
    borderRightColor: "#FAE3E9",
    borderLeftColor: "#FAE3E9",
    flexDirection: "column",
    marginTop: 10,
    paddingTop: -5,
    paddingBottom: -5,
    borderWidth: 1
  }


})