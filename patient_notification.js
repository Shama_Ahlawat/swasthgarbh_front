import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { AppRegistry, StyleSheet, Text, View, Image, FlatList, ScrollView, StatusBar} from 'react-native';
import { Divider, Provider, Appbar } from 'react-native-paper'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';

const patient_notification = (props) => {
    const [data, setdata] = useState([]);

    const ContentTitle = ({ title, style }) => (
        <Appbar.Content
          title={<Text style={style}> {title} </Text>}
        />
      );

    const fetchnotification = async () => {
        const tok = await AsyncStorage.getItem("token")
        const patcred = await fetch('https://swasthgarbh-backend.herokuapp.com/patient', {
            headers: new Headers({
                Authorization: "Bearer " + tok
            })
        })
        const pat = await patcred.json();
        const arr = []
        const allnotification = await fetch('https://swasthgarbh-backend.herokuapp.com/patientnotification_list')
        const notify = await allnotification.json();

        for (var i = 0; i < notify.length; i++) {
            if (notify[i].pat_mobile == pat.mobile) {
                arr.push(notify[i])
                const pops = arr.pop();
                arr.unshift(pops)
                console.log(arr)
            }
        }

        setdata(arr)
    }

    useEffect(() => {
        fetchnotification();
    }, [])


    return (
        <ScrollView>
        <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />

        <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
          <ContentTitle title="Notifications" style={{ color: 'white', fontSize: 20 }} />
        </Appbar.Header>

            <FlatList style={{ flex: 0, backgroundColor: "white" }}
                data={data}
                // maxToRenderPerBatch={10}
                keyExtractor={(item, index) => item._id}
                renderItem={({ item, index }) =>
                    <View>
                        <Text style={{ color: "gray", fontSize: 18, marginLeft: 5, marginBottom: 15, marginTop: 15, marginRight: 5 }}>{item.notification}</Text>
                        <Divider style={styles.divider} />
                    </View>
                }
            />

        </ScrollView>

    )

}

const styles = StyleSheet.create({
    divider: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 1.55,
        shadowRadius: 2.84,
        elevation: 2

    }


})
export default patient_notification