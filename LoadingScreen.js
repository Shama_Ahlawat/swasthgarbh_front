
import React, {useEffect} from 'react';
import {Button, Appbar, TextInput} from 'react-native-paper';
import {
    StyleSheet,
    View,
    ActivityIndicator
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import patient_screen from './patient_screen';

const LoadingScreen= (props) => {
    const detectLogin = async ()=>{
        const token = await AsyncStorage.getItem('token')
        if(token){
            props.navigation.replace('patient_screen')
        }else{
            props.navigate.replace('Signin')
        }
    }
    useEffect(()=>{
        detectLogin()
    },[])
  return (
    <View style={styles.loading}>
    <ActivityIndicator size = "large" color = ""></ActivityIndicator>
    </View>
  );
};

const styles = StyleSheet.create({
    loading:{
        flex:1,
        justifyContent:"center",
        alignItems:"center"
    }

})
export default LoadingScreen;

























