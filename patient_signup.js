import React, { useState, setState, Component, useEffect} from 'react';
import {Picker} from 'react-native';
import { Button, Appbar, TextInput } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';
import DatePicker from 'react-native-datepicker';
// import Picker from "@react-native-community/picker";

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  KeyboardAvoidingView,
  TouchableOpacity,
  Switch
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const patient_signup = (props) => {
  const [mobile, setMobile] = useState('')
  const [password, setPassword] = useState('')
  const [name, setName] = useState('')
  const [address, setAddress] = useState('')
  const [age, setAge] = useState('')
  const [date, setDate] = useState('')
  const [email, setEmail] = useState('')
  const [high_bp_check, set_high_bp_check] = useState(false)
  const [history_pre, set_history_pre] = useState(false)
  const [mother_sis, set_mother_sis] = useState(false)
  const [obesity, set_obesity] = useState(false)
  const [baby, set_baby] = useState(false)
  const [any_history, set_any_history] = useState(false)
  const [education, setEducation] = useState('Select')
  const [lmpdate, setLmpdate] = useState(null)
  const [sestatus, setSestatus] = useState(null)
  const [isEnabled, setIsEnabled] = useState(false);
  const [mobileDoctor, setDoctormobile] = useState('');
  const [docname, setdocname] = useState('');
  const [open, setOpen] = useState(false)

  const toggleSwitch = () => {
    setIsEnabled(previousState => !previousState)
  }

  const ContentTitle = ({ title, style }) => (
    <Appbar.Content
      title={<Text style={style}> {title} </Text>}
    />
  );

  const sendCred = async (props) => {
    console.log('after clickkk')
    fetch("https://swasthgarbh-backend.herokuapp.com/patient_signup", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "mobile": mobile,
        "password": password,
        "name": name,
        "age": age,
        "address": address,
        "email": email,
        "mobileDoctor": mobileDoctor,
        "docname": docname
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log('patient signup successfull *&&&',data)
        try {
          await AsyncStorage.setItem('token', data.token)
          props.navigation.replace("patient_screen")
        } catch (e) {
          console.log("error is ", e)
        }
      })
  }

  const fetchdata = async (mobileDoctor) => {
    await fetch('https://swasthgarbh-backend.herokuapp.com/doctorlist', {
    }).then(res => res.json())
      .then(data => {
        console.log(mobileDoctor)
        for (var i = 0; i < data.length; i++) {
          if (mobileDoctor == (data[i].mobile)) {
            console.log("found")
            console.log(data[i].name)
            setdocname(data[i].name)
           }
        }
      }
      )
  }
  useEffect(() => {
    fetchdata()
  }, [])

  // const docmobile_retrieve = ()=>{
  //   const mobileis = {Doctormobile}
  //   console.log(mobileis)
  // }
  //  const fun = (mobileDoctor)=>{

  //  }



  return (
    <ScrollView>
      <KeyboardAvoidingView behavior='padding'>
        <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />

        <Appbar.Header style={{ backgroundColor: "#F8BBD0" }}>
          <ContentTitle title="Patient Registration" style={{ color: 'white', fontSize: 20 }} />
        </Appbar.Header>

        <TextInput label="name"
          value={name}
          onChangeText={(text) => setName(text)}
          style={{ marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#F8BBD0",opacity:0.5 }}
          theme={{ colors: { primary: "black" } }}></TextInput>

        <TextInput label="address (Optional)"
          value={address}
          onChangeText={(text) => setAddress(text)}
          style={{ flex: 0, marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#F8BBD0",opacity:0.5  }}
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        <TextInput label="age (in years)"
          value={age}
          onChangeText={(text) => setAge(text)}
          style={{ marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#F8BBD0",opacity:0.5  }}
          theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

        {/* <View style={{ flexDirection: "row" }}>
          <Text style={{ color: "grey", marginTop: 20, marginLeft: 18 }}>Last Menstrual Period</Text>

          <DatePicker
            style={{ width: 200, marginTop: 10 }}
            date={lmpdate}
            onDateChange={date => setLmpdate(date)}
            //The enum of date, datetime and time
            placeholder="select date"
            format="DD-MM-YYYY"
            minDate="01-01-2012"
            maxDate={new Date()}
            showYearDropdown
            ScrollableMonthYearDropdown
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateInput: {
                marginLeft: 36
              }
            }}>
          </DatePicker>
        </View> */}

        {/* <View style={styles.container}>
          <Text style={{ color: "grey", marginTop: 5, marginLeft: 10 }}>Some extra details (Optional)</Text>
          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={high_bp_check}
                onValueChange={set_high_bp_check}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>History of high blood pressure prior to pregnancy</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={history_pre}
                onValueChange={set_history_pre}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>A history of preeclampsia</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={mother_sis}
                onValueChange={set_mother_sis}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>Having a mother or sister who had preeclampsia</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={obesity}
                onValueChange={set_obesity}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>A history of obesity</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={baby}
                onValueChange={set_baby}
                style={styles.checkbox}
              />
              <Text style={{ margin: 5 }}>Carrying more than one baby</Text>
            </View>
          </View>

          <View style={styles.container_checkbox}>
            <View style={styles.checkboxContainer}>
              <CheckBox
                value={any_history}
                onValueChange={set_any_history}
                style={styles.checkbox}
              />
              <Text style={{ marginRight: 10, marginLeft: 5, marginTop: 5, marginBottom: 5 }}>History of diabetes, kidney disease, lupus, or rheumatoid arthritis</Text>
            </View>
          </View>

          <Text style={{ marginTop: 5, marginBottom: 0, marginLeft: 5, marginRight: 5 }}>Education </Text>
          <Picker style={{ marginTop: 0 }}
            selectedValue={education}
            onValueChange={(itemValue, itemIndex) => setEducation(itemValue)}>
            <Picker.Item label="Select" value="Select" color="#f06292"></Picker.Item>
            <Picker.Item label="Primary" value="Primary"></Picker.Item>
            <Picker.Item label="Secondary" value="Secondary"></Picker.Item>
            <Picker.Item label="Graduation" value="Graduation"></Picker.Item>
            <Picker.Item label="Post-Graduation" value="Post-Graduation"></Picker.Item>
          </Picker>

          <Text style={{ marginTop: 5, marginBottom: 0, marginLeft: 5, marginRight: 5 }}>SE Status </Text>
          <Picker style={{ marginTop: 0 }}
            selectedValue={sestatus}
            onValueChange={(itemValue, itemIndex) => setSestatus(itemValue)}>
            <Picker.Item label="Select" value="Select" color="#f06292"></Picker.Item>
            <Picker.Item label="Lower" value="Lower"></Picker.Item>
            <Picker.Item label="Upper Lower" value="Upper Lower"></Picker.Item>
            <Picker.Item label="Lower Middle" value="Lower Middle"></Picker.Item>
            <Picker.Item label="Upper Middle" value="Upper Middle"></Picker.Item>
            <Picker.Item label="Upper" value="Upper"></Picker.Item>
          </Picker>

        </View> */}

        <View style={styles.container}>
          <Text style={{ margin: 5, color: "grey" }}>Your credentials</Text>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ marginLeft: 5, color: "gray", marginTop: 40 }}>+91 - </Text>
            <TextInput label="Mobile number"
              value={mobile}
              onChangeText={(text) => setMobile(text)}
              style={{ flex: 1, marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#FAE3E9" }}
              keyboadType='numeric'
              theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
          </View>

          <TextInput label="Password"
            secureTextEntry={true}
            style={{ marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "#FAE3E9" }}
            value={password}
            onChangeText={(text) => setPassword(text)}
            theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

          <TextInput label="Email (optional)"
            value={email}
            onChangeText={(text) => setEmail(text)}
            style={{ marginLeft: 18, marginTop: 18, marginRight: 18, flex: 1, backgroundColor: "#F8BBD0" }}
            theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
        </View>
        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ margin: 5, color: "black" }}>Doctor</Text>
            <Switch style={{ marginLeft: 260 }}
              trackColor={{ false: "grey", true: "pink" }}
              thumbColor={isEnabled ? "pink" : "grey"}
              value={isEnabled}
              onValueChange={(isEnabled) => {
                toggleSwitch();
                setOpen(!open);
              }}
            />
          </View>

          {open ?
            <View style={{ flexDirection: "row" }}>
              <Text style={{ marginLeft: 5, color: "gray", marginTop: 40 }}>+91 - </Text>
              <TextInput label="Registered Doctor's Number"
                // value={Doctormobile}
                onChangeText={mobileDoctor => {
                  setDoctormobile(mobileDoctor)
                  fetchdata(mobileDoctor)
                }}

                style={{ flex: 1, marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "white" }}
                keyboadType='numeric'
                theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
            </View>
            : null}

          {open ?
            <TextInput label="Doctor's Name"
              value={docname}
              onChangeText={(text) => setdocname(text)}
              style={{ flex: 1, marginLeft: 18, marginTop: 18, marginRight: 18, backgroundColor: "white" }}
              editable={false}
              theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
            : null}

        </View>

        <TouchableOpacity
          style={styles.my_Button}
          onPress={() => sendCred(props)}>
          <Text style={{ color: "black", fontweight: "bold", paddingTop: 10, paddingBottom: 10, paddingLeft: 30, paddingRight: 30 }}>Register</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </ScrollView>

  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    paddingBottom: 10,
    marginBottom: 5,
    borderRadius: 15,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    backgroundColor: "white",
  },
  my_Button: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    marginLeft: 5,
    marginBottom: 10,
    marginRight: 15,
    marginTop: 10,
    alignSelf: 'center',
    backgroundColor: '#F8BBD0',
  },
  container_calender: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    padding: 16
  },

  container_checkbox: {
    marginTop: 5,
    paddingLeft: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  checkbox: {
    alignSelf: "flex-start",
  },
  checkboxContainer: {
    flexDirection: "row",
    alignSelf: "flex-start",
    marginBottom: 1,
  }
})

export default patient_signup;

























