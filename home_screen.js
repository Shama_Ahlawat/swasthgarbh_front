import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, TouchableOpacity, Image, KeyboardAvoidingView, TextInput } from 'react-native';
import { Provider } from 'react-native-paper';
import Overlay from 'react-native-modal-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import { widthToDp, heightToDp } from './Responsive';

var logo = require('./drawables/sg.png');
var doc_icon = require('./drawables/doctoricon.png');
var patient_icon = require('./drawables/pregnanticon.png')
var mobile_icon = require('./drawables/mobile.png');
var key_icon = require('./drawables/key.png');
var applogo = require('./drawables/applogo.jpg');
var patient = require('./drawables/patient.jpg');

export default class home_screen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mobile: '',
      password: '',
      finalpass: '',
      visible: false,
      fcmToken:''

    }
  }



  getFirebaseTokenFromAsyncStorage = async() => {
    console.log('coming ')
   await AsyncStorage.getItem("fcmToken", (error, result) => {
      if (result !== undefined && result !== null) {
        this.setState({ fcmToken: result });
        console.log('login class fcm token ',result)
       

      } else {
      console.log('no firebase token genrated')
      }
    });
  };

  componentDidMount=()=>{

   this.getFirebaseTokenFromAsyncStorage()

  }




  // componentDidMount(){
  //   const token = AsyncStorage.getItem('token')
  //  console.log('issss token here',token)
  //   if (token !== null) {
  //     this.props.navigation.replace('patient_screen')
  //   }
  //   else{
  //     this.props.navigation.replace('home_screen')
  //   }
  // }

    sendpatCred = async () => {
    const{fcmToken}=this.state

    fetch("https://swasthgarbh-backend.herokuapp.com/patient_signin", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "mobile": this.state.mobile,
        "password": this.state.password,
        "fcm_token":fcmToken
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log('myyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',data)
        await AsyncStorage.setItem('token', data.token)
        this.props.navigation.replace('patient_screen')
      })
  }

  senddocCred = async () => {
    fetch("https://swasthgarbh-backend.herokuapp.com/doctor_signin", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "mobile": this.state.mobile,
        "password": this.state.password
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log(data)
        await AsyncStorage.setItem('token', data.token)
        this.props.navigation.replace('doctor_screen')
      })
  }

  sendpatbydocCred = async () => {
    fetch("https://swasthgarbh-backend.herokuapp.com/patient_signinbydoc", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "mobile": this.state.mobile,
        "password": this.state.password
      })
    })
      .then(res => res.json())
      .then(async (data) => {
        console.log(data)
        await AsyncStorage.setItem('token', data.token)
        this.props.navigation.replace('patient_screenbydocsignin')
        console.log(token)
      })
  }

  // button_func = () =>  // arrow function for button functioning
  // {
  //   //alert("welcome to SwasthGarbh")
  //   this.props.navigation.navigate('Signin');
  // }
  anc_func = () =>  // arrow function for button functioning
  {
    this.props.navigation.navigate('anc_assist');
  }
  disclaimer_func = () =>  // arrow function for button functioning
  {
    this.props.navigation.navigate('disclaimer');
  }
  patient_func = () =>  // arrow function for button functioning
  {
    this.props.navigation.navigate('patient_signup');
  }
  doctor_func = () => {
    this.props.navigation.navigate('doctor_signup');
  }

  onClose = () => this.setState({ visible: false });
  onOpen = () => this.setState({ visible: true });



  


  render() {
    return (
      <Provider>
        <>
          <View
            style={{
              backgroundColor: '#F8BBD0',
              top: 0, left: 0,
              right: 0, bottom: 0,
              position: 'absolute'
            }} >

            <Image source={applogo} style={{ height: 100, width: 100, alignSelf: 'center', marginTop: 100 }}></Image>

            <Text style={{
              fontSize: widthToDp('8%'),
              color: 'white',
              alignSelf: 'center'

            }} > Chikitsa </Text>

            <View style={styles.container}>
              <TouchableOpacity
                style={styles.myButton}
                onPress={() => this.onOpen()}>
                <Text style={styles.text}>Sign In </Text>
              </TouchableOpacity>
            </View>

            {/* <TouchableOpacity
              style={styles.ANC_asist}
              onPress={this.anc_func}>
              <Text style={styles.text_anc}>ⓘ ANC Assist</Text>
            </TouchableOpacity> */}

            <Text style={{
              fontSize: widthToDp ('4%'),
              color: 'white',
              marginTop: 10,
              alignSelf: 'center'
            }} > or Register as </Text>

            <View style={{ flexDirection: "row" }}>

              <Image source={patient} style={{
                height: 60, width: 60, marginBottom: heightToDp('2%'),
                marginTop: heightToDp('3%'),
                marginLeft: widthToDp(('19%')),
              }}></Image>
              <Image source={doc_icon} style={styles.doc_image}></Image>

            </View>

            <View style={{ flexDirection: "row" }}>

              <View style={styles.container}>
                <TouchableOpacity
                  style={styles.my_Button}
                  onPress={this.patient_func}>
                  <Text style={styles.text}>Patient</Text>
                </TouchableOpacity>
              </View>

              <View style={styles.container}>
                <TouchableOpacity style={styles.my_Button_sys}
                  onPress={this.doctor_func}>
                  <Text style={styles.text}>Doctor</Text>

                </TouchableOpacity>
              </View>
            </View>

            {/* <Text style={{
              fontSize: widthToDp ('4%'),
              color: 'white',
              marginTop: 10,
              alignSelf: 'center'
            }} > Developed by CompBio Lab, IIT Roorkee</Text> */}

            {/* <TouchableOpacity
              onPress={this.disclaimer_func}>
              <Text style={{
                fontSize: widthToDp ('4%'),
                color: 'white',
                marginTop: 2,
                alignSelf: 'center'
              }} > Disclaimer</Text>
            </TouchableOpacity> */}

            <Overlay visible={this.state.visible} onClose={this.onClose} closeOnTouchOutside>
              <View style={{ height: 230, alignSelf: "flex-start", width: "95%", marginLeft: 8 }}>
                <KeyboardAvoidingView behavior='position'>
                  <View style={{ flexDirection: "row" }}>
                    <Image source={mobile_icon} style={{ height: 40, width: 40, marginLeft: widthToDp('1.5%'), marginTop: 20 }}></Image>
                    <View style={{ flex: 1, marginTop: 10 }}>
                      <TextInput placeholder="Mobile number"
                        style={{ borderWidth: 1, borderBottomColor: "pink", borderLeftColor: "white", borderRightColor: "white", borderTopColor: "white", position: "relative", marginLeft: widthToDp('3%') }}
                        value={this.state.mobile}
                        onChangeText={(mobile) =>
                          this.setState({ mobile })
                        }
                        theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
                    </View>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <Image source={key_icon} style={{ height: 40, width: 40, marginLeft: widthToDp('1.5%'), marginTop: 20 }}></Image>
                    <View style={{ flex: 1, marginTop: 10 }}>
                      <TextInput placeholder="Password"
                        secureTextEntry={true}
                        style={{ borderWidth: 1, borderBottomColor: "pink", borderLeftColor: "white", borderRightColor: "white", borderTopColor: "white", marginLeft: widthToDp('3%'), alignItems: "center", backgroundColor: "white", position: "relative" }}
                        value={this.state.password}
                        onChangeText={(password) =>
                          this.setState({ password })
                        }
                        theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
                    </View>
                  </View>

                  <Text style={{ color: "grey", marginLeft: widthToDp ('45%'), marginTop: heightToDp ('2%'), fontStyle: "italic" }}>ⓘ Forgot Password</Text>

                  <TouchableOpacity
                    style={{ alignSelf: "center", backgroundColor: "pink", paddingHorizontal: widthToDp('6%'),
                    paddingVertical: heightToDp('3%'),
                    marginLeft: widthToDp('2%'), marginTop: heightToDp('2%') }}
                    onPress={() => {
                      this.sendpatCred();
                      this.senddocCred();
                      this.sendpatbydocCred();
                    }}>

                    <Text style={{ color: "black", fontSize: widthToDp('4%') }}>Sign In</Text>
                  </TouchableOpacity>


                </KeyboardAvoidingView>
              </View>
            </Overlay>



          </View>
        </>
      </Provider>


    );
  }
}
const styles = StyleSheet.create({
  text: {
    color: 'black',
    fontSize: widthToDp ('4%'),
  },
  text_anc: {
    color: 'grey',
    fontStyle: "italic"
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',

  },
  myButton: {
    paddingHorizontal: 40,
    paddingVertical: 10,
    backgroundColor: 'white'
  },
  my_Button: {
    paddingHorizontal: widthToDp('6%'),
    paddingVertical: heightToDp('2%'),
    marginLeft: widthToDp('16%'),
    backgroundColor: 'white'
  },
  my_Button_sys: {
    paddingHorizontal: widthToDp('6%'),
    paddingVertical: heightToDp('2%'),
    marginLeft: widthToDp('22%'),
    backgroundColor: 'white'
  },
  doc_image: {
    height: 60,
    width: 60,
    marginBottom: heightToDp('2%'),
    marginTop: heightToDp('3%'),
    marginLeft: widthToDp('33%')
  },
  ANC_asist: {
    fontSize: 20,
    fontStyle: 'italic',
    marginTop: 15,
    color: 'grey',
    alignSelf: 'center'
  }
});