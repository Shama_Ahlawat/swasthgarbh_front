import React, { useState, useEffect } from 'react';
import { Button, Appbar, TextInput, Menu, Divider, Provider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import moment from 'moment';
import * as firebase from '@react-native-firebase/app';
//import * as fbmessaging from '@react-native-firebase/messaging';
import messaging from '@react-native-firebase/messaging';
//import { firebaseConfig } from './config';
import Unorderedlist from 'react-native-unordered-list';
import 'moment-timezone';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';
import Overlay from 'react-native-modal-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import all_patientsindoc from './all_patientsindoc';
import patient_listview from './patient_listview';


var profile_icon = require('./drawables/doctor.png');
var refresh_icon = require('./drawables/reload2.png');
var ancassist_icon = require('./drawables/checkup.png');
var regpat_icon = require('./drawables/pinkultrasound.png');
var trim_icon = require('./drawables/send.png');
var corona_icon = require('./drawables/corona.png');
var logout_icon = require('./drawables/logout.png');
var trim_not_icon = require('./drawables/trim_not.png');

//firebase.initializeApp(firebaseConfig);

const doctor_screen = (props) => {
  const [edd, edd_value] = useState('loading ...');
  const [name, setName] = useState('loading ...');
  const [hospital, setHospital] = useState('loading ...');
  const [speciality, setSpeciality] = useState('loading ...');
  const [email, setEmail] = useState('loading ...')
  const [totpat, setTotpat] = useState("---")
  const [visible, setVisible] = React.useState(false);
  const [allpatVisible, setallpatVisible] = React.useState(false);
  const [modalVisible, setModelvisible] = useState(false);
  const [first_trim, setfirst_trim] = useState('');
  const [second_trim, setsecond_trim] = useState('');
  const [third_trim, setthird_trim] = useState('');

  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  const onClose = () => setModelvisible(false);
  const onOpen = () => setModelvisible(true);

  const fetchdata = async (props) => {
    const token = await AsyncStorage.getItem("token")
    await fetch('https://swasthgarbh-backend.herokuapp.com/doctor', {
      headers: new Headers({
        Authorization: "Bearer " + token
      })
    }).then(res => res.json())
      .then(data => {
        console.log(data)
        setName(data.name)
        setHospital(data.hospital)
        setEmail(data.email)
        setSpeciality(data.speciality)

      }
    )
  }
  //const configurationOptions = {
    //debug: true
  //};/
  //firebase.initializeApp(firebaseConfig);
 const firebase = RNFirebase.initializeApp(configurationOptions);
  
  //export default firebase;

  const getToken = async () => {
    //console.log(('bharat :' , fMessage));
    console.log("checking firbase import $$$$$$$$$" + firebase);
    //const msg = fbmessaging.messaging();
    const firebaseTok = await firebaseToken.getToken();
    //console.log(msg.getToken());
    //const firebaseToken = await msg.messaging.getToken;
    console.log("token is ")
    console.log (firebaseTok);
  }
  
  const trim = async () => {
    const token = await AsyncStorage.getItem("token")
    const doc = await fetch('https://swasthgarbh-backend.herokuapp.com/doctor', {
      headers: new Headers({
        Authorization: "Bearer " + token
      })
    })
    const doctor = await doc.json()
    const mobile = (doctor.mobile)
    const dateis = new Date().getDate()+ '/' + new Date().getMonth()+ '/' + new Date().getFullYear();
    console.log("first trim is: ", (first_trim), " second trim is: ", second_trim, " third trim is: ", third_trim)

    await fetch("https://swasthgarbh-backend.herokuapp.com/trim_notification", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "first_trim": first_trim,
        "second_trim": second_trim,
        "third_trim": third_trim,
        "date": dateis,
        'doc_mobile': mobile
      })
    })
      .then(res => res.json())
      .then(async (trim_data) => {
        console.log("the added trimester data is ", trim_data)
      })
  }

  useEffect(() => {
    fetchdata();
    getToken();
  }, [])

  const logout = async () => {
    AsyncStorage.removeItem("token").then(() => {
      props.navigation.replace("home_screen")
    })
  }

  const allpatClose = () => setallpatVisible(false);
  const allpatOpen = () => setallpatVisible(true);


  const patlist = () => {
    props.navigation.navigate('patient_listview')
  }

  const patlistuhid = () => {
    props.navigation.navigate('patient_listview_uhid')
  }

  return (
    <>
      <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
      <Provider>
        <View style={{ height: 60, backgroundColor: "#F8BBD0", }}>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "white", padding: 10, fontSize: 20, fontWeight: "bold", marginTop: 10 }}>Doctor</Text>
            <View style={{ marginRight: 5, marginTop: -7, paddingLeft: 285 }}>
              <Menu
                visible={visible}
                onDismiss={closeMenu}
                anchor={<TouchableOpacity
                  onPress={openMenu} >
                  <Text style={[styles.threedots]}>...</Text>
                </TouchableOpacity>}>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Image source={refresh_icon} style={{ height: 30, width: 30, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => alert("yo")} title="Refresh" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={ancassist_icon} style={{ height: 30, width: 30, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('anc_assist')} title="ANC Assist" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={regpat_icon} style={{ height: 30, width: 30, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('patient_regdoc')} title="Register Patient" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={trim_icon} style={{ height: 25, width: 25, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5, marginLeft: -9 }} onPress={() => { onOpen(); closeMenu() }} title="Trimester Notification" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={corona_icon} style={{ height: 30, width: 30, marginLeft: 5, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => props.navigation.navigate('youtube_covid')} title="Covid'19 Advice" />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Image source={logout_icon} style={{ height: 30, width: 30, marginLeft: 10, marginTop: 5 }}></Image>
                  <Menu.Item style={{ marginTop: -5 }} onPress={() => logout()} title="Logout" />
                </View>
              </Menu>
            </View>
          </View>
        </View>

        <Overlay visible={modalVisible} onClose={onClose} closeOnTouchOutside>

          <View style={{ height: 350, alignSelf: "flex-start", width: "95%", marginLeft: 8 }}>
            <View style={{ flexDirection: "row" }}>
              <Image source={trim_not_icon} style={{ height: 35, width: 35, marginLeft: -5, marginBottom: 10 }}></Image>
              <Text style={{ fontSize: 25, color: "grey", fontWeight: "bold", marginLeft: 10, marginBottom: 10 }}>Trimester Notification</Text>
            </View>

            <View style={styles.container_overlay}>
              <Text style={{ fontSize: 20, color: "grey", fontWeight: "bold", marginLeft: 15 }}>First Trimester</Text>
              <TextInput placeholder="Enter Message"
                style={{ borderLeftColor: "white", borderRightColor: "white", borderTopColor: "white", position: "relative", backgroundColor: "white" }}
                value={first_trim}
                onChangeText={(text) => setfirst_trim(text)}
                multiline={true}
                theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>

              <Text style={{ fontSize: 20, color: "grey", fontWeight: "bold", marginLeft: 15, marginTop: 10 }}>Second Trimester</Text>
              <View style={{ inputHeightBase: 10 }}>
                <TextInput placeholder="Enter Message"
                  style={{ borderLeftColor: "white", borderRightColor: "white", borderTopColor: "white", position: "relative", backgroundColor: "white" }}
                  value={second_trim}
                onChangeText={(text) => setsecond_trim(text)}
                  multiline={true}
                  theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
              </View>

              <Text style={{ fontSize: 20, color: "grey", fontWeight: "bold", marginLeft: 15, marginTop: 10 }}>ThirdTrimester</Text>
              <TextInput placeholder="Enter Message"
                style={{ borderLeftColor: "white", borderRightColor: "white", borderTopColor: "white", position: "relative", backgroundColor: "white" }}
                value={third_trim}
                onChangeText={(text) => setthird_trim(text)}
                multiline={true}
                theme={{ colors: { primary: "#F8BBD0" } }}></TextInput>
            </View>

          </View>

          <TouchableOpacity
            style={{ alignSelf: "center", backgroundColor: "pink", marginTop: 20 }}
            onPress={() => {
              trim();
              onClose();
            }}>
            {/* // onPress= {this.ShowHideComponent}> */}
            <Text style={{ color: "black", padding: 15 }}>Send</Text>
          </TouchableOpacity>

        </Overlay>


        <Overlay visible={allpatVisible} onClose={() => allpatClose()} closeOnTouchOutside>
          <Unorderedlist bulletUnicode={0x25E6} color='#fb6f92' style={{ fontSize: 20 }}>
            <Text
              style={{ fontSize: 20, color: "#fb6f92", marginBottom: 10, marginLeft: 5 }}
              onPress={(props) => { patlist(); allpatClose() }} >Patients registered themselves with you</Text></Unorderedlist>

          <Unorderedlist bulletUnicode={0x25E6} color='#fb6f92' style={{ fontSize: 20 }}>
            <Text style={{ fontSize: 20, color: "#fb6f92", marginLeft: 5 }}
              onPress={(props) => {
                patlistuhid();
                allpatClose()
              }}>
              Patients registered by you</Text></Unorderedlist>


        </Overlay>



        <View style={styles.container}>
          <View style={{ flexDirection: "row" }}>
            <Image source={profile_icon} style={{ height: 100, width: 100, marginTop: 5, marginBottom: 5, marginLeft: 5, marginRight: 10, marginHorizontal: 0 }}></Image>
            <View style={styles.container_text}>
              <Text style={{ fontSize: 15, color: "gray" }}>name                         {name}</Text>
              <Text style={{ fontSize: 15, color: "gray" }}>Hospital                    {hospital}</Text>
              <Text style={{ fontSize: 15, color: "gray" }}>Speciality                  {speciality}</Text>
              <Text style={{ fontSize: 15, color: "gray" }}>Total Patients           {totpat}</Text>
              <Text style={{ fontSize: 15, color: "gray" }}>email                          {email}</Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <Text style={{ fontSize: 12, color: "#f45536", alignSelf: "center", marginBottom: 10 }}>* Dummy data for educational purpose </Text>
          <Text style={{ fontSize: 14, color: "gray", marginLeft: 6, marginTop: 5 }}>X Axis: ANC Level | Y Axis: Number of patients</Text>
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: 10, height: 10, backgroundColor: '#62E4FB', marginBottom: 10, marginTop: 10, marginLeft: 30 }} />
            <Text style={{ fontSize: 10, color: "#313437", marginTop: 7 }}>  Following WHO Guidlines </Text>
            <View style={{ width: 10, height: 10, backgroundColor: '#F44D3D', marginBottom: 10, marginTop: 10, marginLeft: 8 }} />
            <Text style={{ fontSize: 10, color: "#313437", marginTop: 7 }}>  Not Following WHO Guidlines </Text>
          </View>

          <View style={{ flexDirection: "row" }}>

            <TouchableOpacity
              style={styles.my_Button}
              onPress={() => allpatOpen()}>
              <Text style={styles.text}>All Patients</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.my_Button}
              onPress={() => props.navigation.navigate('doc_notifypage')}>
              <Text style={styles.text}>Notifications</Text>
            </TouchableOpacity>
          </View>
          <Text style={{ fontSize: 13, color: "gray", marginLeft: 6, marginTop: 10 }}>Number of patients in corresponding categories </Text>
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: 10, height: 10, backgroundColor: '#1B54B3', marginBottom: 10, marginTop: 15, marginLeft: 5 }} />
            <Text style={{ fontSize: 10, color: "#313437", marginTop: 13 }}>  High Systolic BP </Text>
            <View style={{ width: 10, height: 10, backgroundColor: '#279429', marginBottom: 10, marginTop: 15, marginLeft: 8 }} />
            <Text style={{ fontSize: 10, color: "#313437", marginTop: 13 }}>  High Diastolic BP </Text>
            <View style={{ width: 10, height: 10, backgroundColor: '#E3FB9C', marginBottom: 10, marginTop: 15, marginLeft: 8 }} />
            <Text style={{ fontSize: 10, color: "#313437", marginTop: 13 }}>  Underweight </Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: 10, height: 10, backgroundColor: '#FFC107', marginBottom: 10, marginTop: 3, marginLeft: 5 }} />
            <Text style={{ fontSize: 10, color: "#313437", marginTop: 1 }}>  High Urine Albumin </Text>
            <View style={{ width: 10, height: 10, backgroundColor: '#F44D3D', marginBottom: 10, marginTop: 3, marginLeft: 8 }} />
            <Text style={{ fontSize: 10, color: "#313437", marginTop: 1 }}>  Following WHO </Text>
          </View>
          <Text style={{ fontSize: 10, color: "#313437", marginTop: 0 }}>  (Distribution of #patients on various factors) </Text>

        </View>

      </Provider>
      {/* <Button mode= 'contained' style = {{marginLeft:18, marginRight:18, marginTop:18}} 
      onPress = {() => logout()} > Sign Out </Button> */}

    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0,
    padding: 5,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "white",
    shadowColor: '#470000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    elevation: 2
  },
  container_overlay: {
    flex: 1,
    padding: 10,
    paddingBottom: 0,
    marginBottom: 5,
    borderRadius: 15,
    marginLeft: 2,
    marginRight: 2,
    backgroundColor: "white",
    shadowColor: "#F8BBD0",
    shadowOffset: { width: 10, height: 20 },
    shadowOpacity: 1.2,
    elevation: 2
  },
  container_text: {
    justifyContent: 'flex-start',
    alignItems: "flex-start",
  },
  my_Button: {
    paddingHorizontal: 45,
    paddingVertical: 10,
    marginLeft: 12,
    marginRight: 15,
    alignSelf: 'flex-start',
    backgroundColor: '#F8BBD0'
  },
  logout_Button: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    marginLeft: 5,
    marginRight: 15,
    alignSelf: 'center',
    backgroundColor: '#F8BBD0'
  },
  threedots: {
    color: "white",
    fontWeight: "bold",
    fontSize: 40
  }
})

export default doctor_screen;