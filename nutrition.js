import React, { Component, useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet, TouchableOpacity, Image, ScrollView, StatusBar, TextInput } from 'react-native';
import { Appbar, Menu, Provider } from 'react-native-paper';
import Unorderedlist from 'react-native-unordered-list';
import DatePicker from 'react-native-datepicker';
import 'moment-timezone';
import moment from 'moment';


const nutrition = (props) => {

    return (
        <ScrollView style={{ backgroundColor: '#FAE3E9' }}>
            <StatusBar backgroundColor="#F8BBD0" barStyle="light-content" />
            <Provider>
                <View style={{ height: 60, backgroundColor: "#F8BBD0", }}>
                    <Text style={{ color: "white", padding: 10, fontSize: 20, fontWeight: "bold", marginTop: 10 }}>Nutritional Advice</Text>
                </View>

                <View style={styles.container1}>
                    <Text style={{ color: "black", fontSize: 15, fontStyle: "italic" }}>Nutritional Advise</Text>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, }}>
                        <Text style={{ marginTop: 5, color: "gray" }}>Have a balanced diet</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Ensure adequate hydration and have 6 - 8 glasses of water daily</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Ensure a daily diet with adequate protein, dairy products and fruits</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Have folate rich foods like green leafy vegetables, legumes, beans</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Have Vitamin C rich food daily</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Have a High fibre diet</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Take small and frequent meals</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Avoid smoking/alcohol</Text></Unorderedlist>
                </View>

                <View style={styles.container2}>
                    <Text style={{ color: "black", fontSize: 15, fontStyle: "italic" }}>General</Text>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, }}>
                        <Text style={{ marginTop: 5, color: "gray" }}>Take iron and calcium supplements daily as prescribed</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Iron should not be taken empty stomach</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>A gap of two hours should be kept between the iron and calcium tablets</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Aim for daily moderate intensity activity for 30 minutes</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Avoid lifting heavy weight</Text></Unorderedlist>
                </View>

                <View style={styles.container2}>
                    <Text style={{ color: "black", fontSize: 15, fontStyle: "italic" }}>Diabetic Diet</Text>
                    <Text style={{ color: "gray", fontSize: 15, }}>(3+3) pattern- Take 3 major meals with intervening 3 minor meals.</Text>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, }}>
                        <Text style={{ marginTop: 5, color: "gray" }}>Avoid long hours of fasting</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>The diet should be balanced comprising 35-40% carbohydrates, 20% proteins and 40% fat</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Light exercises are advocated</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>30 minutes of walking daily for 5 days a week</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Symptoms of hypoglycaemia- sweating, palpitations, weakness should not be ignored</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}> A spot blood sugar test using glucometer should be done, If BS is less than 60, take biscuits or juice. Contact your treating Physician</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Home Blood sugar monitoring should be done routinely and recorded</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Records should be brought to ANC OPD for assessment of therapy</Text></Unorderedlist>
                </View>

                <View style={styles.container2}>
                    <Text style={{ color: "black", fontSize: 15, fontStyle: "italic" }}>Nausea and Vomiting</Text>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, }}>
                        <Text style={{ marginTop: 5, color: "gray" }}>Drink plenty of fluids and maintain hydration</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Eat small frequent meals</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Have non-greasy meals with little odor</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Ginger or chamomile tea can help reduce morning sickness</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>If excessive, vitamin B6 supplemets can be tried</Text></Unorderedlist>
                </View>

                 <View style={styles.container2}>
                    <Text style={{ color: "black", fontSize: 15, fontStyle: "italic" }}>Heartburn</Text>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, }}>
                        <Text style={{ marginTop: 5, color: "gray" }}>Eat small frequent meals</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Avoid spicy and greasy foods</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Elevate head of bed when lying down</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Avoid lying down immediately after taking a meal</Text></Unorderedlist>
                </View>

                <View style={styles.container2}>
                    <Text style={{ color: "black", fontSize: 15, fontStyle: "italic" }}>Constipation</Text>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, }}>
                        <Text style={{ marginTop: 5, color: "gray" }}>Eat fresh fruits and vegetables</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Drink 8-10 glasses of water</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Eat foods with high fibre</Text></Unorderedlist>
                </View>

                <View style={styles.container2}>
                    <Text style={{ color: "black", fontSize: 15, fontStyle: "italic" }}>Pedal Edema</Text>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, }}>
                        <Text style={{ marginTop: 5, color: "gray" }}>Rest with legs elevated</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Lie in the left lateral position</Text></Unorderedlist>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, marginTop: -7 }}>
                        <Text style={styles.text_new}>Avoid sitting or standing for long periods</Text></Unorderedlist>
                </View>

                <View style={styles.container2}>
                    <Text style={{ color: "black", fontSize: 15, fontStyle: "italic" }}>Leg Cramps</Text>
                    <Unorderedlist bulletUnicode={0x25E6} color='gray' style={{ fontSize: 22, }}>
                        <Text style={{ marginTop: 5, color: "gray" }}>Daily magnesium/calcium supplements can be used </Text></Unorderedlist>
                </View>

                






            </Provider>
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container1: {
        flex: 1,
        padding: 10,
        paddingBottom: 0,
        marginBottom: 5,
        borderRadius: 15,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
        marginBottom: 10,
        backgroundColor: "white",
        shadowColor: '#470000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.2,
        elevation: 2
    },
    container2: {
        flex: 1,
        padding: 10,
        paddingBottom: 0,
        marginBottom: 5,
        borderRadius: 15,
        marginLeft: 5,
        marginRight: 5,
        marginTop: -3,
        marginBottom: 10,
        backgroundColor: "white",
        shadowColor: '#470000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.2,
        elevation: 2
    },
    text_new: {
        color: "gray",
        fontSize: 15,
        marginTop: -2
    }
});

export default nutrition