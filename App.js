import React, {
  Component,
  useEffect,
  useState,AppState
} from 'react';
import {
  createAppContainer
} from 'react-navigation';
import {
  createStackNavigator
} from '@react-navigation/stack';
import {
  NavigationContainer
} from '@react-navigation/native';



import splash from './splash';
import patient_screen from './patient_screen';
import patient_signup from './patient_signup';
import Signin from './Signin';
import anc_assist from './anc_assist';
import disclaimer from './disclaimer';
import LoadingScreen from './LoadingScreen';
import home_screen from './home_screen';
import doctor_signup from './doctor_signup';
import patient_add_data from './patient_add_data';
import map from './Component/map';
import patient_medicine from './patient_medicine';
import doctor_medicine from './doctor_medicine';
import youtube_covid from './Component/youtube_covid';
import youtube_preeclampsia from './Component/youtube_preeclampsia';
import tests from './tests';
import doctor_screen from './doctor_screen';
import patient_regdoc from './patient_regdoc';
import all_patientsindoc from './all_patientsindoc';
import patient_listview from './patient_listview';
import patient_insidedoc from './patient_insidedoc';
import patient_screenbydoc from './patient_screenbydoc';
import patient_screenbydocsignin from './patient_screenbydocsignin';
import nutrition from './nutrition';
import overlay_medicine from './overlay_medicine';
import patient_listview_uhid from './patient_listview_uhid';
import patientuhid_insidedoc from './patientuhid_insidedoc';
import doc_notifypage from './doc_notifypage';
import patient_notification from './patient_notification';
import add_medication from './add_medication'
import AsyncStorage from '@react-native-community/async-storage';
import {
  intervalToDuration
} from 'date-fns/esm/fp';
import firebase from "react-native-firebase";


const Stack = createStackNavigator();


const App = () => {
  const [isloggedin, setLogged] = useState(null)



  useEffect(() => {
    detectLogin();
    checkPermission();
  
  }, [])



  const checkPermission = async () => {


    firebase.messaging().hasPermission().then(granted => {
      if (granted) {
        firebase.messaging().getToken().then(token => {
          if (token)
            // this.setState({token});
            console.log('token>>>>>>>>=====', token)
          AsyncStorage.setItem('fcmToken', token);
        })
      } else {
        firebase.messaging().requestPermission().then(() => {
          // this.checkForNotificationsPermission();
        }).catch(err => {
          //This is where I get the error:  "failed to grant permissions" (on err.message)
        })
      }
    })
  }


  const detectLogin = async () => {
    const token = await AsyncStorage.getItem('token')

    if (token) {
      setLogged(true)
    } else {
      setLogged(false)
      console.log("no user is logged in")
    }
  }
  const getToken = async () => {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token

        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  const requestPermission = async () => {


    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('error', error);
      console.log("permission rejected");
    }
  }

  return (
    <NavigationContainer>
      <Stack.Navigator
        headerMode="none">
        <Stack.Screen name="splash" component={splash} />
        <Stack.Screen name="home_screen" component={home_screen} />
        <Stack.Screen name="patient_signup" component={patient_signup} />
        <Stack.Screen name="Signin" component={Signin} />
        <Stack.Screen name="doctor_signup" component={doctor_signup} />
        <Stack.Screen name="patient_screen" component={patient_screen} />
        <Stack.Screen name="LoadingScreen" component={LoadingScreen} />
        <Stack.Screen name="anc_assist" component={anc_assist} />
        <Stack.Screen name="disclaimer" component={disclaimer} />
        <Stack.Screen name="patient_add_data" component={patient_add_data} />
        <Stack.Screen name="map" component={map} />
        <Stack.Screen name="patient_medicine" component={patient_medicine} />
        <Stack.Screen name="doctor_medicine" component={doctor_medicine} />
        <Stack.Screen name="youtube_covid" component={youtube_covid} />
        <Stack.Screen name="youtube_preeclampsia" component={youtube_preeclampsia} />
        <Stack.Screen name="tests" component={tests} />
        <Stack.Screen name="doctor_screen" component={doctor_screen} />
        <Stack.Screen name="patient_regdoc" component={patient_regdoc} />
        <Stack.Screen name="all_patientsindoc" component={all_patientsindoc} />
        <Stack.Screen name="patient_listview" component={patient_listview} />
        <Stack.Screen name="patient_insidedoc" component={patient_insidedoc} />
        <Stack.Screen name="patient_screenbydoc" component={patient_screenbydoc} />
        <Stack.Screen name="patient_screenbydocsignin" component={patient_screenbydocsignin} />
        <Stack.Screen name="nutrition" component={nutrition} />
        <Stack.Screen name="overlay_medicine" component={overlay_medicine} />
        <Stack.Screen name="patient_listview_uhid" component={patient_listview_uhid} />
        <Stack.Screen name="patientuhid_insidedoc" component={patientuhid_insidedoc} />
        <Stack.Screen name="doc_notifypage" component={doc_notifypage} />
        <Stack.Screen name="patient_notification" component={patient_notification} />
        <Stack.Screen name="add_medication" component={add_medication} />
        {/* add_medication */}

      </Stack.Navigator>

    </NavigationContainer>
  );
};

export default App;
